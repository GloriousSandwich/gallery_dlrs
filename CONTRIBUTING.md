# Contributing to Gallery-dlrs

- [Setting Up Your Development Environment](#setup)
- [Commit Message Format](#commit)

### <a name="setup"></a> Setting Up Your Development Environment

The following section is **not** an exhaustive guide, and only covers common setup and development tasks.

### Install dependancies

You'll need to have a working Rust environment to build the code, and a working Git installation to fetch the code.

- [Rust](https://www.rust-lang.org/tools/install) for linux.
- [Git](https://git-scm.com/downloads). Note for Linux, macOS, and some
  Unix-like devices Git may be available via a package manager; `apt`, `brew`,
  `yum`, `pacman`, etc.

### Clone the source code

In order to get a copy of the latest version of gallery_dlrs source code:

```console
git clone https://gitlab.com/GloriousSandwich/gallery_dlrs.git
```

### Update the source code

To get the latest updates, you can run:

```console
git pull origin master
```

Note, if you are working on a local git branch it may be wise to use `fetch and `merge` options instead.

```console
git fetch origin/master
```

Please see a good Git tutorial for more information.

## <a name="commit"></a> Commit Message Format

This specification is inspired by [Conventional Commits message format](https://www.conventionalcommits.org/en/v1.0.0/).

We have very precise rules over how our Git commit messages must be formatted. This format leads to
**easier to read commit history**.

Each commit message consists of a **header**, a **body**, and a **footer**.

```
<header>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

The `header` is mandatory and must conform to the [Commit Message Header](#commit-header) format.

#### <a name="commit-header"></a>Commit Message Header

```
<type>(<scope>): <short summary>
  │       │             │
  │       │             └─⫸ Summary in present tense. Not capitalized. No period at the end.
  │       │
  │       └─⫸ Commit Scope: core|config|artstation
  │
  └─⫸ Commit Type: docs|feat|fix|perf|refactor
```

The `<type>` and `<summary>` fields are mandatory, the `(<scope>)` field is optional.

##### Type

Must be one of the following:

* **docs**: Documentation only changes
* **feat**: A new feature
* **fix**: A bug fix
* **perf**: A code change that improves performance
* **refactor**: A code change that neither fixes a bug nor adds a feature

##### Scope

The scope should be the name of the npm package affected (as perceived by the person reading the changelog generated
from commit messages).

The following is the list of supported scopes:

* `core`
* `config`
* `macro`
* `oauth`
* `artstation`
* `reddit`
* `webtoons`

There are currently a few exceptions to the "use package name" rule:

* `changelog`: used for updating the release notes in CHANGELOG.md
* none/empty string: useful for `test` and `refactor` changes that are done across all packages
  (e.g. `test: add missing unit tests`) and for docs changes that are not related to a specific package
  (e.g. `docs: fix typo in tutorial`).

##### Summary

Use the summary field to provide a succinct description of the change:

* use the imperative, present tense: "change" not "changed" nor "changes"
* don't capitalize the first letter
* no dot (.) at the end

#### <a name="commit-footer"></a>Commit Message Footer

The footer can contain information about breaking changes and deprecations and is also the place to reference GitLab
issues and other PRs that this commit closes or is related to.
For example:

```
BREAKING CHANGE: <breaking change summary>
<BLANK LINE>
<breaking change description + migration instructions>
<BLANK LINE>
<BLANK LINE>
Fixes #<issue number>
```

or

```
DEPRECATED: <what is deprecated>
<BLANK LINE>
<deprecation description + recommended update path>
<BLANK LINE>
<BLANK LINE>
Closes #<pr number>
```

Breaking Change section should start with the phrase "BREAKING CHANGE: " followed by a summary of the breaking change, a
blank line, and a detailed description of the breaking change that also includes migration instructions.

Similarly, a Deprecation section should start with "DEPRECATED: " followed by a short description of what is deprecated,
a blank line, and a detailed description of the deprecation that also mentions the recommended update path.

### Revert commits

If the commit reverts a previous commit, it should begin with `revert: `, followed by the header of the reverted commit.

The content of the commit message body should contain:

- information about the SHA of the commit being reverted in the following format: `This reverts commit <SHA>`,
- a clear description of the reason for reverting the commit message.

## Where are some good places to start hacking?

You might want to begin my looking around the [codebase](https://gitlab.com/GloriousSandwich/gallery_dlrs).

More tests would always be great.

Adding support for more websites would be neat.

More documentation would be great.

Improving the look and feel of the documentation would also rock.

Improvements or bugfixes to the existing code would be great.
