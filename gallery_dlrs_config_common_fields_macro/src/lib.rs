use proc_macro::TokenStream;

use quote::quote;
use syn::parse::{Nothing, Parser};
use syn::punctuated::Punctuated;
use syn::{
    parse_macro_input, Data, DataStruct, DeriveInput, Error, Field, Fields, Ident, ItemStruct, Path,
};

const UNSUPPORTED_ERROR: &str = "common_types must be used on a C struct";

#[proc_macro_attribute]
pub fn config_common_fields(_attribute: TokenStream, input: TokenStream) -> TokenStream {
    let mut input: ItemStruct = parse_macro_input!(input as ItemStruct);
    let _ = parse_macro_input!(_attribute as Nothing);

    match &mut input.fields {
        Fields::Named(fields) => {
            fields.named = add_fields(fields.named.clone());
        }
        _ => {
            return Error::new(input.ident.span(), UNSUPPORTED_ERROR)
                .to_compile_error()
                .into();
        }
    }

    TokenStream::from(quote! {
        #input
    })
}

fn add_fields<T: Default>(mut fields: Punctuated<Field, T>) -> Punctuated<Field, T> {
    // Add filename_format.
    fields.push(
        syn::Field::parse_named
            .parse2(quote! { pub filename_format: String })
            .expect("Cannot add filename_format to token stream"),
    );

    // Add directory_format.
    fields.push(
        syn::Field::parse_named
            .parse2(quote! { pub directory_format: String })
            .expect("Cannot add directory_format to token stream"),
    );

    fields
}
