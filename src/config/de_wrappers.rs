/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Custom wrappers for deserializing.

use std::fmt::Formatter;
use std::io::Write;
use std::path::PathBuf;
use std::str::FromStr;
use std::{fmt, io};

use chrono::{DateTime, NaiveDateTime, TimeZone, Utc};
#[allow(unused_imports)]
use file_rotate::compression::Compression;
#[allow(unused_imports)]
use file_rotate::ContentLimit;
use oauth2::{AuthUrl, ClientId, ClientSecret, RedirectUrl, Scope, TokenUrl};
use reqwest::header::{HeaderName, HeaderValue};
use reqwest::Proxy;
use serde::de::{EnumAccess, Error, MapAccess, SeqAccess, Visitor};
use serde::{Deserialize, Deserializer};
use url::Url;

use crate::text::parse_bytes;

#[derive(Clone, Debug)]
#[allow(clippy::missing_docs_in_private_items, missing_docs)]
pub struct AuthUrlWrapper(AuthUrl);

#[derive(Clone, Debug)]
#[allow(clippy::missing_docs_in_private_items, missing_docs)]
pub struct ClientIdWrapper(ClientId);

#[derive(Clone, Debug)]
#[allow(clippy::missing_docs_in_private_items, missing_docs)]
pub struct ClientSecretWrapper(ClientSecret);

#[derive(Clone, Debug)]
#[allow(clippy::missing_docs_in_private_items)]
pub struct CompressionWrapper(Compression);

#[derive(Clone, Debug)]
#[allow(clippy::missing_docs_in_private_items)]
pub struct ContentLimitWrapper(ContentLimit);

#[derive(Clone, Debug, Default)]
#[allow(clippy::missing_docs_in_private_items)]
pub struct FilesizeWrapper(i64);

#[derive(Clone, Debug)]
#[allow(clippy::missing_docs_in_private_items)]
pub struct HeaderNameWrapper(pub HeaderName);

#[derive(Clone, Debug)]
#[allow(clippy::missing_docs_in_private_items)]
pub struct HeaderValueWrapper(pub HeaderValue);

#[derive(Clone, Debug)]
#[allow(missing_docs)]
pub struct PathbufWrapper(PathBuf);

#[derive(Clone, Debug)]
#[allow(clippy::missing_docs_in_private_items)]
pub struct ProxyWrapper(Proxy);

#[derive(Clone, Debug)]
#[allow(missing_docs, clippy::missing_docs_in_private_items)]
pub struct RedirectUrlWrapper(RedirectUrl);

#[derive(Clone, Debug)]
#[allow(missing_docs, clippy::missing_docs_in_private_items)]
pub struct ScopeWrapper(Scope);

#[derive(Clone, Debug)]
#[allow(missing_docs, clippy::missing_docs_in_private_items)]
pub struct TokenUrlWrapper(TokenUrl);

#[derive(Clone, Debug)]
#[allow(clippy::missing_docs_in_private_items)]
pub enum DateTimeWrapper {
    Naive(NaiveDateTime),
    Utc(DateTime<Utc>),
}

impl AuthUrlWrapper {
    #[allow(missing_docs, clippy::missing_docs_in_private_items)]
    pub fn auth_url(&self) -> AuthUrl {
        self.0.clone()
    }
}

impl ClientIdWrapper {
    #[allow(missing_docs, clippy::missing_docs_in_private_items)]
    pub fn client_id(&self) -> ClientId {
        self.0.clone()
    }

    #[allow(missing_docs, clippy::missing_docs_in_private_items)]
    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }
}

impl ClientSecretWrapper {
    #[allow(missing_docs, clippy::missing_docs_in_private_items)]
    pub fn client_secret(&self) -> ClientSecret {
        self.0.clone()
    }
    #[allow(missing_docs, clippy::missing_docs_in_private_items)]
    pub fn secret(&self) -> &String {
        self.0.secret()
    }
}

impl CompressionWrapper {
    #[allow(clippy::missing_docs_in_private_items)]
    pub fn into_inner(self) -> Compression {
        self.0
    }
}

impl ContentLimitWrapper {
    #[allow(clippy::missing_docs_in_private_items)]
    pub fn into_inner(self) -> ContentLimit {
        self.0
    }
}

impl FilesizeWrapper {
    #[allow(clippy::missing_docs_in_private_items)]
    pub fn into_inner(self) -> i64 {
        self.0
    }
}

impl HeaderNameWrapper {
    #[allow(clippy::missing_docs_in_private_items)]
    pub fn into_inner(self) -> HeaderName {
        self.0
    }
}

impl HeaderValueWrapper {
    #[allow(clippy::missing_docs_in_private_items)]
    pub fn into_inner(self) -> HeaderValue {
        self.0
    }
}

impl PathbufWrapper {
    #[allow(missing_docs)]
    pub fn into_inner(self) -> PathBuf {
        self.0
    }

    #[allow(clippy::missing_docs_in_private_items)]
    pub fn to_string(&self) -> Option<String> {
        self.0.to_str().map(|string| string.to_owned())
    }
}

impl ProxyWrapper {
    #[allow(clippy::missing_docs_in_private_items)]
    pub fn into_inner(self) -> Proxy {
        self.0
    }
}

impl RedirectUrlWrapper {
    #[allow(missing_docs, clippy::missing_docs_in_private_items)]
    pub fn redirect_url(&self) -> RedirectUrl {
        self.0.clone()
    }

    #[allow(missing_docs, clippy::missing_docs_in_private_items)]
    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }

    #[allow(missing_docs, clippy::missing_docs_in_private_items)]
    pub fn url(&self) -> &Url {
        self.0.url()
    }
}

impl ScopeWrapper {
    #[allow(missing_docs, clippy::missing_docs_in_private_items)]
    pub fn into_inner(self) -> Scope {
        self.0
    }
}

impl TokenUrlWrapper {
    #[allow(missing_docs, clippy::missing_docs_in_private_items)]
    pub fn token_url(&self) -> TokenUrl {
        self.0.clone()
    }
}

impl DateTimeWrapper {
    #[allow(clippy::missing_docs_in_private_items)]
    pub fn timestamp(&self) -> i64 {
        match self {
            Self::Utc(utc) => utc.timestamp(),
            Self::Naive(naive) => naive.timestamp(),
        }
    }

    #[allow(clippy::missing_docs_in_private_items)]
    pub fn to_naive(&self) -> Option<NaiveDateTime> {
        if let Self::Naive(naive) = &self {
            return Some(*naive);
        }

        None
    }

    #[allow(clippy::missing_docs_in_private_items)]
    pub fn to_utc(&self) -> Option<DateTime<Utc>> {
        if let Self::Utc(utc) = &self {
            return Some(*utc);
        }

        None
    }
}

impl Default for CompressionWrapper {
    fn default() -> Self {
        Self(Compression::None)
    }
}

impl Default for ContentLimitWrapper {
    fn default() -> Self {
        Self(ContentLimit::Lines(1000))
    }
}

impl Default for PathbufWrapper {
    fn default() -> Self {
        Self(std::path::PathBuf::from(""))
    }
}

impl Default for DateTimeWrapper {
    fn default() -> Self {
        Self::Utc(Utc.timestamp(0, 0))
    }
}

impl<'de> Deserialize<'de> for AuthUrlWrapper {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let url: String = String::deserialize(deserializer)?;

        match AuthUrl::new(url.clone()) {
            Ok(url) => Ok(Self(url)),
            Err(error) => {
                let _ = writeln!(
                    io::stderr(),
                    "config error: invalid authorization endpoint URL \"{}\": {}",
                    url,
                    error
                );

                Err(Error::custom(""))
            }
        }
    }
}

impl<'de> Deserialize<'de> for ClientIdWrapper {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let id: String = String::deserialize(deserializer)?;

        Ok(Self(ClientId::new(id)))
    }
}

impl<'de> Deserialize<'de> for ClientSecretWrapper {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let secret: String = String::deserialize(deserializer)?;
        Ok(Self(ClientSecret::new(secret)))
    }
}

impl<'de> Deserialize<'de> for CompressionWrapper {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let max_files: usize = usize::deserialize(deserializer)?;
        Ok(Self(Compression::OnRotate(max_files)))
    }
}

impl<'de> Deserialize<'de> for ContentLimitWrapper {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        #[allow(clippy::missing_docs_in_private_items)]
        struct ContentLimitVisitor;

        impl<'de> Visitor<'de> for ContentLimitVisitor {
            type Value = ContentLimitWrapper;

            fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
                formatter.write_str("a mapping")
            }

            fn visit_map<A>(self, mut map: A) -> Result<Self::Value, A::Error>
            where
                A: MapAccess<'de>,
            {
                let mut content_limit: ContentLimitWrapper = Self::Value::default();

                while let Some((key, value)) = map.next_entry::<String, serde_yaml::Value>()? {
                    match key.as_str() {
                        "bytes" => match usize::deserialize(value) {
                            Ok(limit) => {
                                content_limit = ContentLimitWrapper(ContentLimit::Bytes(limit));
                                dbg!(&content_limit);
                            }
                            Err(error) => {
                                let _ = writeln!(
                                    io::stderr(),
                                    "config error: logging.log_file.content_limit.bytes: {}",
                                    error
                                );
                            }
                        },
                        "lines" => match usize::deserialize(value) {
                            Ok(limit) => {
                                content_limit = ContentLimitWrapper(ContentLimit::Lines(limit));
                            }
                            Err(error) => {
                                let _ = writeln!(
                                    io::stderr(),
                                    "config error: logging.log_file.content_limit.lines: {}",
                                    error
                                );
                            }
                        },
                        "bytes_surpassed" => match usize::deserialize(value) {
                            Ok(limit) => {
                                content_limit =
                                    ContentLimitWrapper(ContentLimit::BytesSurpassed(limit));
                            }
                            Err(error) => {
                                let _ = writeln!(
                                    io::stderr(),
                                    "config error: \
                                     logging.log_file.content_limit.bytes_surpassed: {}",
                                    error
                                );
                            }
                        },
                        _ => {}
                    }
                }
                Ok(content_limit)
            }
        }

        deserializer.deserialize_map(ContentLimitVisitor)
    }
}

impl<'de> Deserialize<'de> for FilesizeWrapper {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let filesize: String = String::deserialize(deserializer)?;

        let bytes: i64 = parse_bytes(filesize.as_str()).unwrap_or_default();

        Ok(Self(bytes))
    }
}

impl<'de> Deserialize<'de> for HeaderNameWrapper {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let name: String = String::deserialize(deserializer)?;

        match HeaderName::from_str(name.as_str()) {
            Ok(name) => Ok(Self(name)),
            Err(error) => {
                let _ = writeln!(
                    io::stderr(),
                    "config error: header name is not valid: \"{}\"",
                    error
                );

                Err(Error::custom(""))
            }
        }
    }
}

impl<'de> Deserialize<'de> for HeaderValueWrapper {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let value: String = String::deserialize(deserializer)?;

        match HeaderValue::from_str(value.as_str()) {
            Ok(value) => Ok(Self(value)),
            Err(error) => {
                let _ = writeln!(
                    io::stderr(),
                    "config error: header value is not valud valid (reason: {})",
                    error
                );

                Err(Error::custom(""))
            }
        }
    }
}

impl<'de> Deserialize<'de> for PathbufWrapper {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let path: String = String::deserialize(deserializer)?;

        match shellexpand::full(path.as_str()) {
            Ok(path) => Ok(Self(std::path::PathBuf::from(path.to_string()))),
            Err(error) => {
                let _ = writeln!(
                    io::stderr(),
                    "could not shellexpand \"{}\": {}",
                    path,
                    error
                );

                Err(Error::custom(""))
            }
        }
    }
}

impl<'de> Deserialize<'de> for ProxyWrapper {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let proxy: String = String::deserialize(deserializer)?;

        match Proxy::all(proxy.as_str()) {
            Ok(proxy) => Ok(Self(proxy)),
            Err(error) => {
                let _ = writeln!(io::stderr(), "invalid proxy \"{}\": {}", proxy, error);

                Err(Error::custom(""))
            }
        }
    }
}

impl<'de> Deserialize<'de> for RedirectUrlWrapper {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let url: String = String::deserialize(deserializer)?;

        match RedirectUrl::new(url.clone()) {
            Ok(url) => Ok(Self(url)),
            Err(error) => {
                let _ = writeln!(
                    io::stderr(),
                    "invalid redirection url \"{}\": {}",
                    url,
                    error
                );

                Err(Error::custom(""))
            }
        }
    }
}

impl<'de> Deserialize<'de> for ScopeWrapper {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let scope: String = String::deserialize(deserializer)?;

        Ok(Self(Scope::new(scope)))
    }
}

impl<'de> Deserialize<'de> for TokenUrlWrapper {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let url: String = String::deserialize(deserializer)?;

        match TokenUrl::new(url.clone()) {
            Ok(url) => Ok(Self(url)),
            Err(error) => {
                let _ = writeln!(
                    io::stderr(),
                    "invalid token endpoint URL \"{}\": {}",
                    url,
                    error
                );

                Err(Error::custom(""))
            }
        }
    }
}

impl<'de> Deserialize<'de> for DateTimeWrapper {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        #[allow(clippy::missing_docs_in_private_items)]
        struct DateTimeVisitor;

        impl<'de> Visitor<'de> for DateTimeVisitor {
            type Value = DateTimeWrapper;

            fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
                writeln!(formatter, "a dict or an integer")
            }

            fn visit_u64<E>(self, v: u64) -> Result<Self::Value, E>
            where
                E: Error,
            {
                Ok(Self::Value::Utc(Utc.timestamp(v as i64, 0)))
            }

            fn visit_map<A>(self, mut map: A) -> Result<Self::Value, A::Error>
            where
                A: MapAccess<'de>,
            {
                let mut date_time: DateTimeWrapper = Self::Value::default();

                while let Some((key, value)) = map.next_entry::<String, String>()? {
                    match NaiveDateTime::parse_from_str(key.as_str(), value.as_str()) {
                        Ok(date) => {
                            date_time = DateTimeWrapper::Naive(date);
                            break;
                        }
                        Err(error) => {
                            let _ = writeln!(
                                io::stderr(),
                                "Config error: extractor.reddit.date_min/max: {}",
                                error
                            );
                        }
                    };
                }

                Ok(date_time)
            }
        }

        deserializer.deserialize_any(DateTimeVisitor)
    }
}

impl From<&str> for AuthUrlWrapper {
    fn from(item: &str) -> Self {
        Self(AuthUrl::new(item.to_owned()).expect("Invalid authorization endpoint URL"))
    }
}

impl From<&str> for ClientIdWrapper {
    fn from(item: &str) -> Self {
        Self(ClientId::new(item.to_owned()))
    }
}

impl From<&str> for ClientSecretWrapper {
    fn from(item: &str) -> Self {
        Self(ClientSecret::new(item.to_owned()))
    }
}

impl From<&str> for PathbufWrapper {
    fn from(item: &str) -> Self {
        Self(PathBuf::from(item))
    }
}

impl From<&str> for RedirectUrlWrapper {
    fn from(item: &str) -> Self {
        Self(RedirectUrl::new(item.to_owned()).expect("invalide redirect URL"))
    }
}

impl From<&str> for ScopeWrapper {
    fn from(item: &str) -> Self {
        Self(Scope::new(item.to_owned()))
    }
}

impl From<&str> for TokenUrlWrapper {
    fn from(item: &str) -> Self {
        Self(TokenUrl::new(item.to_owned()).expect("invalid token URL"))
    }
}

impl From<String> for PathbufWrapper {
    fn from(item: String) -> Self {
        Self(PathBuf::from(item))
    }
}
