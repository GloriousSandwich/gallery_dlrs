/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Configurations for `Gallery_dlrs`.

use alacritty_config_derive::ConfigDeserialize;

use crate::config::cache::CacheConfig;
use crate::config::colors::Colors;
use crate::config::downloader::DownloaderConfig;
use crate::config::extractor::ExtractorConfig;
use crate::config::logging::Logging;

/// Configurations for the User Interface.
#[derive(ConfigDeserialize, Clone, Debug, Default)]
pub struct UIConfig {
    /// Extractor configurations.
    pub extractor: ExtractorConfig,

    /// Downloader configurations.
    pub downloader: DownloaderConfig,

    /// Cache configuration.
    pub cache: CacheConfig,

    /// Logging configurations.
    pub logging: Logging,

    /// RGB values for colors
    pub colors: Colors,
}
