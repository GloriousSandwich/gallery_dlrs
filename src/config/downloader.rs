/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Downloader Configurations.

use alacritty_config_derive::ConfigDeserialize;
use reqwest::header::{HeaderName, HeaderValue};

use crate::config::de_wrappers::{FilesizeWrapper, HeaderNameWrapper, HeaderValueWrapper};

/// Downloader configurations.
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct DownloaderConfig {
    /// Minimum allowed file size in bytes.
    pub filesize_min: FilesizeWrapper,

    /// Maximum allowed file size in bytes.
    pub filesize_max: Option<FilesizeWrapper>,

    /// Controls the use of `.part` files during file downloads.
    pub part: bool,

    /// Connection timeout during file downloads.
    pub timeout: f64,

    /// Maximum number of retries during file downloads.
    pub retries: i64,

    /// Skip files already downloaded.
    pub skip: bool,

    /// Number of seconds to wait before downloading a URL.
    pub sleep: i64,

    /// HTTP configurations.
    pub http: Http,
}

/// HTTP configurations.
#[derive(ConfigDeserialize, Clone, Debug, Default)]
pub struct Http {
    /// HTTP headers.
    pub headers: Vec<Header>,
}

#[derive(ConfigDeserialize, Clone, Debug, Default)]
/// This struct represents HTTP headers.
pub struct Header {
    /// HTTP header name.
    pub name: Option<HeaderNameWrapper>,
    /// HTTP header value.
    pub value: Option<HeaderValueWrapper>,
}

impl Header {
    /// Returns the header name.
    pub fn name(&self) -> Option<&HeaderName> {
        match &self.name {
            Some(name) => Some(&name.0),
            None => None,
        }
    }

    /// Returns the header value.
    pub fn value(&self) -> Option<&HeaderValue> {
        match &self.value {
            Some(value) => Some(&value.0),
            None => None,
        }
    }
}

impl Default for DownloaderConfig {
    fn default() -> Self {
        Self {
            filesize_min: Default::default(),
            filesize_max: None,
            part: true,
            timeout: 30.0,
            retries: 4,
            skip: true,
            sleep: 0,
            http: Default::default(),
        }
    }
}
