/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Configuration for the `Gallery_dlrs` command line application

use std::ffi::OsStr;
use std::fmt::{Debug, Display, Formatter};
use std::io::Write;
use std::path::{Path, PathBuf};
use std::process::exit;
use std::{fs, io};

use log::{debug, error, warn};
use serde::Deserialize;
use serde_yaml::{Mapping, Value};
use xdg::BaseDirectories;

use crate::cli::CliArgs;
use crate::config::ui_config::UIConfig;

pub mod cache;
pub mod colors;
pub mod de_wrappers;
pub mod downloader;
pub mod extractor;
pub mod logging;
pub mod serde_utils;
pub mod ui_config;

/// Struct for handling the config file.
pub struct ConfigFile {
    /// Config file path.
    config_path: PathBuf,
    /// If config file path exists.
    config_path_exists: bool,
}

/// Errors that can occur during config loading.
#[derive(Debug)]
#[non_exhaustive]
pub enum Error {
    /// Config file not found.
    NotFound,
    /// IO error reading file.
    Io(io::Error),
    /// Not valid YAML or missing parameters.
    Yaml(serde_yaml::Error),
}

impl ConfigFile {
    /// Construct a new [`ConfigFile`].
    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        let xdg_dirs: BaseDirectories = BaseDirectories::with_prefix("gallery_dlrs")
            .unwrap_or_else(|error| {
                error!("Config: {}", error);
                exit(1);
            });

        let default_config_path: String =
            format!("{}/config/default.yaml", env!("CARGO_MANIFEST_DIR"));
        let default_config_path: &Path = Path::new(&default_config_path);
        let config_path: PathBuf = match xdg_dirs.place_config_file("gallery_dlrs.yaml") {
            Ok(file) => file,
            Err(error) => {
                warn!("Config: Cannot create config directory (reason: {})", error);
                warn!("Config: Reverting to default config");
                PathBuf::from(default_config_path)
            }
        };

        let config_path_exists: bool = config_path.is_file();
        Self {
            config_path,
            config_path_exists,
        }
    }

    /// Returns true if the config file exists.
    pub fn exists(&self) -> bool {
        self.config_path_exists
    }

    /// Create the config file.
    pub fn create(&self) -> io::Result<()> {
        fs::copy(
            Path::new(format!("{}/config/default.yaml", env!("CARGO_MANIFEST_DIR")).as_str()),
            Path::new(&self.config_path),
        )?;
        debug!("Config file created");
        Ok(())
    }

    /// Loads configurations from a config file. A custom file path can be
    /// specified via `custom_path`.
    pub fn load<P: ?Sized + AsRef<OsStr>>(
        &self,
        args: &CliArgs,
        custom_path: Option<&P>,
    ) -> UIConfig {
        let config_path: PathBuf = match custom_path {
            Some(path) => PathBuf::from(path),
            None => self.config_path.clone(),
        };

        let mut config: UIConfig = Self::load_from(&config_path, args.config_options.clone())
            .expect("Could not load config");

        let _ = writeln!(
            io::stdout(),
            "Configuration file loaded from: {}",
            config_path.display()
        );

        args.override_config(&mut config);
        config
    }

    #[allow(clippy::missing_docs_in_private_items)]
    fn load_from<P: AsRef<Path> + Debug>(path: &P, cli_config: Value) -> Result<UIConfig, Error> {
        match Self::read_config(path, cli_config) {
            Ok(config) => Ok(config),
            Err(error) => {
                error!("Unable to load config {:?}: (reason: {})", path, error);
                Err(error)
            }
        }
    }

    /// Deserialize the config file into `serde_yaml::Value`.
    fn parse_config_to_value<P: AsRef<Path>>(path: &P) -> Result<Value, Error> {
        let mut contents: String = fs::read_to_string(path)?;

        // Remove UTF-8 BOM.
        if contents.starts_with('\u{FEFF}') {
            contents = contents.split_off(3);
        }

        // Load configuration file as Value.
        let config: Value = match serde_yaml::from_str(&contents) {
            Ok(config) => config,
            Err(error) => {
                // Prevent parsing error with an empty string and commented out file.
                if error.to_string() == "EOF while parsing a value" {
                    Value::Mapping(Mapping::new())
                } else {
                    return Err(Error::Yaml(error));
                }
            }
        };

        Ok(config)
    }

    /// Deserialize the config file from `path`.
    fn read_config<P: AsRef<Path>>(path: &P, cli_config: Value) -> Result<UIConfig, Error> {
        let mut config_value: Value = Self::parse_config_to_value(path)?;

        // Override config with CLI config.
        config_value = serde_utils::merge(config_value, cli_config);

        let config: UIConfig = UIConfig::deserialize(config_value)?;
        Ok(config)
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::NotFound => write!(f, "Unable to locate config file"),
            Error::Io(err) => write!(f, "Error reading config file: {}", err),
            Error::Yaml(err) => write!(f, "Config error: {}", err),
        }
    }
}

impl From<io::Error> for Error {
    fn from(val: io::Error) -> Self {
        if val.kind() == io::ErrorKind::NotFound {
            Error::NotFound
        } else {
            Error::Io(val)
        }
    }
}

impl From<serde_yaml::Error> for Error {
    fn from(val: serde_yaml::Error) -> Self {
        Error::Yaml(val)
    }
}
