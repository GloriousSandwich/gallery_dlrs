//! Cache configuration.

use std::env;

use alacritty_config_derive::ConfigDeserialize;

use crate::config::de_wrappers::PathbufWrapper;

/// Cache configuration.
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct CacheConfig {
    /// Cache directory.
    pub directory: PathbufWrapper,
}

impl Default for CacheConfig {
    fn default() -> Self {
        let cache_env: String =
            env::var("XDG_CACHE_HOME").unwrap_or(format!("{}/.cache", env!("HOME")));
        let directory: PathbufWrapper = PathbufWrapper::from(format!("{}/gallery_dlrs", cache_env));

        Self { directory }
    }
}
