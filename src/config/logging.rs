/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Logging configurations.

use std::io;
use std::io::Write;

use alacritty_config_derive::ConfigDeserialize;
use log::LevelFilter;
use serde::Deserialize;

#[allow(unused_imports)]
use crate::config::de_wrappers::CompressionWrapper;
#[allow(unused_imports)]
use crate::config::de_wrappers::ContentLimitWrapper;
use crate::config::de_wrappers::PathbufWrapper;

/// Output Configuration.
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct Logging {
    /// Log level filter.
    pub log_level: LevelFilter,

    /// Console logging configuration.
    pub console: Console,

    /// Logfile configuration.
    pub log_file: LogfileConfig,
}

/// Console logging Configuration.
#[derive(ConfigDeserialize, Clone, Debug, Default)]
pub struct Console {
    /// The output stream to log to.
    pub stream: OutputStream,
}

/// Logfile configuration.
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct LogfileConfig {
    /// File to write logging output to.
    pub path: PathbufWrapper,

    /// Append a number when rotating the log file.
    pub max_files: usize,

    /// Conditions on which a file is rotated.
    pub content_limit: ContentLimitWrapper,

    /// When to compress older files.
    pub compression: CompressionWrapper,
}

/// The output stream to log to.
#[derive(Deserialize, Clone, Debug)]
pub enum OutputStream {
    /// Standard Output.
    Stdout,
    /// Standard Error.
    Stderr,
}

impl Default for Logging {
    fn default() -> Self {
        Self {
            log_level: LevelFilter::Info,
            console: Default::default(),
            log_file: Default::default(),
        }
    }
}

impl Default for LogfileConfig {
    fn default() -> Self {
        Self {
            path: PathbufWrapper::from("/tmp/gallery_dlrs.log"),
            max_files: 3,
            content_limit: Default::default(),
            compression: Default::default(),
        }
    }
}

impl Default for OutputStream {
    fn default() -> Self {
        OutputStream::Stdout
    }
}

impl Write for OutputStream {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        match self {
            OutputStream::Stdout => io::stdout().write(buf),
            OutputStream::Stderr => io::stderr().write(buf),
        }
    }

    fn flush(&mut self) -> io::Result<()> {
        match self {
            OutputStream::Stdout => io::stdout().flush(),
            OutputStream::Stderr => io::stderr().flush(),
        }
    }
}
