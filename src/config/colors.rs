/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Color types and specifications.

use alacritty_config_derive::ConfigDeserialize;
use colored::Color;

#[derive(ConfigDeserialize, Clone, Debug, Default)]
/// Manage colors for gallery_dlrs.
pub struct Colors {
    /// Colors for logging
    pub logging: LoggingColors,
}

/// Colors for logging.
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct LoggingColors {
    /// Output color for the trace log filter
    pub trace: Color,
    /// Output color for the info log filter
    pub info: Color,
    /// Output color for the debug log filter
    pub debug: Color,
    /// Output color for the warn log filter
    pub warn: Color,
    /// Output color for the error log filter
    pub error: Color,
}

impl Default for LoggingColors {
    fn default() -> Self {
        Self {
            trace: Color::White,
            info: Color::White,
            debug: Color::White,
            warn: Color::White,
            error: Color::White,
        }
    }
}
