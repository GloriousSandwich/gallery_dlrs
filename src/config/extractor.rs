/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Extractor configurations.
use alacritty_config_derive::ConfigDeserialize;
use chrono::{TimeZone, Utc};
use gallery_dlrs_config_common_fields_macro::config_common_fields;
use oauth2::{AccessToken, RefreshToken};

use crate::config::de_wrappers::{
    AuthUrlWrapper, ClientIdWrapper, ClientSecretWrapper, DateTimeWrapper, PathbufWrapper,
    ProxyWrapper, RedirectUrlWrapper, ScopeWrapper, TokenUrlWrapper,
};

/// Extractor configuration.
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct ExtractorConfig {
    /// Directory path used as a base for all download destinations.
    pub base_directory: PathbufWrapper,

    /// Controls the behavior when downloading files that have been
    /// downloaded before.
    ///
    /// # Special values
    /// * `true`: Skip downloads.
    /// * `false`: Overwrite already exists files.
    pub skip: bool,

    /// Number of worker threads used for extracting images/videos.
    pub threads: i64,

    /// User-Agent header value to be used for HTTP requests.
    pub user_agent: String,

    /// Maximum number of times a failed HTTP request is retried before
    /// giving up, or `-1` for infinite retries.
    pub retries: i64,

    /// Amount of time (in seconds) to wait for a successful connection
    /// and response from a remote server.
    pub timeout: f64,

    /// Use fallback download URLs when a download fails.
    pub fallback: bool,

    /// Number of seconds to sleep before each download.
    pub sleep: i64,

    /// Number of seconds to sleep before handling an input URL,
    /// i.e., before starting a new extractor.
    pub sleep_extractor: i64,

    /// Additional cookies.
    pub cookies: Option<Vec<Cookie>>,

    /// Proxy used for remote connections.
    pub proxy: Option<ProxyWrapper>,

    /// Configurations for Artstation.
    pub artstation: Artstation,

    /// Configurations for Imgur.
    pub imgur: Imgur,

    /// Configurations for Reddit.
    pub reddit: Reddit,

    /// Configurations for Webtoons.
    pub webtoons: Webtoons,
}

/// A single HTTP cookie.
#[derive(ConfigDeserialize, Clone, Debug, Default)]
pub struct Cookie {
    /// Cookie name.
    pub name: String,

    /// Cookie value.
    pub value: String,
}

/// Artstation configuration.
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct Artstation {
    /// Number of pages to extract projects from Artstation.
    pub depth: i64,

    /// Artstation extractors.
    pub sub_extractors: ArtstationExtractors,
}

/// Artstation sub-extractors configuration.
#[derive(ConfigDeserialize, Clone, Debug, Default)]
pub struct ArtstationExtractors {
    /// Artstation album extractor configuration.
    pub album: ArtstationAlbum,

    /// Artstation artwork extractor configuration.
    pub artwork: ArtstationArtwork,

    /// Artstation challenge extractor configuration.
    pub challenge: ArtstationChallenge,

    /// Artstation following extractor configuration.
    pub following: ArtstationFollowing,

    /// Artstation image extractor configuration.
    pub image: ArtstationImage,

    /// Artstation likes extractor configuration.
    pub likes: ArtstationLikes,

    /// Artstation search extractor configuration.
    pub search: ArtstationSearch,

    /// Artstation user extractor configuration.
    pub user: ArtstationUser,
}

/// Artstation album extractor configuration.
#[config_common_fields]
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct ArtstationAlbum {}

/// Artstation artwork extractor configuration.
#[config_common_fields]
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct ArtstationArtwork {}

/// Artstation challenge extractor configuration.
#[config_common_fields]
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct ArtstationChallenge {}

/// Artstation following extractor configuration.
#[config_common_fields]
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct ArtstationFollowing {}

/// Artstation image extractor configuration.
#[config_common_fields]
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct ArtstationImage {}

/// Artstation likes extractor configuration.
#[config_common_fields]
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct ArtstationLikes {}

/// Artstation search extractor configuration.
#[config_common_fields]
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct ArtstationSearch {}

/// Artstation user extractor configuration.
#[config_common_fields]
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct ArtstationUser {}

/// Imgur configuration.
#[derive(ConfigDeserialize, Clone, Debug, Default)]
#[non_exhaustive]
pub struct Imgur {
    /// Oauth2 configuration.
    pub oauth2: ImgurOauth2,

    /// Imgur extractors configuration.
    pub(crate) sub_extractors: ImgurExtractors,
}

/// Imgur extractors configuration.
#[derive(ConfigDeserialize, Clone, Debug, Default)]
pub(crate) struct ImgurExtractors {
    /// Imgur image extractor configuration.
    pub(crate) image: ImgurImage,
}

/// Imgur image extractor configuration.
#[config_common_fields]
#[derive(ConfigDeserialize, Clone, Debug)]
pub(crate) struct ImgurImage {}

/// Imgur Oauth2 configuration.
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct ImgurOauth2 {
    /// The authorization server.
    pub auth_url: AuthUrlWrapper,

    /// Tells imgur which app is making the request.
    pub client_id: ClientIdWrapper,

    /// URL used to retrieve the access token.
    pub token_url: TokenUrlWrapper,

    /// URL used to redirect to if authorization succeeds.
    pub redirect_url: RedirectUrlWrapper,

    /// Imgur's Oauth2 access token.
    #[config(skip)]
    pub(crate) access_token: Option<AccessToken>,

    /// Imgur's Oauth2 refresh token.
    #[config(skip)]
    pub(crate) refresh_token: Option<RefreshToken>,
}

/// Reddit configuration.
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct Reddit {
    /// Oauth2 configuration.
    pub oauth2: RedditOauth2,

    /// Ignore all submissions posted before this date.
    pub date_min: DateTimeWrapper,

    /// Ignore all submissions posted after this date.
    pub date_max: DateTimeWrapper,

    /// Ignore all submissions posted before the submission with this ID.
    pub id_min: i64,

    /// Ignore all submissions posted after the submission with this ID.
    pub id_max: Option<i64>,

    /// Maximum recursion depth.
    pub recursion: i64,

    /// Controls video download behavior.
    pub videos: bool,

    /// Reddit subextractor configuration.
    pub sub_extractors: RedditExtractors,
}

/// Oauth2 configuration for reddit.
#[derive(ConfigDeserialize, Clone, Debug)]
#[non_exhaustive]
pub struct RedditOauth2 {
    /// The authorization server.
    pub auth_url: AuthUrlWrapper,

    /// Tells reddit which app is making the request
    pub client_id: ClientIdWrapper,

    /// A secret only known to the client and authorization server.
    pub client_secret: Option<ClientSecretWrapper>,

    /// Use a permanent or bearer token.
    pub duration: String,

    /// URL used to redirect to if authorization succeeds.
    pub redirect_url: RedirectUrlWrapper,

    /// Reqwest access to areas of the reddit API.
    pub scope: Vec<ScopeWrapper>,

    /// URL used to retrieve the access token.
    pub token_url: TokenUrlWrapper,

    /// User agent used for the client.
    pub user_agent: String,

    /// Reddit's Oauth2 access token.
    #[config(skip)]
    pub access_token: Option<AccessToken>,

    /// Reddit's Oauth2 refresh token.
    #[config(skip)]
    pub refresh_token: Option<RefreshToken>,
}

/// Reddit extractor configurations.
#[derive(ConfigDeserialize, Clone, Debug, Default)]
pub struct RedditExtractors {
    /// Reddit home image extractor.
    pub home: RedditHome,

    /// Reddit image extractor configuration.
    pub image: RedditImage,

    /// Reddit submission extractor configuration.
    pub submission: RedditSubmission,

    /// Reddit subreddit extractor configuration.
    pub subreddit: RedditSubreddit,

    /// Reddit user extractor configuration.
    pub user: RedditUser,
}

/// Reddit home extractor configuration.
#[config_common_fields]
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct RedditHome {}

/// Reddit image extractor configuration.
#[config_common_fields]
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct RedditImage {}

/// Reddit submission extractor configuration.
#[config_common_fields]
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct RedditSubmission {}

/// Reddit subreddit extractor configuration.
#[config_common_fields]
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct RedditSubreddit {}

/// Reddit user extractor configuration.
#[config_common_fields]
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct RedditUser {}

/// Webtoons configuration.
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct Webtoons {
    /// Number of pages to extract episodes from Webtoons.
    pub depth: i64,

    /// Webtoons extractor configurations.
    pub sub_extractors: WebtoonsExtractors,
}

/// Webtoons extractor configurations.
#[derive(ConfigDeserialize, Clone, Debug, Default)]
pub struct WebtoonsExtractors {
    /// Webtoons episode extractor configuration.
    pub episode: WebtoonsEpisode,

    /// Webtoons comic extractor configuration.
    pub comic: WebtoonsComic,
}

/// Webtoons comic extractor configuration.
#[config_common_fields]
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct WebtoonsComic {}

/// Webtoons episode extractor configuration.
#[config_common_fields]
#[derive(ConfigDeserialize, Clone, Debug)]
pub struct WebtoonsEpisode {}

/// Default directory format for artstation extractors.
const ARTSTATION_DEFAULT_DIRECTORY_FORMAT: &str = "artstation/{user[username]}";

/// Default filename format for artstation extractors.
const ARTSTATION_DEFAULT_FILENAME_FORMAT: &str = "{category}_{id}_{title}.{extension}";

/// Default directory format for imgur extractors.
const IMGUR_DEFAULT_DIRECTORY_FORMAT: &str = "imgur";

/// Default filename format for imgur extractors.
const IMGUR_DEFAULT_FILENAME_FORMAT: &str = "{id}_{title:?_//}.{extension}";

/// Default directory format for reddit extractors.
const REDDIT_DEFAULT_DIRECTORY_FORMAT: &str = "reddit/{subreddit}";

/// Default filename format for reddit extractors.
const REDDIT_DEFAULT_FILENAME_FORMAT: &str = "{id}{num:? //>02} {title[:220]}.{extension}";

/// Default directory format for webtoons extractors.
const WEBTOONS_DEFAULT_DIRECTORY_FORMAT: &str = "webtoons/{comic}";

/// Default filename format for webtoons extractors.
const WEBTOONS_DEFAULT_FILENAME_FORMAT: &str = "{episode_no}-{image_no:>02}.{extension}";

impl RedditOauth2 {
    /// Returns the access_token.
    pub fn access_token(&self) -> AccessToken {
        self.access_token.clone().expect("No access token found")
    }

    /// Returns the refresh_token.
    pub fn refresh_token(&self) -> RefreshToken {
        self.refresh_token.clone().expect("No refresh token found")
    }
}

impl Default for ExtractorConfig {
    fn default() -> Self {
        Self {
            base_directory: PathbufWrapper::from("./gallery-dlrs/"),
            skip: true,
            threads: 4,
            user_agent: "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 \
                         Firefox/91.0"
                .to_string(),
            retries: 4,
            timeout: 30.0,
            fallback: true,
            sleep: 0,
            sleep_extractor: 0,
            cookies: Default::default(),
            proxy: Default::default(),
            artstation: Default::default(),
            imgur: Default::default(),
            reddit: Default::default(),
            webtoons: Default::default(),
        }
    }
}

impl Default for Artstation {
    fn default() -> Self {
        Self {
            depth: 2,
            sub_extractors: Default::default(),
        }
    }
}

impl Default for ArtstationAlbum {
    fn default() -> Self {
        Self {
            directory_format: ARTSTATION_DEFAULT_DIRECTORY_FORMAT.to_string()
                + "/Albums/{album_id} - {album_title}",
            filename_format: ARTSTATION_DEFAULT_FILENAME_FORMAT.to_string(),
        }
    }
}

impl Default for ArtstationArtwork {
    fn default() -> Self {
        Self {
            directory_format: "artstation/Artworks/{query}".to_owned(),
            filename_format: ARTSTATION_DEFAULT_FILENAME_FORMAT.to_string(),
        }
    }
}

impl Default for ArtstationChallenge {
    fn default() -> Self {
        Self {
            directory_format: "artstation/Challenges/{challenge_id} - {challenge_title}".to_owned(),
            filename_format: ARTSTATION_DEFAULT_FILENAME_FORMAT.to_string(),
        }
    }
}

impl Default for ArtstationFollowing {
    fn default() -> Self {
        Self {
            directory_format: ARTSTATION_DEFAULT_DIRECTORY_FORMAT.to_string(),
            filename_format: ARTSTATION_DEFAULT_FILENAME_FORMAT.to_string(),
        }
    }
}

impl Default for ArtstationImage {
    fn default() -> Self {
        Self {
            directory_format: ARTSTATION_DEFAULT_DIRECTORY_FORMAT.to_string(),
            filename_format: ARTSTATION_DEFAULT_FILENAME_FORMAT.to_string(),
        }
    }
}

impl Default for ArtstationLikes {
    fn default() -> Self {
        Self {
            directory_format: ARTSTATION_DEFAULT_DIRECTORY_FORMAT.to_string() + "/Likes",
            filename_format: ARTSTATION_DEFAULT_FILENAME_FORMAT.to_string(),
        }
    }
}

impl Default for ArtstationSearch {
    fn default() -> Self {
        Self {
            directory_format: "artstation/Searches/{query}".to_owned(),
            filename_format: ARTSTATION_DEFAULT_FILENAME_FORMAT.to_string(),
        }
    }
}

impl Default for ArtstationUser {
    fn default() -> Self {
        Self {
            directory_format: ARTSTATION_DEFAULT_DIRECTORY_FORMAT.to_string(),
            filename_format: ARTSTATION_DEFAULT_FILENAME_FORMAT.to_string(),
        }
    }
}

impl Default for ImgurImage {
    fn default() -> Self {
        Self {
            directory_format: IMGUR_DEFAULT_DIRECTORY_FORMAT.to_string(),
            filename_format: IMGUR_DEFAULT_FILENAME_FORMAT.to_string(),
        }
    }
}

impl Default for ImgurOauth2 {
    fn default() -> Self {
        Self {
            auth_url: AuthUrlWrapper::from("https://api.imgur.com/oauth2/authorize"),
            client_id: ClientIdWrapper::from("0ac03803bcc3cc0"),
            token_url: TokenUrlWrapper::from("https://api.imgur.com/oauth2/token"),
            redirect_url: RedirectUrlWrapper::from("http://127.0.0.1:6614"),
            access_token: Default::default(),
            refresh_token: Default::default(),
        }
    }
}

impl Default for Reddit {
    fn default() -> Self {
        Self {
            oauth2: Default::default(),
            date_min: Default::default(),
            date_max: DateTimeWrapper::Utc(Utc.timestamp(253402210800, 0)),
            id_min: Default::default(),
            id_max: Default::default(),
            recursion: Default::default(),
            videos: true,
            sub_extractors: Default::default(),
        }
    }
}

impl Default for RedditOauth2 {
    fn default() -> Self {
        Self {
            auth_url: AuthUrlWrapper::from("https://www.reddit.com/api/v1/authorize"),
            client_id: ClientIdWrapper::from("6N9uN0krSDE-ig"),
            client_secret: Some(ClientSecretWrapper::from("ffK7-rPpK07C-UZmwHQUsUQvPnk7UA")),
            duration: "permanent".to_string(),
            redirect_url: RedirectUrlWrapper::from("http://127.0.0.1:6614"),
            scope: vec![ScopeWrapper::from("read"), ScopeWrapper::from("history")],
            token_url: TokenUrlWrapper::from("https://www.reddit.com/api/v1/access_token"),
            user_agent: "Rust:gallery-dlrs".to_string(),
            access_token: Some(AccessToken::new(
                "1315056491297-oZGK3seSIQFoHM6DTEtNO2xPk9JvSQ".to_owned(),
            )),
            refresh_token: Some(RefreshToken::new(
                "1315056491297-bH7DrMrtnWUzww6ghUju8n_9yFJKSg".to_string(),
            )),
        }
    }
}

impl Default for RedditHome {
    fn default() -> Self {
        Self {
            directory_format: REDDIT_DEFAULT_DIRECTORY_FORMAT.to_string(),
            filename_format: REDDIT_DEFAULT_FILENAME_FORMAT.to_string(),
        }
    }
}

impl Default for RedditImage {
    fn default() -> Self {
        Self {
            directory_format: "reddit".to_owned(),
            filename_format: "{filename}.{extension}".to_owned(),
        }
    }
}

impl Default for RedditSubmission {
    fn default() -> Self {
        Self {
            directory_format: REDDIT_DEFAULT_DIRECTORY_FORMAT.to_string(),
            filename_format: REDDIT_DEFAULT_FILENAME_FORMAT.to_string(),
        }
    }
}

impl Default for RedditSubreddit {
    fn default() -> Self {
        Self {
            directory_format: REDDIT_DEFAULT_DIRECTORY_FORMAT.to_string(),
            filename_format: REDDIT_DEFAULT_FILENAME_FORMAT.to_string(),
        }
    }
}

impl Default for RedditUser {
    fn default() -> Self {
        Self {
            directory_format: REDDIT_DEFAULT_DIRECTORY_FORMAT.to_string(),
            filename_format: REDDIT_DEFAULT_FILENAME_FORMAT.to_string(),
        }
    }
}

impl Default for Webtoons {
    fn default() -> Self {
        Self {
            depth: 2,
            sub_extractors: Default::default(),
        }
    }
}

impl Default for WebtoonsComic {
    fn default() -> Self {
        Self {
            directory_format: WEBTOONS_DEFAULT_DIRECTORY_FORMAT.to_string(),
            filename_format: WEBTOONS_DEFAULT_FILENAME_FORMAT.to_string(),
        }
    }
}

impl Default for WebtoonsEpisode {
    fn default() -> Self {
        Self {
            directory_format: WEBTOONS_DEFAULT_DIRECTORY_FORMAT.to_string(),
            filename_format: WEBTOONS_DEFAULT_FILENAME_FORMAT.to_string(),
        }
    }
}
