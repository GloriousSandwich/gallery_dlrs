/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Oauth API implementation.
use std::fmt::Display;
use std::io::{BufRead, BufReader, Write};
use std::net::{AddrParseError, SocketAddr, TcpListener, TcpStream};
use std::str::FromStr;

use log::{debug, error, info};
use oauth2::basic::BasicClient;
use oauth2::http::{HeaderMap, Method};
use oauth2::reqwest::http_client;
use oauth2::{
    AccessToken, AuthUrl, AuthorizationCode, AuthorizationRequest, Client, ClientId, ClientSecret,
    CsrfToken, ErrorResponse, HttpRequest, HttpResponse, PkceCodeChallenge, PkceCodeVerifier,
    RedirectUrl, RefreshToken, ResponseType, RevocableToken, TokenIntrospectionResponse,
    TokenResponse, TokenType, TokenUrl,
};
use reqwest::header::AUTHORIZATION;
use url::Url;

use crate::config::extractor::RedditOauth2;

impl RedditOauth2 {
    /// Authenticates a reddit user with oauth2.
    pub fn authenticate(&mut self) {
        // Create an Oauth2 client.
        let client_id: ClientId = self.client_id.client_id();
        let auth_url: AuthUrl = self.auth_url.auth_url();
        let token_url: TokenUrl = self.token_url.token_url();
        let redirect_uri: RedirectUrl = self.redirect_url.redirect_url();
        let client_secret: Option<ClientSecret> = self
            .client_secret
            .as_ref()
            .map(|secret| secret.client_secret());

        let client = BasicClient::new(client_id, client_secret, auth_url, Some(token_url))
            .set_redirect_uri(redirect_uri);

        // Generate a PKCE challenge.
        let (pkce_challenge, pkce_verifier) = PkceCodeChallenge::new_random_sha256();

        // Build the full authorization URL.
        let mut builder: AuthorizationRequest = client.authorize_url(CsrfToken::new_random);
        for scope in self.scope.iter() {
            builder = builder.add_scope(scope.clone().into_inner());
        }

        let (mut auth_url, csrf_token) = builder.set_pkce_challenge(pkce_challenge).url();
        auth_url = self.add_url_queries(auth_url);

        info!(
            "Make sure you're logged into reddit before opening this URL in your browser: {}",
            auth_url.to_string()
        );

        self.redirect_server(&client, &csrf_token, pkce_verifier);
    }

    /// Adds custom URL queries for reddit authorization.
    fn add_url_queries(&self, mut url: Url) -> Url {
        url.query_pairs_mut()
            .append_pair("duration", self.duration.as_str());

        url
    }

    /// Redirects the user to the `redirect_uri` after login in.
    fn redirect_server<TE, TIR, TT, TR, RT, TRE>(
        &mut self,
        client: &Client<TE, TR, TT, TIR, RT, TRE>,
        csrf_token: &CsrfToken,
        pkce_verifier: PkceCodeVerifier,
    ) where
        TE: ErrorResponse + 'static,
        TR: TokenResponse<TT>,
        TT: TokenType,
        TIR: TokenIntrospectionResponse<TT>,
        RT: RevocableToken,
        TRE: ErrorResponse + 'static,
    {
        let redirect_uri: &Url = self.redirect_url.url();
        let address: SocketAddr =
            url_to_sockaddr(redirect_uri).expect("Could not convert redirect_uri to a sockaddr");
        let listener: TcpListener = TcpListener::bind(address).expect("Failed to bind to sockaddr");

        #[allow(clippy::never_loop)]
        for mut stream in listener.incoming().flatten() {
            let code: AuthorizationCode;
            let state: CsrfToken;
            {
                let mut reader: BufReader<&TcpStream> = BufReader::new(&stream);

                let mut request_line: String = String::new();
                #[allow(clippy::unwrap_used)]
                reader.read_line(&mut request_line).unwrap();

                #[allow(clippy::unwrap_used)]
                let redirect_uri: &str = request_line.split_whitespace().nth(1).unwrap();
                let url: String = format!("{}{}", self.redirect_url.as_str(), redirect_uri);
                let url: Url = Url::parse(&url).unwrap_or_else(|_| {
                    panic!("Failed to parse \"{}\" as a URL record", url.as_str())
                });

                let code_pair = url
                    .query_pairs()
                    .find(|pair| {
                        let &(ref key, _) = pair;
                        key == "code"
                    })
                    .expect("URL does not contain the query `code`");

                let (_, value) = code_pair;
                code = AuthorizationCode::new(value.into_owned());

                let state_pair = url
                    .query_pairs()
                    .find(|pair| {
                        let &(ref key, _) = pair;
                        key == "state"
                    })
                    .expect("URL does not contain the query `state`");

                let (_, value) = state_pair;
                state = CsrfToken::new(value.into_owned());
            }

            let message: &str = "Go back to your terminal :)";
            let response: String = format!(
                "HTTP/1.1 200 OK\r\ncontent-length: {}\r\n\r\n{}",
                message.len(),
                message
            );
            stream
                .write_all(response.as_bytes())
                .expect("Failed to write to stream");

            debug!("Reddit returned the following code: {}", code.secret());
            debug!(
                "Reddit returned the following state: {} (expected `{}`)",
                state.secret(),
                csrf_token.secret()
            );

            let secret: ClientSecret = self
                .client_secret
                .clone()
                .unwrap_or_else(|| panic!("Client secret missing"))
                .client_secret();

            // Reddit returns the error code 500 if the redirect uri ends with `/`.
            let redirect_uri: &str = redirect_uri
                .as_str()
                .strip_suffix('/')
                .unwrap_or(redirect_uri.as_str());

            let request_body: Vec<u8> = format!(
                "grant_type=authorization_code&code={}&redirect_uri={}",
                code.secret(),
                redirect_uri
            )
            .into_bytes();

            let client_id: String = self.client_id.as_str().to_owned();

            // Exchange the code with a token.
            let token_result = client
                .exchange_code(code)
                .set_pkce_verifier(pkce_verifier)
                .request(|request| {
                    execute(client_id, secret.secret().clone(), request, request_body)
                });

            if token_result.is_err() {
                error!("Reddit returned the following token: {:?}", token_result);
            } else {
                debug!("Reddit returned the following token: {:?}", token_result);
            }

            if let Ok(token) = token_result {
                self.access_token = Some(token.access_token().clone());
                self.refresh_token = Some(
                    token
                        .refresh_token()
                        .expect("No refresh token found")
                        .clone(),
                );
            }

            // The server will terminate itself after collecting the first code.
            break;
        }
    }
}

/// Creates authorization headers.
#[allow(clippy::missing_panics_doc)]
pub fn authorize_headers<CI, CS>(client_id: CI, client_secret: CS) -> HeaderMap
where
    CI: Display,
    CS: Display,
{
    // Encode client ID and client secret in base64.
    let secret: String = base64::encode(format!("{}:{}", client_id, client_secret));
    let mut headers: HeaderMap = HeaderMap::new();

    #[allow(clippy::unwrap_used)]
    headers.append(
        AUTHORIZATION,
        ("Basic ".to_owned() + secret.as_str()).parse().unwrap(),
    );

    headers
}

/// Adds custom HTTP headers and body before calling
/// `oauth2::reqwest::http_client`.
fn execute<CI, CS>(
    client_id: CI,
    client_secret: CS,
    mut request: HttpRequest,
    body: Vec<u8>,
) -> Result<HttpResponse, impl std::error::Error>
where
    CI: Display,
    CS: Display,
{
    request.headers = authorize_headers(client_id, client_secret);
    request.method = Method::POST;
    request.body = body;

    http_client(request)
}

/// Converts a [`Url`] to a [`SocketAddr`].
fn url_to_sockaddr(url: &Url) -> Result<SocketAddr, AddrParseError> {
    let mut url: String = url.to_string();

    if url.starts_with("http://") {
        url = url[7..].to_owned();
    } else if url.starts_with("https://") {
        url = url[8..].to_owned();
    }

    // Remove everything after the port number.
    if let Some(index) = url.find('/') {
        url = url[..index].to_owned();
    }

    SocketAddr::from_str(url.as_str())
}
