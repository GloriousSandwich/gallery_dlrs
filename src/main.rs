/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Command-line program to download images and collections from several
//! image hosting sites.

use std::ffi::OsStr;
use std::io::Write;
use std::path::Path;
use std::process::exit;
use std::{io, panic};

use log::{debug, info, trace, warn};

use gallery_dlrs::app::{App, JobKind};
use gallery_dlrs::cli::{generate_manpage, CliArgs};
use gallery_dlrs::config::extractor::ExtractorConfig;
use gallery_dlrs::config::ui_config::UIConfig;
use gallery_dlrs::config::ConfigFile;
use gallery_dlrs::extractor::{list_extractors, list_modules};
use gallery_dlrs::logging;

fn main() {
    attatch_hook();

    let args: CliArgs = CliArgs::new();
    if let Err(error) = generate_manpage() {
        let _ = writeln!(io::stderr(), "Failed to generate manpage: {}", error);
    };

    gallery_dlrs(args);
}

/// Runs main Gallery_dlrs entrypoint.
fn gallery_dlrs(args: CliArgs) {
    // Load configuration file.
    let config_file: ConfigFile = ConfigFile::new();
    let mut use_default_config: bool = false;
    if !config_file.exists() {
        if let Err(error) = config_file.create() {
            eprintln!("Could not create config file (reason: {})", error);
            use_default_config = true;
        }
    }
    let mut config: UIConfig = if args.ignore_config || use_default_config {
        let default_config_path: String =
            format!("{}/config/default.yaml", env!("CARGO_MANIFEST_DIR"));
        let default_config_path: &Path = Path::new(default_config_path.as_str());
        config_file.load(&args, Some(default_config_path))
    } else {
        config_file.load(&args, None::<&str>)
    };

    // Initialize logger.
    logging::initialize(&config).expect("Unable to initialize logger");

    info!("Welcome to Gallery_dlrs");
    info!("Version {}", env!("CARGO_PKG_VERSION"));

    if args.list_extractors {
        list_extractors();
        exit(0);
    } else if args.list_modules {
        list_modules();
        exit(0);
    }

    let mut job_kind: JobKind = JobKind::default();

    if args.get_urls {
        job_kind = JobKind::Url;
    } else if args.list_keywords {
        job_kind = JobKind::Keyword;
    }

    debug!("Job selected: {}", job_kind);

    // Handles user authentication via oauth.
    if let Some(website) = args.oauth {
        let config: &mut ExtractorConfig = &mut config.extractor;
        match website.as_str() {
            "reddit" => config.reddit.oauth2.authenticate(),
            _ => warn!("\"{}\" not supported for oauth", website),
        }
    }

    // Initialize and run the main loop.
    #[allow(clippy::unwrap_used)]
    let (mut app, runtime) = App::new(
        config,
        job_kind,
        args.url
            .expect(
                "No
    url found",
            )
            .as_str(),
    )
    .unwrap();

    app.run(&runtime);

    info!("Goodbye");
}

/// Register a custom panic hook.
fn attatch_hook() {
    // Exit program if a thread panics.
    let orig_hook = panic::take_hook();
    panic::set_hook(Box::new(move |panic_info| {
        // invoke the default handler and exit the process
        orig_hook(panic_info);
        exit(1);
    }));
}
