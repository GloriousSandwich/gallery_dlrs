/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Core types and functions.

use std::fmt::{Display, Formatter};
use std::io::Write;
use std::path::Path;
use std::process::exit;
use std::sync::Arc;
use std::{fs, io, thread};

use log::{debug, error, info, trace, warn};
use pyo3::{PyObject, Python};
use reqwest::Client;
use serde_json::Value;
use tokio::runtime::{Builder, Runtime};

use crate::config::ui_config::UIConfig;
use crate::downloader::http::HttpDownloader;
use crate::downloader::ytdl::YoutubeDL;
use crate::downloader::{DownloadStatus, Downloader, DownloaderKind};
use crate::extractor::artstation::common::ImageFallbackUrl;
use crate::extractor::message::Message;
use crate::extractor::{match_extractor, Error, Extractor, ExtractorKind, SubExtractorKind};
use crate::init_request::{initialize_reqwest_downloader, initialize_reqwest_extractor};
use crate::path::PathFormatter;
use crate::python;

/// Core types used for running Gallery_dlrs.
pub struct App {
    /// Type of extractor.
    pub extractor: Arc<dyn Extractor>,

    /// Responsible for creating and formatting files.
    pub path_formatter: PathFormatter,

    /// A custom reqwest client for downloaders only.
    downloader_client: Client,

    /// A custom reqwest client for extractors only.
    extractor_client: Client,

    /// Type of job.
    job_kind: JobKind,

    /// App configurations.
    config: UIConfig,

    /// Vec of URLs that has been queued.
    urls_visited: Vec<String>,
}

/// Each type of job.
#[derive(Clone, Copy)]
#[non_exhaustive]
pub enum JobKind {
    /// [`DownloadJob`]
    Download,

    /// [`UrlJob`]
    Url,

    /// [`KeywordJob`].
    Keyword,
}

impl App {
    /// Creates a new ['App'] and a custom tokio runtime.
    pub fn new(config: UIConfig, job_kind: JobKind, url: &str) -> Result<(Self, Runtime), Error> {
        let extractor: Arc<dyn Extractor> = match_extractor(&config, url)?;

        let path_formatter: PathFormatter = PathFormatter::init(&config, &extractor);

        let extractor_kind: ExtractorKind = extractor.get_category();

        let downloader_client: Client =
            initialize_reqwest_downloader(&config, url, &extractor_kind);

        let extractor_client: Client = initialize_reqwest_extractor(&config, url, &extractor_kind);

        let thread_name: String = extractor.get_subextractor().to_string();

        trace!("Thread name: {}", &thread_name);

        let runtime: Runtime =
            initialize_tokio_runtime(config.extractor.threads as usize, thread_name);

        Ok((
            Self {
                extractor,
                path_formatter,
                downloader_client,
                extractor_client,
                job_kind,
                config,
                urls_visited: Default::default(),
            },
            runtime,
        ))
    }

    /// Starts the main loop.
    pub fn run(&mut self, runtime: &Runtime) {
        runtime.handle().block_on(async {
            let metadata: Vec<Value> = self.extractor.get_metadata(self.client().clone()).await;
            let items: Vec<(Message, Vec<Value>)> = self.extractor.items(metadata);

            trace!("Number of items: {}", items.len());

            let sub_category: SubExtractorKind = self.extractor.get_subextractor();

            match &self.job_kind {
                JobKind::Download => {
                    for (message, metadata) in items.into_iter() {
                        self.dispatch_download(&sub_category, message, metadata)
                            .await;
                    }
                }
                JobKind::Url => {
                    for (message, metadata) in items.into_iter() {
                        Self::dispatch_url(&sub_category, message, &metadata);
                    }
                }
                JobKind::Keyword => {
                    for (message, ref mut metadata) in items.into_iter() {
                        Self::dispatch_keyword(&sub_category, message, &mut metadata[0]);
                    }
                }
            }
        });
    }

    /// Returns the extractor's reqwest client.
    pub fn client(&self) -> &Client {
        &self.extractor_client
    }

    /// Downloads and saves urls to disk.
    async fn dispatch_download(
        &mut self,
        sub_category: &SubExtractorKind,
        message: Message,
        mut metadata: Vec<Value>,
    ) {
        match message {
            Message::Url => match sub_category {
                SubExtractorKind::ArtstationFollowing
                | SubExtractorKind::ArtstationArtwork
                | SubExtractorKind::ArtstationAlbum
                | SubExtractorKind::ArtstationUser
                | SubExtractorKind::ArtstationLikes
                | SubExtractorKind::ArtstationImage
                | SubExtractorKind::ArtstationSearch => {
                    self.dispatch_download_url_artstation_common(&mut metadata)
                        .await;
                }
                SubExtractorKind::ArtstationChallenge => {
                    self.dispatch_download_url_artstation_challenge(&mut metadata)
                        .await;
                }
                SubExtractorKind::ImgurImage => {
                    self.dispatch_download_url_imgur_image(&mut metadata).await;
                }
                SubExtractorKind::RedditImage => {
                    self.dispatch_download_url_reddit_image(&mut metadata).await;
                }
                SubExtractorKind::RedditHome
                | SubExtractorKind::RedditSubmission
                | SubExtractorKind::RedditUser
                | SubExtractorKind::RedditSubreddit => {
                    self.dispatch_download_url_reddit_common(&mut metadata)
                        .await;
                }
                SubExtractorKind::WebtoonsEpisode | SubExtractorKind::WebtoonsComic => {
                    self.dispatch_download_url_webtoons_common(&mut metadata)
                        .await;
                }
            },
            Message::Directory => {
                self.handle_directory(&metadata[0]);
            }
            Message::Queue => match sub_category {
                SubExtractorKind::RedditHome
                | SubExtractorKind::RedditSubmission
                | SubExtractorKind::RedditSubreddit
                | SubExtractorKind::RedditUser => {
                    self.dispatch_download_queue_reddit_common(&mut metadata);
                }
                _ => {
                    warn!("Queuing for this URL is not supported");
                }
            },
            Message::Version => {
                todo!()
            }
        }
    }

    /// Queue download URLs for reddit submissions.
    fn dispatch_download_queue_reddit_common(&mut self, metadata: &mut [Value]) {
        for submission in metadata.iter() {
            if let Some(urls) = submission.get("queuing_urls").cloned() {
                let urls: &Vec<Value> = match urls.as_array() {
                    Some(urls) => urls,
                    None => {
                        warn!("Cannot convert queuing URLs into vec");
                        continue;
                    }
                };

                for url in urls {
                    let url: &str = match url.as_str() {
                        Some(url) => url,
                        None => {
                            warn!("Cannot convert url {:?} into string", url);
                            continue;
                        }
                    };

                    info!("Attempting to queue URL: {}", url);
                    self.handle_queue(url);
                }
            }
        }
    }

    /// Download and save artstation following, artwork, album, user, likes,
    /// image, and search URLs to disk.
    async fn dispatch_download_url_artstation_common(&mut self, metadata: &mut [Value]) {
        for project in metadata.iter() {
            let assets: Vec<Value> = project["assets"]
                .as_array()
                .expect("No assets in metadata")
                .clone();

            for asset in assets.iter() {
                let asset_has_image: bool = asset["has_image"]
                    .as_bool()
                    .expect("Asset missing field: has_image");

                if !asset_has_image {
                    continue;
                }

                self.path_formatter.prepare_for_downloads(asset);

                let url: String = asset["image_url"]
                    .as_str()
                    .expect("Asset missing image url")
                    .to_owned();

                let download_status: DownloadStatus = self.handle_url(url.as_str()).await;

                // Use fallback urls if the download failed.
                if download_status == DownloadStatus::Success {
                    continue;
                } else if download_status == DownloadStatus::Forbidden {
                    let fallback_urls: Vec<ImageFallbackUrl> =
                        serde_json::from_value(asset["fallback_urls"].clone())
                            .expect("No fallback URLs found");

                    self.download_fallback_urls_artstation(fallback_urls).await;
                }
            }
        }
    }

    /// Download and save artstation challenge URLs to disk.
    async fn dispatch_download_url_artstation_challenge(&mut self, metadata: &mut [Value]) {
        for challenge in metadata.iter_mut() {
            let urls: Vec<Value> = challenge["urls"].as_array().expect("No URLs found").clone();

            for (index, url) in urls.iter().enumerate() {
                let url: String = url
                    .as_str()
                    .unwrap_or_else(|| {
                        warn!("Could not convert URL {} to string", url);
                        exit(1);
                    })
                    .to_owned();

                if !url.contains("/large/") {
                    continue;
                }

                let asset_id: i64 = match challenge["asset_ids"][index].as_i64() {
                    Some(asset_id) => asset_id,
                    None => {
                        error!(
                            "Could not convert asset id '{}' to i64",
                            challenge["asset_id"].to_string()
                        );
                        continue;
                    }
                };

                let filename: String = match challenge["filenames"][index].as_str() {
                    Some(filename) => filename.to_owned(),
                    None => {
                        error!(
                            "Could not convert filename {} to string",
                            challenge["filenames"][index].to_string()
                        );
                        continue;
                    }
                };

                let extension: String = match challenge["extensions"][index].as_str() {
                    Some(extension) => extension.to_owned(),
                    None => {
                        error!(
                            "Could not convert extension {} to string",
                            challenge["extensions"][index].to_string()
                        );
                        continue;
                    }
                };

                challenge["asset_id"] = Value::from(asset_id);
                challenge["filename"] = Value::String(filename);
                challenge["extension"] = Value::String(extension);

                self.path_formatter.prepare_for_downloads(challenge);

                self.handle_url(url.as_str()).await;
            }
        }
    }

    /// Download imgur image URLs to disk.
    async fn dispatch_download_url_imgur_image(&mut self, metadata: &mut [Value]) {
        self.path_formatter.prepare_for_downloads(&metadata[0]);

        let image_url: &str = metadata[0]["url"]
            .as_str()
            .expect("Cannot fetch image URL from metadata");

        self.handle_url(image_url).await;
    }

    /// Download reddit submission, user, and subreddit URLs to disk.
    async fn dispatch_download_url_reddit_common(&mut self, metadata: &mut [Value]) {
        for submission in metadata.iter_mut() {
            // Skip if submission has been removed from reddit.
            if !submission["removed_by_category"].is_null() {
                continue;
            }

            //Skip if submission needs to be queued.
            if !submission["queuing_urls"].is_null() {
                continue;
            }

            let mut download_single_url: bool = true;

            // If submission contains multiple URLs.
            if let Some(urls) = submission.get("urls").cloned() {
                let urls: &Vec<Value> = match urls.as_array() {
                    Some(urls) => urls,
                    None => {
                        warn!("Cannot convert URLs into vec");
                        continue;
                    }
                };

                for (index, url) in urls.iter().enumerate() {
                    let url: &str = match url.as_str() {
                        Some(url) => url,
                        None => {
                            warn!("Cannot convert URL \"{:?}\" into string", url);
                            continue;
                        }
                    };

                    let filename: Value = submission["filenames"][index].clone();
                    let extension: Value = submission["extensions"][index].clone();
                    let num: Value = submission["nums"][index].clone();

                    submission["filename"] = filename;
                    submission["extension"] = extension;
                    submission["num"] = num;

                    self.path_formatter.prepare_for_downloads(submission);
                    self.handle_url(url).await;
                }
                download_single_url = false;
            }

            if download_single_url {
                let image_url: String = submission["url"]
                    .as_str()
                    .expect("No image url in submission")
                    .to_owned();

                self.path_formatter.prepare_for_downloads(submission);
                self.handle_url(image_url.as_str()).await;
            }
        }
    }

    /// Download and save URLs to disk for reddit images.
    async fn dispatch_download_url_reddit_image(&mut self, metadata: &mut [Value]) {
        let metadata: &Value = &metadata[0];
        self.path_formatter.prepare_for_downloads(metadata);

        let image_url: String = metadata["image_url"]
            .as_str()
            .expect("No image url found")
            .to_owned();

        self.handle_url(image_url.as_str()).await;
    }

    /// Download and save webtoons episode and comic URLs to disk.
    async fn dispatch_download_url_webtoons_common(&mut self, metadata: &mut [Value]) {
        for episode in metadata.iter() {
            self.path_formatter.prepare_for_downloads(episode);

            let image_url: String = episode["image_url"]
                .as_str()
                .expect("No image url for episode")
                .to_owned();

            self.handle_url(image_url.as_str()).await;
        }
    }

    /// Prints the keywords and example values for the given URL.
    fn dispatch_keyword(sub_category: &SubExtractorKind, message: Message, metadata: &mut Value) {
        match message {
            Message::Directory => {
                Python::with_gil(|py| {
                    let metadata: PyObject = match sub_category {
                        SubExtractorKind::ArtstationFollowing
                        | SubExtractorKind::ArtstationArtwork
                        | SubExtractorKind::ArtstationAlbum
                        | SubExtractorKind::ArtstationUser
                        | SubExtractorKind::ArtstationLikes
                        | SubExtractorKind::ArtstationImage
                        | SubExtractorKind::ArtstationSearch => {
                            let asset: Value = metadata["assets"][0].clone();
                            let _ = metadata["assets"].take();
                            metadata["asset"] = asset;

                            pythonize::pythonize(py, &metadata)
                                .expect("Cannot convert metadata to PyObject")
                        }
                        SubExtractorKind::RedditImage
                        | SubExtractorKind::RedditSubmission
                        | SubExtractorKind::RedditSubreddit
                        | SubExtractorKind::RedditUser => pythonize::pythonize(py, &metadata)
                            .expect("Cannot convert metadata to PyObject"),
                        _ => {
                            error!("Directory keywords not supported for {}", sub_category);
                            return;
                        }
                    };

                    let _ = writeln!(io::stdout(), "\nKeywords for directory names:");

                    python::handle_keyword_job(py, metadata)
                        .expect("Cannot print keywords for directory");
                });
            }
            Message::Url => {
                Python::with_gil(|py| {
                    let metadata: PyObject = match sub_category {
                        SubExtractorKind::ArtstationFollowing
                        | SubExtractorKind::ArtstationArtwork
                        | SubExtractorKind::ArtstationAlbum
                        | SubExtractorKind::ArtstationUser
                        | SubExtractorKind::ArtstationLikes
                        | SubExtractorKind::ArtstationImage
                        | SubExtractorKind::ArtstationSearch => {
                            let asset: Value = metadata["assets"][0].clone();
                            let _ = metadata["assets"].take();
                            metadata["asset"] = asset;

                            pythonize::pythonize(py, &metadata)
                                .expect("Cannot convert metadata to PyObject")
                        }
                        SubExtractorKind::RedditHome
                        | SubExtractorKind::RedditImage
                        | SubExtractorKind::RedditSubmission
                        | SubExtractorKind::RedditSubreddit
                        | SubExtractorKind::RedditUser
                        | SubExtractorKind::WebtoonsEpisode
                        | SubExtractorKind::WebtoonsComic => pythonize::pythonize(py, &metadata[0])
                            .expect("Cannot convert metadat to PyObject"),
                        _ => {
                            error!("Filename keywords not supported for {}", sub_category);
                            return;
                        }
                    };

                    let _ = writeln!(io::stdout(), "\nKeywords for filenames:");

                    python::handle_keyword_job(py, metadata)
                        .expect("Cannot print keywords for URL");
                });
            }
            Message::Queue => {
                // No plans for implementing queuing for keywords.
            }
            Message::Version => {
                todo!()
            }
        }
    }

    /// Prints the urls that would be downloaded.
    fn dispatch_url(sub_category: &SubExtractorKind, message: Message, metadata: &[Value]) {
        if message == Message::Url {
            match sub_category {
                SubExtractorKind::ArtstationFollowing
                | SubExtractorKind::ArtstationArtwork
                | SubExtractorKind::ArtstationAlbum
                | SubExtractorKind::ArtstationUser
                | SubExtractorKind::ArtstationLikes
                | SubExtractorKind::ArtstationImage
                | SubExtractorKind::ArtstationSearch => {
                    Self::dispatch_url_artstation_common(metadata);
                }
                SubExtractorKind::ArtstationChallenge => {
                    Self::dispatch_url_artstation_challenge(metadata);
                }
                SubExtractorKind::ImgurImage => Self::dispatch_url_imgur_image(metadata),
                SubExtractorKind::RedditImage => {
                    Self::dispatch_url_reddit_image(metadata);
                }
                SubExtractorKind::RedditHome
                | SubExtractorKind::RedditSubmission
                | SubExtractorKind::RedditUser
                | SubExtractorKind::RedditSubreddit => {
                    Self::dispatch_url_reddit_common(metadata);
                }
                SubExtractorKind::WebtoonsEpisode | SubExtractorKind::WebtoonsComic => {
                    Self::dispatch_url_webtoons_common(metadata);
                }
            }
        }
    }

    /// Prints download URLs for webtoons episodes and comics.
    fn dispatch_url_webtoons_common(metadata: &[Value]) {
        for episode in metadata.iter() {
            let image_url: String = episode["image_url"]
                .as_str()
                .expect("No image url for episode")
                .to_owned();

            let _ = writeln!(io::stdout(), "{}", image_url);
        }
    }

    /// Prints the download URLs for artstation challenges.
    fn dispatch_url_artstation_challenge(metadata: &[Value]) {
        for challenge in metadata.iter() {
            let urls: Vec<Value> = challenge["urls"].as_array().expect("No URLs found").clone();

            for url in urls.into_iter() {
                let url: String = url
                    .as_str()
                    .unwrap_or_else(|| {
                        warn!("Could not convert URL {} to string", url);
                        exit(1);
                    })
                    .to_owned();

                let _ = writeln!(io::stdout(), "{}", url);
            }
        }
    }

    /// Prints the URLs that would be downloaded for artstation following,
    /// artwork, album, user, likes, image, and search URLs.
    fn dispatch_url_artstation_common(metadata: &[Value]) {
        for project in metadata.iter() {
            let assets: Vec<Value> = project["assets"]
                .as_array()
                .expect("No assets in metadata")
                .clone();

            for asset in assets.iter() {
                let asset_has_image: bool = asset["has_image"]
                    .as_bool()
                    .expect("Asset missing field: has_image");

                if !asset_has_image {
                    continue;
                }

                let url: String = asset["image_url"]
                    .as_str()
                    .expect("Asset missing image url")
                    .to_owned();

                let _ = writeln!(io::stdout(), "{}", url);
            }
        }
    }

    /// Prints the download URLs for imgur images.
    fn dispatch_url_imgur_image(metadata: &[Value]) {
        let image_url: &str = metadata[0]["image_url"]
            .as_str()
            .expect("No download URls found");

        let _ = writeln!(io::stdout(), "{}", image_url);
    }

    /// Prints the download URLs for reddit images.
    fn dispatch_url_reddit_image(metadata: &[Value]) {
        let metadata: &Value = &metadata[0];
        let image_url: String = metadata["image_url"]
            .as_str()
            .expect("No image url found")
            .to_owned();

        let _ = writeln!(io::stdout(), "{}", image_url);
    }

    /// Prints the download URLs for reddit submissions, users, and subreddits.
    fn dispatch_url_reddit_common(metadata: &[Value]) {
        for submission in metadata.iter() {
            let image_url: String = submission["url"]
                .as_str()
                .expect("No image url in submission")
                .to_owned();

            let _ = writeln!(io::stdout(), "{}", image_url);
        }
    }

    /// Download fallback URLs for artstation extractors.
    async fn download_fallback_urls_artstation(&mut self, fallback_urls: Vec<ImageFallbackUrl>) {
        for url in fallback_urls.into_iter() {
            match url {
                ImageFallbackUrl::Large(url)
                | ImageFallbackUrl::Medium(url)
                | ImageFallbackUrl::Small(url) => {
                    trace!("Using fallback URL \"{}\"", url.as_str());

                    let download_status: DownloadStatus = self.handle_url(url.as_str()).await;

                    if download_status == DownloadStatus::Success {
                        break;
                    }
                }
            }
        }
    }

    /// Builds and creates the directory if necessary.
    fn handle_directory(&mut self, metadata: &Value) {
        #[allow(clippy::unwrap_used)]
        let base_directory: String = self.config.extractor.base_directory.to_string().unwrap();
        let base_directory: String = shellexpand::full(base_directory.as_str())
            .expect("Could not shell expand base directory")
            .to_string();

        self.path_formatter
            .build_directory(&metadata, base_directory);

        if self.path_formatter.create_directory {
            self.path_formatter.create_directory();
        }
    }

    /// Handles the queuing of extractors.
    fn handle_queue(&mut self, url: &str) {
        if self.urls_visited.contains(&url.to_string()) {
            return;
        }

        self.urls_visited.push(url.to_owned());

        match App::new(self.config.clone(), JobKind::Download, url) {
            Ok((mut app, runtime)) => {
                thread::spawn(move || {
                    app.run(&runtime);
                })
                .join()
                .expect("Thread panicked");
            }
            Err(error) => {
                warn!("Cannot queue URL {}: {}", url, error);
            }
        }
    }

    /// Downloads and saves the url to disk.
    async fn handle_url(&mut self, mut url: &str) -> DownloadStatus {
        // Create a new downloader.
        let downloader: Box<dyn Downloader> = if url.starts_with("ytdl:") {
            url = &url[5..];

            info!("Using Youtube downloader");

            YoutubeDL::new()
        } else {
            info!("Using HTTP downloader");

            HttpDownloader::new(&self.config, self.downloader_client.clone())
        };

        let file_path: String = self.path_formatter.file_path.clone();

        if downloader.kind() == DownloaderKind::Ytdp {
            // Download URLs with the Youtube downloader.
            downloader
                .download(url, file_path, &mut self.path_formatter)
                .await
        } else {
            // Download URLs with the HTTP downloader.
            let is_part_file: bool = self.path_formatter.is_part_file;
            let part_file_path: String = self.path_formatter.part_file_path.clone();
            let file_exists: bool = Path::exists(Path::new(&file_path));
            let part_file_exists: bool = Path::exists(Path::new(&part_file_path));
            let is_skip: bool = self.config.extractor.skip;

            if file_exists && part_file_exists {
                debug!("Removing {}", &self.path_formatter.part_file_path);
                fs::remove_file(file_path.clone()).unwrap_or_else(|_| {
                    panic!("Could not remove {}", &self.path_formatter.part_file_path)
                });
            }

            return if (file_exists || !is_part_file) && is_skip {
                info!("{} exists, skipping...", file_path);
                DownloadStatus::FileAlreadyExists
            } else {
                let download_status: DownloadStatus = if is_part_file {
                    downloader
                        .download(url, part_file_path.clone(), &mut self.path_formatter)
                        .await
                } else {
                    downloader
                        .download(url, file_path, &mut self.path_formatter)
                        .await
                };

                if download_status != DownloadStatus::Success {
                    return download_status;
                }

                if is_part_file {
                    self.path_formatter
                        .convert_part_file(part_file_path.as_str())
                        .expect("Could not remove path file extension");

                    info!("Successfully converted part file");
                }
                download_status
            };
        }
    }
}

impl Default for JobKind {
    fn default() -> Self {
        JobKind::Download
    }
}

impl Display for JobKind {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            JobKind::Download => write!(f, "DownloadJob"),
            JobKind::Keyword => write!(f, "KeywordJob"),
            JobKind::Url => write!(f, "UrlJob"),
        }
    }
}

/// Initialize a [`tokio::runtime::Runtime`].
fn initialize_tokio_runtime(number_of_threads: usize, thread_name: String) -> Runtime {
    #[allow(clippy::unwrap_used)]
    Builder::new_multi_thread()
        .worker_threads(number_of_threads)
        .thread_name(thread_name)
        .enable_all()
        .build()
        .unwrap()
}
