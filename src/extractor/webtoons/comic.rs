/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Responsible for extracting a comic on webtoons.com.

use std::sync::Arc;

use async_trait::async_trait;
use fancy_regex::{CaptureMatches, Captures, Regex};
use futures::future;
use reqwest::{Client, Response};
use serde_json::Value;
use tokio::sync::Mutex;

use crate::extractor::message::Message;
use crate::extractor::webtoons::common::Common;
use crate::extractor::{Error, Extractor, ExtractorKind, SubExtractorKind};
use crate::text;

/// Extractor for comics on webtoons.com
pub struct WebtoonsComicExtractor {
    /// Shared Webtoons extractor types and methods.
    common: Common,
    /// Episode URL.
    url: String,
    /// Episode language.
    lang: String,
    /// Episode Genre.
    genre: String,
    /// Comic URL.
    comic: String,
    /// Number of pages to extract from a comic.
    depth: i64,
    /// URL has the query `page`.
    has_page_number: bool,
    /// Comic page number,
    page_number: Option<i64>,
}

impl WebtoonsComicExtractor {
    /// Construct a new [`WebtoonsComicExtractor`].
    pub fn new(
        url: &str,
        mut depth: i64,
        filename_format: &str,
    ) -> Result<WebtoonsComicExtractor, Error> {
        let url_regex_pattern: Regex = Regex::new(
            r#"(?:https?://)?(?:www\.)?webtoons\.com/(([^/?#]+)/([^/?#]+)/([^/?#]+))/list(?:\?([^#]+))"#
        ).expect("Could not compile regex");

        let captures: Captures = url_regex_pattern
            .captures(url)?
            .ok_or(Error::SubExtractorNotFound)?;

        let mut lang: String = "".to_owned();
        let mut genre: String = "".to_owned();
        let mut comic: String = "".to_owned();
        let mut url_parts: Vec<&mut String> = vec![&mut lang, &mut genre, &mut comic];

        for (url_part, match_index) in (url_parts.iter_mut()).zip(2..5) {
            let match_string: &str = match captures.get(match_index) {
                Some(capture) => capture.as_str(),
                None => continue,
            };

            url_part.push_str(match_string);
        }

        let queries: Vec<String> = text::parse_url_query_to_vec(url);

        let mut page_number: Option<i64> = None;
        let mut has_page_number: bool = false;

        if let Some(index) = queries.iter().position(|query| query.contains("page")) {
            let page_number_temp: i64 = queries[index + 1]
                .clone()
                .parse()
                .expect("Could not convert page number to i64");
            has_page_number = true;
            depth += page_number_temp;
            page_number = Some(page_number_temp);
        };

        Ok(Self {
            common: Common::new(filename_format.to_owned()),
            url: url.to_owned(),
            lang,
            genre,
            comic,
            depth,
            page_number,
            has_page_number,
        })
    }
}

#[async_trait]
impl Extractor for WebtoonsComicExtractor {
    fn items(&self, metadata: Vec<Value>) -> Vec<(Message, Vec<Value>)> {
        self.common.items(metadata)
    }

    fn get_file_format(&self) -> &String {
        &self.common.filename_format
    }

    fn get_category(&self) -> ExtractorKind {
        ExtractorKind::Webtoons
    }

    fn get_subextractor(&self) -> SubExtractorKind {
        SubExtractorKind::WebtoonsComic
    }

    async fn get_metadata(&self, client: Client) -> Vec<Value> {
        let metadata: Arc<Mutex<Vec<Value>>> = Arc::new(Mutex::new(Vec::new()));
        let page_begin: i64 = if let Some(page_number) = self.page_number {
            page_number
        } else {
            1
        };

        for page_number in page_begin..self.depth {
            let url: String = if self.has_page_number {
                let mut url: String = self.url.clone();
                let original_page_query: String = format!("page={}", page_begin);
                let new_page_query: String = format!("page={}", page_number);
                url = url.replace(original_page_query.as_str(), new_page_query.as_str());
                url
            } else {
                format!("{}&page={}", self.url, page_number)
            };

            let response: Response = Common::get_request(&client, url.as_str()).await;
            let body: String = response.text().await.expect("No text in response");

            if !body.contains(url.as_str()) {
                break;
            }

            let page: String = text::extract(body.as_str(), r#"id="_listUl""#, r#"</ul>"#)
                .expect("Could not extract image urls from response body");

            // `page` does not live long enough in [`CaptureMatches`] so this wizardry below
            // solves that.
            let page_cloned: String = page.clone();
            let page_static: &'static str = Box::leak(page_cloned.into_boxed_str());
            let matches: CaptureMatches = self
                .common
                .episode_url_regex_pattern
                .captures_iter(page_static);
            let mut futures: Vec<_> = Vec::new();

            for capture in matches {
                let metadata_cloned: Arc<Mutex<Vec<Value>>> = metadata.clone();
                let lang: String = self.lang.clone();
                let genre: String = self.genre.clone();
                let comic: String = self.comic.clone();
                let client_cloned: Client = client.clone();

                futures.push(tokio::spawn(async move {
                    // Using a .unwrap() *should* be safe in this for loop.
                    #[allow(clippy::unwrap_used)]
                    let episode_url: &str = capture.unwrap().get(0).unwrap().as_str();

                    let queries: Vec<String> = text::parse_url_query_to_vec(episode_url);

                    #[allow(clippy::unwrap_used)]
                    let episode_number_index: usize = queries
                        .iter()
                        .position(|query| query == "episode_no")
                        .unwrap()
                        + 1;

                    #[allow(clippy::unwrap_used)]
                    let episode_number: i64 = queries[episode_number_index].parse().unwrap();

                    let episode_response: Response =
                        Common::get_request(&client_cloned, episode_url).await;

                    let episode_page: String = episode_response
                        .text()
                        .await
                        .expect("No text in episode response");

                    let episode_title: String = Common::get_episode_title(episode_page.as_str());
                    let episode_description: String =
                        Common::get_episode_description(episode_page.as_str());

                    let image_urls: Vec<String> = Common::get_image_urls(episode_page.as_str());
                    let image_numbers = 0..image_urls.len();

                    for (image_url, image_number) in
                        (image_urls.into_iter()).zip(image_numbers.into_iter())
                    {
                        let mut episode_metadata: Value = Value::default();

                        episode_metadata["url"] = Value::String(episode_url.to_owned());
                        episode_metadata["lang"] = Value::String(lang.clone());
                        episode_metadata["genre"] = Value::String(genre.clone());
                        episode_metadata["comic"] = Value::String(comic.clone());
                        episode_metadata["episode_no"] = Value::from(episode_number);
                        episode_metadata["title"] = Value::String(episode_title.clone());
                        episode_metadata["description"] =
                            Value::String(episode_description.clone());
                        episode_metadata["image_url"] = Value::String(image_url.clone());
                        episode_metadata["image_no"] = Value::from(image_number);

                        metadata_cloned.lock().await.push(episode_metadata);
                    }
                }));
            }
            future::join_all(futures).await;
        }

        #[allow(clippy::unwrap_used)]
        let metadata: Vec<Value> = Arc::try_unwrap(metadata).unwrap().into_inner();
        metadata
    }
}
