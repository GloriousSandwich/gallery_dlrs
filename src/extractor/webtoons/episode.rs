/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Responsible for extracting an episode on webtoons.com.

use async_trait::async_trait;
use fancy_regex::{Captures, Regex};
use log::info;
use reqwest::{Client, Response};
use serde_json::Value;

use crate::extractor::message::Message;
use crate::extractor::webtoons::common::Common;
use crate::extractor::{Error, Extractor, ExtractorKind, SubExtractorKind};
use crate::text;

/// Extractor for episodes on webtoons.com.
pub struct WebtoonsEpisodeExtractor {
    /// Shared Webtoons extractor types and methods.
    common: Common,
    /// Episode URL.
    url: String,
    /// Episode language.
    lang: String,
    /// Episode Genre.
    genre: String,
    /// Comic URL.
    comic: String,
    /// Episode number.
    episode_number: i64,
}

impl WebtoonsEpisodeExtractor {
    /// Construct a new [`WebtoonsEpisodeExtractor`].
    pub fn new(url: &str, filename_format: &str) -> Result<WebtoonsEpisodeExtractor, Error> {
        let common: Common = Common::new(filename_format.to_owned());

        let captures: Captures = common
            .episode_url_regex_pattern
            .captures(url)?
            .ok_or(Error::SubExtractorNotFound)?;

        let mut lang: String = "".to_owned();
        let mut genre: String = "".to_owned();
        let mut comic: String = "".to_owned();
        let mut url_parts: Vec<&mut String> = vec![&mut lang, &mut genre, &mut comic];

        for (url_part, match_index) in (url_parts.iter_mut()).zip(2..5) {
            let match_string: &str = match captures.get(match_index) {
                Some(capture) => capture.as_str(),
                None => continue,
            };

            url_part.push_str(match_string);
        }

        let queries: Vec<String> = text::parse_url_query_to_vec(url);
        let episode_number: i64 = match queries.iter().position(|query| query == "episode_no") {
            Some(query) => queries[query + 1]
                .parse()
                .expect("Could not convert page number into integer"),
            None => return Err(Error::SubExtractorNotFound),
        };

        Ok(Self {
            common,
            url: url.to_owned(),
            lang,
            genre,
            comic,
            episode_number,
        })
    }
}

#[async_trait]
impl Extractor for WebtoonsEpisodeExtractor {
    fn items(&self, metadata: Vec<Value>) -> Vec<(Message, Vec<Value>)> {
        self.common.items(metadata)
    }

    fn get_file_format(&self) -> &String {
        &self.common.filename_format
    }

    fn get_category(&self) -> ExtractorKind {
        ExtractorKind::Webtoons
    }

    fn get_subextractor(&self) -> SubExtractorKind {
        SubExtractorKind::WebtoonsEpisode
    }

    async fn get_metadata(&self, client: Client) -> Vec<Value> {
        let response: Response = Common::get_request(&client, self.url.as_str()).await;
        let page: String = response.text().await.expect("No text in response");
        let title: String = text::extract(
            page.as_str(),
            r#"<meta property="og:title" content=""#,
            r#"""#,
        )
        .expect("");

        let description: String = text::extract(
            page.as_str(),
            r#"<meta property="og:description" content=""#,
            r#"""#,
        )
        .expect("");

        let image_urls: Vec<String> = Common::get_image_urls(page.as_str());
        let mut metadata: Vec<Value> = Vec::with_capacity(image_urls.len());

        for (image_no, image_url) in image_urls.iter().enumerate() {
            let mut episode_metadata: Value = Value::default();

            episode_metadata["url"] = Value::String(self.url.clone());
            episode_metadata["lang"] = Value::String(self.lang.clone());
            episode_metadata["genre"] = Value::String(self.genre.clone());
            episode_metadata["comic"] = Value::String(self.comic.clone());
            episode_metadata["episode_no"] = Value::from(self.episode_number);
            episode_metadata["title"] = Value::String(title.clone());
            episode_metadata["description"] = Value::String(description.clone());
            episode_metadata["image_url"] = Value::String(image_url.clone());
            episode_metadata["image_no"] = Value::from(image_no);

            metadata.push(episode_metadata);
        }

        info!("Deserialized Episode {}", self.episode_number);
        metadata
    }
}
