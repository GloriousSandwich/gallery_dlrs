/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Common types and functions shared between Webtoons extractors.
use std::process::exit;

use fancy_regex::Regex;
use log::error;
use reqwest::{Client, Response};
use serde_json::Value;

use crate::extractor::message::Message;
use crate::text;

/// Common methods shared between Webtoons extractors.
pub struct Common {
    /// Format for filename.
    pub filename_format: String,
    /// Regex pattern for a Webtoons episode URL.
    pub episode_url_regex_pattern: Regex,
}

impl Common {
    /// Creates a new [`Common`].
    pub fn new(filename_format: String) -> Self {
        let episode_url_regex_pattern: Regex = Regex::new(
            r#"(?:https?://)?(?:www\.)?webtoons\.com/(([^/?#]+)/([^/?#]+)/([^/?#]+)/(?:[^/?#]+))/viewer(?:\?([^#'\"]+))"#
        )
            .expect("Could not compile regex");

        Self {
            filename_format,
            episode_url_regex_pattern,
        }
    }
    /// Extract the image URL and the metadata associated with it.
    pub fn items(&self, mut metadata: Vec<Value>) -> Vec<(Message, Vec<Value>)> {
        let mut items: Vec<(Message, Vec<Value>)> = Vec::new();
        items.push((Message::Directory, metadata.clone()));

        for episode in metadata.iter_mut() {
            let image_url: String = episode["image_url"]
                .as_str()
                .expect("Episode has no image URL")
                .to_owned();

            let (_filename, extension) = text::extract_name_from_url(image_url.as_str());
            episode["extension"] = Value::String(extension);
        }
        items.push((Message::Url, metadata));
        items
    }

    /// Make a `GET` request to a URL.
    pub async fn get_request(client: &Client, url: &str) -> Response {
        // TODO: Add proper error handling for status code 404.
        let response: Response = client.get(url).send().await.unwrap_or_else(|error| {
            error!("{error}");
            exit(1);
        });

        if response.url().as_str().contains("/ageGate") {
            error!(
                "HTTP redirect to age gate check: {}",
                response.url().as_str()
            );
            exit(1);
        }
        response
    }

    /// Extract images from a response body.
    pub fn get_image_urls(body: &str) -> Vec<String> {
        let image_urls: Vec<String> =
            text::extract_all(body, r#"class="_images" data-url=""#, r#"""#)
                .expect("No images found");

        // Bypass HTTP status code 403.
        let image_urls: Vec<String> = image_urls
            .into_iter()
            .map(|url| url.replace("://webtoon-phinf.", "://swebtoon-phinf."))
            .collect();

        image_urls
    }

    /// Returns the episode title from `page`.
    pub fn get_episode_title(page: &str) -> String {
        text::extract(page, r#"<meta property="og:title" content=""#, r#"""#).expect("")
    }

    /// Returns the episode description from `page`.
    pub fn get_episode_description(page: &str) -> String {
        text::extract(page, r#"<meta property="og:description" content=""#, r#"""#).expect("")
    }
}
