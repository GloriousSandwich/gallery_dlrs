/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Module responsible for extracting and deserializing Webtoons metadata.

pub mod comic;
mod common;
pub mod episode;
