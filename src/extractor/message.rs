/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Enum for message identifiers

/// Extractors yield their results as message-tuples, where the first element
/// is one of the following identifiers. This message-identifier determines
/// the type and meaning of the other elements in such a tuple.
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
#[non_exhaustive]
pub enum Message {
    /// - Message.Version:
    ///     - Message protocol version (currently always '1')
    #[allow(dead_code)]
    Version = 1,
    /// - Message.Directory:
    ///     - Sets the target directory for all following images
    Directory = 2,
    /// - Message.Url:
    ///     - Image URL and its metadata
    Url = 3,
    /// - Message.Queue:
    /// - (External) URL that should be handled by another extractor
    Queue = 6,
}
