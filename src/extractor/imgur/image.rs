//! Responsible for extracting individual images on imgur.

use async_trait::async_trait;
use fancy_regex::{Captures, Regex};
use reqwest::Client;
use serde_json::Value;

use crate::config::extractor::Imgur;
use crate::extractor::imgur::api::ImgurAPI;
use crate::extractor::imgur::common::Common;
use crate::extractor::message::Message;
use crate::extractor::Error::SubExtractorNotFound;
use crate::extractor::{Error, Extractor, ExtractorKind, SubExtractorKind};
use crate::text;

/// Extractor for extracting individual images on imgur.
pub struct ImgurImageExtractor {
    /// The Imgur API interface.
    api: ImgurAPI,

    /// Shared types and methods for Imgur extractors.
    common: Common,

    /// Image hash.
    key: String,
}

impl ImgurImageExtractor {
    /// Creates a new [`ImgurImageExtractor`].
    pub fn new(url: &str, config: &Imgur, filename_format: &str) -> Result<Self, Error> {
        let url_regex: Regex = Regex::new(r#"(?:https?://)?(?:www\.|[im]\.)?imgur\.com/(?!gallery|search)(?:r/\w+/)?(\w{7}|\w{5})[sbtmlh]?"#).expect("Cannot compile regex");

        let captures: Captures = url_regex.captures(url)?.ok_or(SubExtractorNotFound)?;

        let key: String = captures
            .get(1)
            .expect("Cannot find `key` in URL")
            .as_str()
            .to_owned();

        Ok(Self {
            api: ImgurAPI::new(config.clone()),
            common: Common::new(filename_format.to_owned()),
            key,
        })
    }
}

#[async_trait]
impl Extractor for ImgurImageExtractor {
    fn items(&self, mut metadata: Vec<Value>) -> Vec<(Message, Vec<Value>)> {
        let image: &mut Value = &mut metadata[0];

        // Remove necessary keys.
        let _ = image["ad_url"].take();
        let _ = image["ad_type"].take();

        image["media"] = image["media"][0].take();

        let image_extension: String = image["media"]["ext"]
            .as_str()
            .expect("Cannot get image extension from metadata")
            .to_string();

        if image_extension.as_str() == "jpeg" {
            image["ext"] = Value::String("jpg".to_owned());
        }

        let image_id: &str = image["id"]
            .as_str()
            .expect("Cannot get image extension from metadata");

        let url: String = format!("https://i.imgur.com/{}.{}", image_id, image_extension);
        image["url"] = Value::String(url.clone());

        let (filename, extension) = text::extract_name_from_url(url.as_str());
        image["filename"] = Value::String(filename);
        image["extension"] = Value::String(extension);

        vec![
            (Message::Directory, metadata.clone()),
            (Message::Url, metadata),
        ]
    }

    fn get_file_format(&self) -> &String {
        &self.common.filename_format
    }

    fn get_category(&self) -> ExtractorKind {
        ExtractorKind::Imgur
    }

    fn get_subextractor(&self) -> SubExtractorKind {
        SubExtractorKind::ImgurImage
    }

    async fn get_metadata(&self, client: Client) -> Vec<Value> {
        let image: Value = self.api.image(&client, &self.key).await;

        vec![image]
    }
}
