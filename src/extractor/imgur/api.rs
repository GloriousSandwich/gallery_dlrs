//! Interface for the Imgur API.

use std::collections::HashMap;
use std::fmt::Display;

use oauth2::ClientId;
use reqwest::header::{HeaderMap, AUTHORIZATION};
use reqwest::{Client, Response};
use serde_json::Value;

use crate::config::extractor::Imgur;

/// Interface for the Imgur API.
///
/// Ref: https://apidocs.imgur.com/
pub(crate) struct ImgurAPI {
    /// Configurations for Imgur extractors.
    config: Imgur,

    /// Headers for authorization.
    headers: HeaderMap,
}

impl ImgurAPI {
    /// Returns a new [`ImgurAPI`].
    pub(crate) fn new(config: Imgur) -> Self {
        let client_id: ClientId = config.oauth2.client_id.client_id();
        let mut headers: HeaderMap = HeaderMap::new();

        #[allow(clippy::unwrap_used)]
        headers.insert(
            AUTHORIZATION,
            format!("Client-ID {}", client_id.as_str()).parse().unwrap(),
        );

        Self { config, headers }
    }

    /// Fetches the metadata for an image.
    pub(crate) async fn image<I: Display>(&self, client: &Client, image_hash: I) -> Value {
        let endpoint: String = format!("/post/v1/media/{}", image_hash);
        let query: HashMap<String, String> =
            HashMap::from([("include".to_owned(), "media,tags,account".to_owned())]);

        let json: Value = self.call(client, endpoint, &query).await;

        json
    }

    /// Call the Imgur API.
    async fn call<E: Display + Sized>(
        &self,
        client: &Client,
        endpoint: E,
        query: &HashMap<String, String>,
    ) -> Value {
        let url: String = format!("https://api.imgur.com{}", endpoint);

        let response: Response = client
            .get(url.as_str())
            .headers(self.headers.clone())
            .query(query)
            .send()
            .await
            .unwrap_or_else(|error| panic!("Error calling the imgur API: {}", error));

        let json: Value = response
            .json()
            .await
            .unwrap_or_else(|error| panic!("Cannot convert response into JSON: {}", error));

        json
    }
}
