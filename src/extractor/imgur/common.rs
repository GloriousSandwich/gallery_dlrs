//! Common types and methods for Imgur extractors.

/// Common types and methods for Imgur extractors.
pub(crate) struct Common {
    /// Format for the filename.
    pub(crate) filename_format: String,
}

impl Common {
    /// Creates a new [`Common`].
    pub(crate) fn new(filename_format: String) -> Self {
        Self { filename_format }
    }
}
