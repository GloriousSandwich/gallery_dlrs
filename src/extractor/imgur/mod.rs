//! Module responsible for extracting Imgur metadata.

pub mod api;
pub mod common;
pub mod image;
