//! Responsible for extracting URLs from a submission on reddit.com.

use async_trait::async_trait;
use fancy_regex::{Captures, Regex};
use reqwest::Client;
use serde_json::Value;
use tokio::sync::Mutex;

use crate::cache::Cache;
use crate::config::extractor::Reddit;
use crate::extractor::message::Message;
use crate::extractor::reddit::api::RedditAPI;
use crate::extractor::reddit::common::Common;
use crate::extractor::{Error, Extractor, ExtractorKind, SubExtractorKind};

/// Extractor for URLs from a submission on reddit.com.
pub struct RedditSubmissionExtractor {
    /// The reddit API interface.
    api: Mutex<RedditAPI>,

    /// Shared types and methods for reddit extractors.
    common: Common,

    /// The ID belonging to the reddit submission.
    submission_id: String,
}

impl RedditSubmissionExtractor {
    /// Creates a new [`RedditSubmissionExtractor`].
    pub fn new(
        url: &str,
        config: &Reddit,
        cache: &Cache,
        filename_format: &str,
    ) -> Result<Self, Error> {
        let url_regex: Regex = Regex::new(r#"(?:https?://)?(?:(?:\w+\.)?reddit\.com/(?:(?:r|u|user)/[^/?#]+/comments|gallery)|redd\.it)/([a-z0-9]+)"#).expect("Could not compile regex");

        let captures: Captures = url_regex
            .captures(url)?
            .ok_or(Error::SubExtractorNotFound)?;

        let submission_id: String = captures
            .get(1)
            .expect("Cannot find submission ID from URL")
            .as_str()
            .to_owned();

        Ok(Self {
            api: Mutex::new(RedditAPI::new(config.clone(), cache.clone())),
            submission_id,
            common: Common::new(filename_format.to_string()),
        })
    }
}

#[async_trait]
impl Extractor for RedditSubmissionExtractor {
    fn items(&self, metadata: Vec<Value>) -> Vec<(Message, Vec<Value>)> {
        self.common.items(metadata)
    }

    fn get_file_format(&self) -> &String {
        &self.common.filename_format
    }

    fn get_category(&self) -> ExtractorKind {
        ExtractorKind::Reddit
    }

    fn get_subextractor(&self) -> SubExtractorKind {
        SubExtractorKind::RedditSubmission
    }

    async fn get_metadata(&self, client: Client) -> Vec<Value> {
        let submission: Value = self
            .api
            .lock()
            .await
            .submission(client, self.submission_id.as_str())
            .await;

        vec![submission]
    }
}
