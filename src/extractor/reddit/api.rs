//! Interface for the Reddit API.

use std::collections::HashMap;
use std::fmt;

use chrono::{TimeZone, Utc};
use log::warn;
use oauth2::{AccessToken, ClientId, ClientSecret, RefreshToken};
use oauth2::http::HeaderMap;
use reqwest::{Client, Response, StatusCode};
use reqwest::header::AUTHORIZATION;
use serde_json::Value;

use crate::cache::Cache;
use crate::config::de_wrappers::DateTimeWrapper;
use crate::config::extractor::Reddit;
use crate::oauth;

/// Interface for the Reddit API.
///
/// Ref: https://www.reddit.com/dev/api/
pub struct RedditAPI {
    /// Configurations for Reddit extractors.
    config: Reddit,

    /// Handles creating and reading cache files.
    cache: Cache,

    /// Tells reddit which app is making the request.
    client_id: ClientId,

    /// A secret only known to the client and authorization server.
    client_secret: ClientSecret,

    /// Access token for making http requests.
    access_token: AccessToken,

    /// Refresh token used to gain a new access token.
    refresh_token: RefreshToken,
}

impl RedditAPI {
    /// Returns a new [`RedditAPI`].
    pub fn new(config: Reddit, cache: Cache) -> Self {
        // Write access_token to cache if it doesn't already exist.
        let mut access_token_exists_in_cache: bool = false;

        // Fetch the access_token from cache if it exists.
        let access_token: AccessToken = match cache.read("access_token") {
            Ok(access_token) => match String::from_utf8(access_token) {
                Ok(access_token) => {
                    access_token_exists_in_cache = true;
                    AccessToken::new(access_token)
                }
                Err(error) => {
                    warn!("cache: Cannot convert access_token to string: {}", error);
                    config.oauth2.access_token()
                }
            },
            Err(error) => {
                warn!("cache: Cannot access access_token: {:?}", error);
                config.oauth2.access_token()
            }
        };

        if !access_token_exists_in_cache {
            if let Err(error) = cache.write("access_token", access_token.secret()) {
                warn!("cache: Cannot write access token to cache: {:?}", error);
            }
        }

        // Write refresh_token to cache if it doesn't already exist.
        let mut refresh_token_exists_in_cache: bool = false;

        // Fetch the refresh_token from cache if it exists.
        let refresh_token: RefreshToken = match cache.read("refresh_token") {
            Ok(refresh_token) => match String::from_utf8(refresh_token) {
                Ok(refresh_token) => {
                    refresh_token_exists_in_cache = true;
                    RefreshToken::new(refresh_token)
                }
                Err(error) => {
                    warn!("cache: Cannot convert refresh_token to string: {}", error);
                    config.oauth2.refresh_token()
                }
            },
            Err(error) => {
                warn!("cache: Cannot access refresh_token: {:?}", error);
                config.oauth2.refresh_token()
            }
        };

        if !refresh_token_exists_in_cache {
            if let Err(error) = cache.write("refresh_token", refresh_token.secret()) {
                warn!("cache: Cannot write access token to cache: {:?}", error);
            }
        }

        Self {
            access_token,
            refresh_token,
            cache,
            client_id: config.oauth2.client_id.clone().client_id(),
            client_secret: config
                .oauth2
                .client_secret
                .clone()
                .expect("No client secret found")
                .client_secret(),
            config,
        }
    }

    /// Fetches the submissions for a submission id.
    pub async fn submission(&mut self, client: Client, submission_id: &str) -> Value {
        let endpoint: String = "/comments/".to_owned() + submission_id + "/.json";

        let mut submission: Value = self
            .call(
                &client,
                endpoint,
                &HashMap::from([("raw_json".to_owned(), "1".to_owned())]),
            )
            .await;

        submission[0]["data"]["children"][0]["data"].take()
    }

    /// Fetches the submissions for a subreddit.
    pub async fn submission_subreddit(
        &mut self,
        client: Client,
        subreddit: &str,
        query: HashMap<String, String>,
    ) -> Vec<Value> {
        let endpoint: String = subreddit.to_owned() + "/.json";

        let submission: Vec<Value> = self.pagination(&client, query, &endpoint).await;

        submission
    }

    /// Fetches the submissions for a reddit user.
    pub async fn submission_user(
        &mut self,
        client: Client,
        user: &str,
        query: HashMap<String, String>,
    ) -> Vec<Value> {
        let endpoint: String = format!("/user/{}/.json", user);

        let submission: Vec<Value> = self.pagination(&client, query, &endpoint).await;

        submission
    }

    /// Setup types before calling the Reddit API.
    async fn pagination(
        &mut self,
        client: &Client,
        mut query: HashMap<String, String>,
        endpoint: &String,
    ) -> Vec<Value> {
        // Setup types.
        let id_min: i64 = self.config.id_min;
        let id_max: i64 = self.config.id_max.unwrap_or(2147483647);

        let date_min: i64 = if let Some(date_min) = self.config.date_min.to_naive() {
            date_min.timestamp()
        } else if let Some(date_min) = self.config.date_min.to_utc() {
            date_min.timestamp()
        } else {
            DateTimeWrapper::default().timestamp()
        };

        let date_max: i64 = if let Some(date_max) = self.config.date_max.to_naive() {
            date_max.timestamp()
        } else if let Some(date_max) = self.config.date_max.to_utc() {
            date_max.timestamp()
        } else {
            Utc.timestamp(253402210800, 0).timestamp()
        };

        let mut submissions: Vec<Value> = Vec::new();

        // Call the Reddit API.
        loop {
            let submission: Value = self.call(client, &endpoint, &query).await["data"].take();

            #[allow(clippy::unwrap_used)]
            let children: Vec<Value> = submission["children"].as_array().cloned().unwrap();

            for child in children.iter() {
                let post: &Value = &child["data"];

                #[allow(clippy::unwrap_used)]
                let post_id: &str = post["id"].as_str().unwrap();

                #[allow(clippy::unwrap_used)]
                let post_id_decoded: i64 = self.decode_base36(post_id) as i64;

                // Using `as_i64()` returns `None` and I don't know why.
                #[allow(clippy::unwrap_used)]
                let post_created_utc: i64 = post["created_utc"].as_f64().unwrap() as i64;

                if (date_min <= post_created_utc)
                    && (post_created_utc <= date_max)
                    && (id_min <= post_id_decoded)
                    && (post_id_decoded <= id_max)
                {
                    #[allow(clippy::unwrap_used)]
                    let kind: &str = child["kind"].as_str().unwrap();

                    if kind == "t3" {
                        submissions.push(post.clone());
                    }
                }
            }

            if let Some(after) = submission["after"].as_str() {
                query.insert("after".to_owned(), after.to_owned());
            } else {
                return submissions;
            }
        }
    }

    /// Authenticate the application by requesting an access token.
    fn authenticate_headers(&self) -> HeaderMap {
        let mut headers: HeaderMap = HeaderMap::new();
        #[allow(clippy::unwrap_used)]
        headers.append(
            AUTHORIZATION,
            format!("bearer {}", self.access_token.secret())
                .parse()
                .unwrap(),
        );

        headers
    }

    /// Call the Reddit API.
    async fn call<E>(
        &mut self,
        client: &Client,
        endpoint: E,
        query: &HashMap<String, String>,
    ) -> Value
    where
        E: fmt::Display + Sized,
    {
        let url: String = format!("https://oauth.reddit.com{}", endpoint);

        // Refresh access token.
        self.refresh_token(&client).await;

        let response: Response = client
            .get(url.as_str())
            .headers(self.authenticate_headers())
            .query(query)
            .send()
            .await
            .unwrap_or_else(|error| panic!("Error calling the reddit API: {}", error));
        let json: Value = response
            .json::<Value>()
            .await
            .unwrap_or_else(|error| panic!("Could not convert response into json: {}", error));

        if let Some(error) = json.get("error") {
            #[allow(clippy::unwrap_used)]
            match error.as_i64().unwrap() {
                403 => {
                    panic!("Insufficient privileges to access the specified resource");
                }
                404 => {
                    panic!("Request to {} could not be found", url);
                }
                _ => {
                    panic!("{}", json.get("message").unwrap_or(error));
                }
            }
        }

        json
    }

    /// Decodes `s` via base36.
    fn decode_base36(&self, s: &str) -> usize {
        let alphabet: &str = "0123456789abcdefghijklmnopqrstuvwxyz";
        let mut number: usize = 0;
        let base: usize = alphabet.len();

        for c in s.chars() {
            number *= base;
            number += match alphabet.find(c) {
                Some(index) => index,
                None => continue,
            };
        }

        number
    }

    /// Fetches a new access token.
    async fn refresh_token(&mut self, client: &Client) {
        let headers: HeaderMap =
            oauth::authorize_headers(self.client_id.as_str(), self.client_secret.secret());
        let response: Response = client
            .post("https://www.reddit.com/api/v1/access_token")
            .headers(headers)
            .body(
                "grant_type=refresh_token&refresh_token=".to_owned() + self.refresh_token.secret(),
            )
            .send()
            .await
            .expect("Could not refresh access token");

        if response.status() != StatusCode::OK {
            let json: Value = response
                .json()
                .await
                .expect("Cannot convert response into JSON");
            panic!("Server response: {:?}", json);
        }

        let json: Value = response
            .json()
            .await
            .expect("Cannot convert response into JSON");

        self.access_token = AccessToken::new(
            json["access_token"]
                .as_str()
                .expect("No access token found")
                .to_owned(),
        );

        if let Err(error) = self.cache.write("access_token", self.access_token.secret()) {
            warn!("Cannot write access token to cache: {:?}", error);
        }
    }
}
