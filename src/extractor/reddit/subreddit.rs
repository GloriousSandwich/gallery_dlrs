//! Responsible for extracting URLs from subreddit on reddit.com.

use std::collections::HashMap;

use async_trait::async_trait;
use fancy_regex::{Captures, Regex};
use reqwest::Client;
use serde_json::Value;
use tokio::sync::Mutex;

use crate::cache::Cache;
use crate::config::extractor::Reddit;
use crate::extractor::message::Message;
use crate::extractor::reddit::api::RedditAPI;
use crate::extractor::reddit::common::Common;
use crate::extractor::{Error, Extractor, ExtractorKind, SubExtractorKind};
use crate::text;

/// Extractor for URLs from subreddit on reddit.com.
pub struct RedditSubredditExtractor {
    /// The reddit API interface.
    api: Mutex<RedditAPI>,

    /// Shared types and methods for reddit extractors.
    common: Common,

    /// The URL query.
    queries: HashMap<String, String>,

    /// Name of the subreddit.
    subreddit: String,
}

impl RedditSubredditExtractor {
    /// Creates a new [`RedditSubredditExtractor`].
    pub fn new(
        url: &str,
        config: &Reddit,
        cache: &Cache,
        filename_format: &str,
    ) -> Result<Self, Error> {
        let url_regex: Regex = Regex::new(
            r#"(?:https?://)?(?:\w+\.)?reddit\.com(/r/[^/?#]+(?:/([a-z]+))?)/?(?:\?([^#]*))?(?:$|#)"#,
        ).expect("Cannot compile regex");

        let captures: Captures = url_regex
            .captures(url)?
            .ok_or(Error::SubExtractorNotFound)?;

        let subreddit: String = captures
            .get(1)
            .expect("Cannot find subreddit in URL")
            .as_str()
            .to_owned();

        let mut queries: HashMap<String, String> = text::parse_url_query_to_map(url);
        queries.insert("limit".to_owned(), "100".to_owned());

        Ok(Self {
            api: Mutex::new(RedditAPI::new(config.clone(), cache.clone())),
            common: Common::new(filename_format.to_string()),
            queries,
            subreddit,
        })
    }
}

#[async_trait]
impl Extractor for RedditSubredditExtractor {
    fn items(&self, metadata: Vec<Value>) -> Vec<(Message, Vec<Value>)> {
        self.common.items(metadata)
    }

    fn get_file_format(&self) -> &String {
        &self.common.filename_format
    }

    fn get_category(&self) -> ExtractorKind {
        ExtractorKind::Reddit
    }

    fn get_subextractor(&self) -> SubExtractorKind {
        SubExtractorKind::RedditSubreddit
    }

    async fn get_metadata(&self, client: Client) -> Vec<Value> {
        let submission: Vec<Value> = self
            .api
            .lock()
            .await
            .submission_subreddit(client, self.subreddit.as_str(), self.queries.clone())
            .await;

        submission
    }
}
