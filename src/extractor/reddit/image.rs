/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Responsible for extracting reddit-hosted images.

use async_trait::async_trait;
use fancy_regex::Regex;
use reqwest::Client;
use serde_json::Value;

use crate::extractor::message::Message;
use crate::extractor::{Error, Extractor, ExtractorKind, SubExtractorKind};
use crate::text;

/// Extractor for reddit hosted-images.
pub struct RedditImageExtractor {
    /// Image URL.
    url: String,
    /// Format for filename.
    filename_format: String,
}

impl RedditImageExtractor {
    /// Creates a new [`RedditImageExtractor`]
    pub fn new(url: &str, filename_format: &str) -> Result<Self, Error> {
        let url_regex: Regex =
            Regex::new(r#"(?:https?://)?i\.redd(?:\.it|ituploads\.com)/[^/?#]+(?:\?[^#]*)?"#)
                .expect("Could not compile regex");

        if url_regex.captures(url)?.is_some() {
            return Ok(Self {
                url: url.to_owned(),
                filename_format: filename_format.to_string(),
            });
        }

        Err(Error::SubExtractorNotFound)
    }
}

#[async_trait]
impl Extractor for RedditImageExtractor {
    fn items(&self, mut metadata: Vec<Value>) -> Vec<(Message, Vec<Value>)> {
        let (filename, extension) = text::extract_name_from_url(self.url.as_str());
        metadata[0]["filename"] = Value::String(filename);
        metadata[0]["extension"] = Value::String(extension);
        metadata[0]["image_url"] = Value::String(self.url.clone());
        metadata[0]["num"] = Value::from(1);

        vec![
            (Message::Directory, metadata.clone()),
            (Message::Url, metadata),
        ]
    }

    fn get_file_format(&self) -> &String {
        &self.filename_format
    }

    fn get_category(&self) -> ExtractorKind {
        ExtractorKind::Reddit
    }

    fn get_subextractor(&self) -> SubExtractorKind {
        SubExtractorKind::RedditImage
    }

    async fn get_metadata(&self, _: Client) -> Vec<Value> {
        // No metadata is needed for this extractor.
        vec![Value::default()]
    }
}
