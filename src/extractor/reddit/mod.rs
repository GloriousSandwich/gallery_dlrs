/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Module responsible for deserializing Reddit metadata.
pub mod api;
pub mod common;
pub mod home;
pub mod image;
pub mod submission;
pub mod subreddit;
pub mod user;
