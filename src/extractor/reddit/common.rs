//! Common types and methods for Reddit extractors.

use log::{debug, warn};
use serde_json::Value;

use crate::extractor::message::Message;
use crate::text;

/// Common types and methods for Reddit extractors.
pub struct Common {
    /// Format for the filename.
    pub filename_format: String,
}

impl Common {
    /// Creates a new [`Common`].
    pub fn new(filename_format: String) -> Self {
        Self { filename_format }
    }

    /// Extract image/video URLs and the metadata associated with it.
    // Clippy says something in this function can panic but I don't see it.
    #[allow(clippy::missing_panics_doc)]
    pub fn items(&self, mut metadata: Vec<Value>) -> Vec<(Message, Vec<Value>)> {
        let mut items: Vec<(Message, Vec<Value>)> = vec![(Message::Directory, metadata.clone())];
        let mut queuing_urls_exists_in_metadata: bool = false;
        let mut queuing_urls_exists_in_submission: bool = false;

        for submission in metadata.iter_mut() {
            let gallery_id: String = format!("Gallery: {}", submission["id"]);
            let mut queuing_urls: Vec<String> = Vec::new();

            // Skip if the submission has been removed from reddit.
            if !submission["removed_by_category"].is_null() {
                warn!("{}: has been removed from reddit", gallery_id);
                continue;
            }

            let url: String = submission["url"]
                .as_str()
                .expect("No URL found")
                .to_string();

            if url.starts_with("https://i.redd.it") {
                let (filename, extension) = text::extract_name_from_url(url.as_str());
                submission["filename"] = Value::String(filename);
                submission["extension"] = Value::String(extension);
                submission["num"] = Value::from(1);
            } else if url.starts_with("https://v.redd.it") {
                let (filename, extension) = text::extract_name_from_url(url.as_str());
                submission["filename"] = Value::String(filename);
                submission["extension"] = Value::String(extension);
                submission["num"] = Value::from(1);
                submission["url"] = Value::String(format!("ytdl:{}", url));
            }
            // Checks and extracts URLs if submission is a gallery. Ref: https://www.reddit.com/gallery/hrrh23
            else if let Some(gallery) = submission.get("gallery_data") {
                match submission.get("media_metadata") {
                    Some(metadata) => {
                        let items: &Vec<Value> = gallery["items"]
                            .as_array()
                            .expect("Cannot convert 'items' into array");

                        let mut urls: Vec<String> = Vec::new();

                        for item in items {
                            let media_id: String = item["media_id"]
                                .as_str()
                                .unwrap_or_else(|| panic!("{}: missing 'media_id'", gallery_id))
                                .to_string();

                            let data: Value = metadata[media_id].clone();

                            let status: &str = data["status"]
                                .as_str()
                                .expect("Cannot convert 'status' to bool");

                            if status != "valid" || data.get("s").is_none() {
                                warn!(
                                    "{}: skipping item {} (status: {})",
                                    gallery_id, item["media_id"], data["status"]
                                );
                            }

                            let src: &Value = data
                                .get("s")
                                .unwrap_or_else(|| panic!("{}: missing 's'", gallery_id));

                            let url = match (src.get("u"), src.get("gif"), src.get("mp4")) {
                                (Some(url), _, _) => url,
                                (_, Some(gif), _) => gif,
                                (_, _, Some(mp4)) => mp4,
                                _ => {
                                    panic!(
                                        "{}: unable to fetch download URL for item {}",
                                        gallery_id, item["media_id"]
                                    );
                                }
                            };

                            let url: String = url
                                .as_str()
                                .expect("Cannot convert url to string")
                                .to_owned();

                            let url: String = url.split('?').collect::<Vec<&str>>()[0]
                                .replace("/preview.", "/i.");
                            urls.push(url);
                        }

                        let mut filenames: Vec<String> = Vec::with_capacity(urls.len());
                        let mut extensions: Vec<String> = Vec::with_capacity(urls.len());

                        for url in urls.iter() {
                            let (filename, extension) = text::extract_name_from_url(url);
                            filenames.push(filename);
                            extensions.push(extension);
                        }

                        submission["nums"] = Value::from((0..urls.len()).collect::<Vec<usize>>());
                        submission["urls"] = Value::from(urls);
                        submission["filenames"] = Value::from(filenames);
                        submission["extensions"] = Value::from(extensions);
                    }
                    None => {
                        warn!("{}: missing 'media_metadata'", gallery_id);
                    }
                }
            } else if !submission["is_self"].as_bool().expect("") {
                debug!("Using a different extractor for {}", submission["id"]);

                queuing_urls.push(url.clone());
                queuing_urls_exists_in_submission = true;
                queuing_urls_exists_in_metadata = true;
            }

            if queuing_urls_exists_in_submission {
                submission["queuing_urls"] = Value::from(queuing_urls);
                queuing_urls_exists_in_submission = false;
            }
        }

        if queuing_urls_exists_in_metadata {
            items.push((Message::Queue, metadata.clone()));
        }

        items.push((Message::Url, metadata));

        items
    }
}
