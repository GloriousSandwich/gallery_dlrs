/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Responsible for extracting every image from an Artstation user.

use std::sync::Arc;

use async_trait::async_trait;
use fancy_regex::{Captures, Regex};
use futures::future;
use log::info;
use reqwest::{Client, Response};
use serde_json::Value;
use tokio::sync::Mutex;

use crate::extractor::artstation::common::Common;
use crate::extractor::message::Message;
use crate::extractor::{Error, Extractor, ExtractorKind, SubExtractorKind};

/// Extractor for all projects of an artstation user
pub struct ArtstationUserExtractor {
    /// Shared artstation extractor types and methods.
    common: Common,
    /// Artist username.
    username: String,
    /// Number of pages to extract projects from Artstation.
    depth: i64,
}

impl ArtstationUserExtractor {
    /// Construct a new [`ArtstationUserExtractor`].
    pub fn new(
        url: &str,
        depth: i64,
        filename_format: &String,
    ) -> Result<ArtstationUserExtractor, Error> {
        let url_regex_pattern = Regex::new(
            r"(?:https?://)?(?:(?:www\.)?artstation\.com/(?!artwork|projects|search)([^/?#]+)(?:/albums/all)?|((?!www)\w+)\.artstation\.com(?:/projects)?)/?$"
        ).expect("Could not compile regex");

        let captures: Captures = url_regex_pattern
            .captures(url)?
            .ok_or(Error::SubExtractorNotFound)?;

        let username = captures
            .get(1)
            .or_else(|| captures.get(2))
            .expect("Could not get artist's username from URL.")
            .as_str()
            .into();

        Ok(ArtstationUserExtractor {
            common: Common::new(filename_format.to_string()),
            username,
            depth,
        })
    }
}

#[async_trait]
impl Extractor for ArtstationUserExtractor {
    fn items(&self, metadata: Vec<Value>) -> Vec<(Message, Vec<Value>)> {
        self.common.items(metadata)
    }

    fn get_file_format(&self) -> &String {
        &self.common.filename_format
    }

    fn get_category(&self) -> ExtractorKind {
        ExtractorKind::Artstation
    }

    fn get_subextractor(&self) -> SubExtractorKind {
        SubExtractorKind::ArtstationUser
    }

    async fn get_metadata(&self, client: Client) -> Vec<Value> {
        // 1. Deserialize the user's metadata.
        let metadata: Arc<Mutex<Vec<Value>>> = Arc::new(Mutex::new(Vec::new()));
        let mut futures: Vec<_> = Vec::new();

        for page_number in 1..self.depth {
            let user_metadata_url = format!(
                "{}/users/{}/projects.json?page={}",
                self.common.root, self.username, page_number
            );

            let response: Response = Common::get_request(&client, user_metadata_url.as_str()).await;

            let user_page_json: Value = response
                .json()
                .await
                .expect("Could not deserialize user page json");

            let data: Vec<Value> = match user_page_json["data"].as_array() {
                Some(data) => data.clone(),
                None => break,
            };

            // 2. Deserialize each user's artwork.
            for project in data.into_iter() {
                let client_cloned: Client = client.clone();
                let root: String = self.common.root.clone();
                let metadata_cloned: Arc<Mutex<Vec<Value>>> = Arc::clone(&metadata);

                futures.push(tokio::spawn(async move {
                    let hash_id: &str = project["hash_id"]
                        .as_str()
                        .expect("Could not get project hash ID");

                    let project_metadata_url = format!("{}/projects/{}.json", root, hash_id);

                    let response: Response =
                        Common::get_request(&client_cloned, project_metadata_url.as_str()).await;

                    let json: Value = response
                        .json()
                        .await
                        .expect("Failed deserializing project assets");

                    #[allow(clippy::unwrap_used)]
                    metadata_cloned.lock().await.push(json);
                }));
            }
        }
        future::join_all(futures).await;
        info!("Deserialized {}/projects.json", self.username);

        #[allow(clippy::unwrap_used)]
        let metadata: Vec<Value> = Arc::try_unwrap(metadata).unwrap().into_inner();
        metadata
    }
}
