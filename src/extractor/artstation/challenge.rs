/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Extractor implementation for Artstation challenges.

use std::sync::Arc;

use async_trait::async_trait;
use fancy_regex::{Captures, Regex};
use futures::future;
use log::info;
use reqwest::{Client, Response};
use serde_json::Value;
use tokio::sync::Mutex;

use crate::extractor::artstation::common::Common;
use crate::extractor::message::Message;
use crate::extractor::{Error, Extractor, ExtractorKind, SubExtractorKind};
use crate::text;

/// Extractor for submissions of artstation challenges.
pub struct ArtstationChallengeExtractor {
    /// Shared artstation extractor types and methods.
    common: Common,
    /// Format for the filename.
    filename_format: String,
    /// ID for each challenge.
    challenge_id: String,
    /// A query for sorting artstation search results.
    sorting: String,
    /// Number of pages to extract projects from Artstation.
    depth: i64,
}

impl ArtstationChallengeExtractor {
    /// Construct a new [`ArtstationChallengeExtractor`].
    pub fn new(
        url: &str,
        depth: i64,
        filename_format: &String,
    ) -> Result<ArtstationChallengeExtractor, Error> {
        let url_regex_pattern = Regex::new(
            r"(?:https?://)?(?:www\.)?artstation\.com/contests/[^/?#]+/challenges/(\d+)/?(?:\?sorting=([a-z]+))?",
        ).expect("Could not compile regex");

        let captures: Captures = url_regex_pattern
            .captures(url)?
            .ok_or(Error::SubExtractorNotFound)?;

        let challenge_id = captures
            .get(1)
            .expect("Could not get challenge_id from URL.")
            .as_str()
            .into();

        let sorting = match captures.get(2) {
            Some(capture) => capture.as_str().into(),
            None => "popular".to_owned(),
        };

        Ok(ArtstationChallengeExtractor {
            common: Common::new(filename_format.to_string()),
            filename_format: "{submission_id}_{asset_id}_{filename}.{extension}".to_string(),
            challenge_id,
            sorting,
            depth,
        })
    }

    /// Get an image's submission ID from its URL.
    ///
    /// # Examples
    /// ```
    /// # use gallery_dlrs::extractor::artstation::challenge::ArtstationChallengeExtractor;
    ///
    /// let url: &str = "https://cdna.artstation.com/p/media_assets/images/images/000/108/236/large/wip4.jpg?1498534544";
    /// assert_eq!(ArtstationChallengeExtractor::id_from_url(url), 108236);
    /// ```
    ///
    /// # Panics
    ///
    /// This function panics if the submission ID could not be found.
    pub fn id_from_url(url: &str) -> i64 {
        let url_parts: Vec<&str> = url.split('/').collect();
        let id_parts = &url_parts[7..10];
        let mut id = String::with_capacity(9);
        for part in id_parts.iter() {
            id.push_str(part.to_owned());
        }
        #[allow(clippy::unwrap_used)]
        let id: i64 = id.parse().unwrap();
        id
    }
}

#[async_trait]
impl Extractor for ArtstationChallengeExtractor {
    fn items(&self, metadata: Vec<Value>) -> Vec<(Message, Vec<Value>)> {
        vec![
            (Message::Directory, metadata.clone()),
            (Message::Url, metadata),
        ]
    }

    fn get_file_format(&self) -> &String {
        &self.filename_format
    }

    fn get_category(&self) -> ExtractorKind {
        ExtractorKind::Artstation
    }

    fn get_subextractor(&self) -> SubExtractorKind {
        SubExtractorKind::ArtstationChallenge
    }

    async fn get_metadata(&self, client: Client) -> Vec<Value> {
        let metadata: Arc<Mutex<Vec<Value>>> = Arc::new(Mutex::new(Vec::new()));
        let mut futures: Vec<_> = Vec::new();

        // 1. Multiple irrelevant JSON files has to be deserialize to get to the
        // important JSON file.
        let challenge_metadata_url: String = format!(
            "{}/contests/_/challenges/{}.json",
            self.common.root, self.challenge_id
        );

        let response: Response =
            Common::get_request(&client, challenge_metadata_url.as_str()).await;

        let challenge_metadata: Value = response
            .json()
            .await
            .expect("Could not deserialize challenge metadata");

        for page_number in 1..self.depth {
            let submission_metadata_url: String = format!(
                "{}/contests/_/challenges/{}/submissions.json?sorting={}&page={}",
                self.common.root, self.challenge_id, self.sorting, page_number
            );
            let response: Response =
                Common::get_request(&client, submission_metadata_url.as_str()).await;

            let submission_metadata: Value = response
                .json()
                .await
                .expect("Could not deserialize submissions json");

            let data: Vec<Value> = match submission_metadata["data"].as_array() {
                Some(data) => data.clone(),
                None => break,
            };

            for submission in data.into_iter() {
                let client_cloned: Client = client.clone();
                let root: String = self.common.root.clone();
                let sorting: String = self.sorting.clone();
                let metadata_cloned: Arc<Mutex<Vec<Value>>> = Arc::clone(&metadata);
                let challenge_title: String = challenge_metadata["title"]
                    .as_str()
                    .expect("Challenge has no title")
                    .to_owned();

                let challenge_id: i64 = challenge_metadata["id"]
                    .as_i64()
                    .expect("Could not get challenge ID");

                futures.push(tokio::spawn(async move {
                    // 2. This is the important JSON file that will be used during the rest of the
                    // program.
                    let update_metadata_url: String = format!(
                        "{}/contests/submission_updates.json?sorting={}&submission_id={}",
                        root, sorting, submission["id"]
                    );

                    let response: Response =
                        Common::get_request(&client_cloned, update_metadata_url.as_str()).await;

                    let update_metadata: Value = response
                        .json()
                        .await
                        .expect("Could not deserialize submission update json");

                    let data: Vec<Value> = update_metadata["data"]
                        .as_array()
                        .expect("Data does not exists in metadata")
                        .clone();

                    for mut update in data {
                        // The image urls are hidden in `body_presentation_html` along with
                        // other unnecessary data.
                        let body_presentation_html: String = update["body_presentation_html"]
                            .as_str()
                            .expect("Could not convert body_presentation_html to string")
                            .to_string();

                        let urls: Vec<String> = match text::extract_all(
                            body_presentation_html.as_str(),
                            r#"https://cdna.artstation.com"#,
                            r#"""#,
                        ) {
                            Some(urls) => urls,
                            None => break,
                        };

                        let urls: Vec<String> = urls
                            .iter()
                            .map(|url| format!("https://cdna.artstation.com{}", url))
                            .collect();

                        let mut asset_ids: Vec<i64> = Vec::with_capacity(urls.len());
                        let mut filenames: Vec<String> = Vec::with_capacity(urls.len());
                        let mut extentions: Vec<String> = Vec::with_capacity(urls.len());

                        for url in urls.iter() {
                            let asset_id: i64 = ArtstationChallengeExtractor::id_from_url(url);
                            let (filename, extension) = text::extract_name_from_url(url.as_str());
                            filenames.push(filename);
                            extentions.push(extension);
                            asset_ids.push(asset_id);
                        }

                        update["asset_ids"] = Value::from(asset_ids);
                        update["urls"] = Value::from(urls);
                        update["filenames"] = Value::from(filenames);
                        update["extensions"] = Value::from(extentions);
                        update["challenge_id"] = Value::from(challenge_id);
                        update["challenge_title"] = Value::from(challenge_title.clone());

                        metadata_cloned.lock().await.push(update);
                    }
                }));
            }
        }
        future::join_all(futures).await;
        info!("Deserialize challenge {}", self.challenge_id);

        #[allow(clippy::unwrap_used)]
        let metadata: Vec<Value> = Arc::try_unwrap(metadata).unwrap().into_inner();
        metadata
    }
}
