/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Responsible for extracting liked projects of an artstation user.

use std::sync::Arc;

use async_trait::async_trait;
use fancy_regex::{Captures, Regex};
use futures::future;
use log::info;
use reqwest::{Client, Response};
use serde_json::Value;
use tokio::sync::Mutex;

use crate::extractor::artstation::common::Common;
use crate::extractor::message::Message;
use crate::extractor::{Error, Extractor, ExtractorKind, SubExtractorKind};

/// Extractor for liked projects of an artstation user.

pub struct ArtstationLikesExtractor {
    /// Shared artstation extractor types and methods.
    common: Common,
    /// Name of user.
    username: String,
    /// Number of pages to extract projects from Artstation.
    depth: i64,
}

impl ArtstationLikesExtractor {
    /// Construct a new [`ArtstationImageExtractor`].
    pub fn new(
        url: &str,
        depth: i64,
        filename_format: &String,
    ) -> Result<ArtstationLikesExtractor, Error> {
        let url_regex_pattern = Regex::new(
            r"(?:https?://)?(?:www\.)?artstation\.com/(?!artwork|projects|search)([^/?#]+)/likes/?",
        )
        .expect("Could not compile regex");

        let captures: Captures = url_regex_pattern
            .captures(url)?
            .ok_or(Error::SubExtractorNotFound)?;

        let username: String = captures
            .get(1)
            .expect("Could not get username from URL.")
            .as_str()
            .to_owned();

        Ok(ArtstationLikesExtractor {
            common: Common::new(filename_format.to_string()),
            username,
            depth,
        })
    }
}

#[async_trait]
impl Extractor for ArtstationLikesExtractor {
    fn items(&self, metadata: Vec<Value>) -> Vec<(Message, Vec<Value>)> {
        self.common.items(metadata)
    }

    fn get_file_format(&self) -> &String {
        &self.common.filename_format
    }

    fn get_category(&self) -> ExtractorKind {
        ExtractorKind::Artstation
    }

    fn get_subextractor(&self) -> SubExtractorKind {
        SubExtractorKind::ArtstationLikes
    }

    async fn get_metadata(&self, client: Client) -> Vec<Value> {
        let metadata: Arc<Mutex<Vec<Value>>> = Arc::new(Mutex::new(Vec::new()));
        let mut futures: Vec<_> = Vec::new();

        // 1. Deserialize the user's likes metadata.
        for page_number in 1..self.depth {
            let likes_metadata_url = format!(
                "{}/users/{}/likes.json?page={}",
                self.common.root, self.username, page_number
            );

            let response: Response =
                Common::get_request(&client, likes_metadata_url.as_str()).await;

            let likes_metadata: Value = response
                .json()
                .await
                .expect("Failed deserializing likes metadata");

            let data: Vec<Value> = match likes_metadata["data"].as_array() {
                Some(data) => data.clone(),
                None => break,
            };

            // 2. Deserialize the metadata for each liked project.
            for project in data.into_iter() {
                let client_cloned: Client = client.clone();
                let root: String = self.common.root.clone();
                let metadata_cloned: Arc<Mutex<Vec<Value>>> = Arc::clone(&metadata);
                let username: String = self.username.clone();

                futures.push(tokio::spawn(async move {
                    let hash_id: &str = project["hash_id"]
                        .as_str()
                        .expect("Could not get project hash ID");

                    let project_metadata_url = format!("{}/projects/{}.json", root, hash_id);

                    let response: Response =
                        Common::get_request(&client_cloned, project_metadata_url.as_str()).await;

                    let mut json: Value = response
                        .json()
                        .await
                        .expect("Failed deserializing project assets");

                    // Replace the name of the person who created the artwork with the person who
                    // liked the artwork.
                    json["user"]["username"] = Value::String(username);
                    metadata_cloned.lock().await.push(json);
                }));
            }
        }
        future::join_all(futures).await;
        info!("Deserialized {}/likes.json", self.username);

        #[allow(clippy::unwrap_used)]
        let metadata: Vec<Value> = Arc::try_unwrap(metadata).unwrap().into_inner();
        metadata
    }
}
