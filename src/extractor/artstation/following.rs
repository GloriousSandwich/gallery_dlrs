/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Extractor implementation for an Artstation user's following page.

use std::sync::Arc;

use async_trait::async_trait;
use fancy_regex::{Captures, Regex};
use futures::future;
use log::info;
use reqwest::{Client, Response};
use serde_json::Value;
use tokio::sync::Mutex;

use crate::extractor::artstation::common::Common;
use crate::extractor::message::Message;
use crate::extractor::{Error, Extractor, ExtractorKind, SubExtractorKind};

/// Extractor for a user's followed users.
pub struct ArtstationFollowingExtractor {
    /// Shared artstation extractor types and methods.
    common: Common,
    /// Name of user.
    username: String,
    /// Number of pages to extract projects from Artstation.
    depth: i64,
}

impl ArtstationFollowingExtractor {
    /// Construct a new [`ArtstationFollowingExtractor`].
    pub fn new(
        url: &str,
        depth: i64,
        filename_format: &String,
    ) -> Result<ArtstationFollowingExtractor, Error> {
        let url_regex_pattern = Regex::new(
            r"(?:https?://)?(?:www\.)?artstation\.com/(?!artwork|projects|search)([^/?#]+)/following",
        )
            .expect("Could not compile regex");

        let captures: Captures = url_regex_pattern
            .captures(url)?
            .ok_or(Error::SubExtractorNotFound)?;

        let username: String = captures
            .get(1)
            .expect("Could not get username from URL.")
            .as_str()
            .to_string();

        Ok(ArtstationFollowingExtractor {
            common: Common::new(filename_format.to_string()),
            username,
            depth,
        })
    }
}

#[async_trait]
impl Extractor for ArtstationFollowingExtractor {
    fn items(&self, metadata: Vec<Value>) -> Vec<(Message, Vec<Value>)> {
        self.common.items(metadata)
    }

    fn get_file_format(&self) -> &String {
        &self.common.filename_format
    }

    fn get_category(&self) -> ExtractorKind {
        ExtractorKind::Artstation
    }

    fn get_subextractor(&self) -> SubExtractorKind {
        SubExtractorKind::ArtstationFollowing
    }

    async fn get_metadata(&self, client: Client) -> Vec<Value> {
        let metadata: Arc<Mutex<Vec<Value>>> = Arc::new(Mutex::new(Vec::new()));
        let mut futures: Vec<_> = Vec::new();

        // 1. Deserialize the user's following page metadata.
        for page_number in 1..self.depth {
            let following_metadata_url: String = format!(
                "{}/users/{}/following.json?page={}",
                self.common.root, self.username, page_number
            );

            // TODO: Add proper error handling for status code 404.
            let response: Response =
                Common::get_request(&client, following_metadata_url.as_str()).await;

            let following_metadata: Value = response
                .json()
                .await
                .expect("Could not deserialize following metadata");

            let data: Vec<Value> = match following_metadata["data"].as_array() {
                Some(data) => data.clone(),
                None => break,
            };

            let mut following_futures: Vec<_> = Vec::new();

            // 2. Deserialize each follower's metadata.
            for following_user in data.into_iter() {
                let client_cloned: Client = client.clone();
                let root: String = self.common.root.clone();
                let depth: i64 = self.depth;
                let metadata_cloned: Arc<Mutex<Vec<Value>>> = Arc::clone(&metadata);

                following_futures.push(tokio::spawn(async move {
                    'inner: for page_number_inner in 1..depth {
                        let username: &str = following_user["username"]
                            .as_str()
                            .expect("Could not find username");

                        let user_metadata_url = format!(
                            "{}/users/{}/projects.json?page={}",
                            root, username, page_number_inner
                        );

                        let response: Response =
                            Common::get_request(&client_cloned, user_metadata_url.as_str()).await;

                        let user_metadata: Value = response
                            .json()
                            .await
                            .expect("Could not deserialize follower metadata");

                        let data_inner: Vec<Value> = match user_metadata["data"].as_array() {
                            Some(data_inner) => data_inner.clone(),
                            None => break 'inner,
                        };

                        // 3. Deserialize each follower's artwork.
                        for project in data_inner.into_iter() {
                            let hash_id: &str = project["hash_id"]
                                .as_str()
                                .expect("Could not get project hash ID");

                            let project_metadata_url =
                                format!("{}/projects/{}.json", root, hash_id);
                            let response: Response =
                                Common::get_request(&client_cloned, project_metadata_url.as_str())
                                    .await;

                            let json: Value = response
                                .json()
                                .await
                                .expect("Failed deserializing follower project assets");

                            metadata_cloned.lock().await.push(json);
                        }
                    }
                }));
            }
            futures.push(following_futures);
        }

        for future in futures.into_iter() {
            future::join_all(future).await;
        }

        info!("Deserialized {}/following.json", self.username);

        #[allow(clippy::unwrap_used)]
        let metadata: Vec<Value> = Arc::try_unwrap(metadata).unwrap().into_inner();

        metadata
    }
}
