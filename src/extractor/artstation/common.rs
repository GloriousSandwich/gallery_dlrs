/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Common structs and settings shared between Artstation extractors.
use std::process::exit;

use base64;
use log::error;
use reqwest::{Client, Response};
use serde::{Deserialize, Serialize};
use serde_json::Value;

use crate::extractor::message::Message;
use crate::text;

/// If an image quality, for example a 4k image, does not exists, fallback to
/// the next highest image quality.
///
/// # Example
/// ```ignore
/// // The URL below is `ImageFallbackUrl::Large`, however if the file  
/// // does not exist, fallback to the next highest quality.
/// "https://cdnb.artstation.com/p/assets/images/images/049/024/707/large/-s-t-e-l-l-a-n-t-e-cruella.jpg?1651518684"
///
/// // `ImageFallbackUrl::Medium`
/// "https://cdnb.artstation.com/p/assets/images/images/049/024/707/medium/-s-t-e-l-l-a-n-t-e-cruella.jpg?1651518684"
///
/// // `ImageFallbackUrl::Small`
/// "https://cdnb.artstation.com/p/assets/images/images/049/024/707/small/-s-t-e-l-l-a-n-t-e-cruella.jpg?1651518684"
/// ```
#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum ImageFallbackUrl {
    /// 1080p image url? Not sure of the exact quality.
    Large(String),
    /// 720p image url? Not sure of the exact quality.
    Medium(String),
    /// 360p image url? Not sure of the exact quality.
    Small(String),
}

/// Common settings shared between artstation extractors.
pub struct Common {
    /// Artstation website root URL.
    pub root: String,
    /// Format for filename.
    pub filename_format: String,
}

impl Common {
    /// Construct a new [`ArtstationExtractor`].
    pub fn new(filename_format: String) -> Common {
        Common {
            root: "https://www.artstation.com".to_owned(),
            filename_format,
        }
    }

    /// Extract the image URL and the metadata associated with it.
    pub fn items(&self, mut metadata: Vec<Value>) -> Vec<(Message, Vec<Value>)> {
        let mut items = Vec::new();

        #[allow(clippy::unwrap_used, irrefutable_let_patterns)]
        items.push((Message::Directory, metadata.clone()));

        for artwork in metadata.iter_mut() {
            let assets: Vec<Value> = artwork["assets"]
                .as_array()
                .expect("No assets in artwork")
                .clone();

            // Using `enumerate()` here I think is a hacky way to satisfy the borrow
            // checker.
            for (index, asset) in assets.iter().enumerate() {
                if artwork["title"].is_null() {
                    artwork["assets"][index]["title"] = Value::String("".to_owned());
                } else {
                    let title: String = artwork["title"]
                        .as_str()
                        .expect("Artwork has no title")
                        .to_owned();

                    artwork["assets"][index]["title"] = Value::String(title);
                }

                artwork["assets"][index]["category"] = Value::String("artstation".to_owned());

                let asset_has_image: bool = asset["has_image"]
                    .as_bool()
                    .expect("Asset missing field: has_image");

                if asset_has_image {
                    let image_url: String = asset["image_url"]
                        .as_str()
                        .expect("Asset has no image_url")
                        .to_owned();

                    let (filename, extension) = text::extract_name_from_url(image_url.as_str());
                    artwork["assets"][index]["filename"] = Value::String(filename);
                    artwork["assets"][index]["extension"] = Value::String(extension);
                    artwork["assets"][index]["image_url"] =
                        Value::String(Self::no_cache(image_url.as_str()));

                    let image_url_temp: String = image_url.clone();
                    let image_url_parts: Vec<&str> = image_url_temp.split("/large/").collect();

                    if image_url_parts.get(1).is_some() {
                        let left_part: &str = image_url_parts[0];
                        let right_part: &str = image_url_parts[1];
                        artwork["assets"][index]["image_url"] =
                            Value::String(format!("{}/4k/{}", left_part, right_part));

                        let fallback_urls: Vec<ImageFallbackUrl> = vec![
                            ImageFallbackUrl::Large(format!("{}/large/{}", left_part, right_part)),
                            ImageFallbackUrl::Medium(format!(
                                "{}/medium/{}",
                                left_part, right_part
                            )),
                            ImageFallbackUrl::Small(format!("{}/small/{}", left_part, right_part)),
                        ];

                        let fallback_urls: Value = serde_json::to_value(fallback_urls)
                            .expect("Could not serialize fallback URLs");

                        artwork["assets"][index]["fallback_urls"] = fallback_urls;
                    }
                }
            }
        }
        items.push((Message::Url, metadata));
        items
    }

    /// Make a `GET` request to a URL.
    pub async fn get_request(client: &Client, url: &str) -> Response {
        // TODO: Add proper error handling for status code 404.
        client.get(url).send().await.unwrap_or_else(|error| {
            error!("{error}");
            exit(1);
        })
    }

    /// Cause a cache miss to prevent Cloudflare 'optimizations'.
    ///
    /// Cloudflare's 'Polish' optimization strips image metadata and may even
    /// recompress an image as lossy JPEG. This can be prevented by causing
    /// a cache miss when requesting an image by adding a random dummy query
    /// parameter.
    pub fn no_cache(url: &str) -> String {
        // TODO: Add random 64-bit numbers instead of hard coded value.
        let bytes: u64 = rand::random();
        let bytes: String = if cfg!(target_endian = "big") {
            base64::encode(bytes.to_be_bytes())
        } else {
            base64::encode(bytes.to_le_bytes())
        };

        let dummy_parameter: String = format!("gallery_dlrs_no_cache={}", base64::encode(bytes));
        let separator: char = if url.contains('?') { '&' } else { '?' };
        format!("{}{}{}", url, separator, dummy_parameter)
    }
}
