/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Responsible for extracting individual images/projects.

use async_trait::async_trait;
use fancy_regex::{Captures, Regex};
use log::debug;
use reqwest::{Client, Response};
use serde_json::Value;

use crate::extractor::artstation::common::Common;
use crate::extractor::message::Message;
use crate::extractor::{Error, Extractor, ExtractorKind, SubExtractorKind};

/// Extractor for images from a single artstation project.
pub struct ArtstationImageExtractor {
    /// Shared artstation extractor types and methods.
    common: Common,
    /// Project ID of artwork.
    project_id: String,
    // Type of sub extractor.
    //sub_extractor: SubExtractor,
}

impl ArtstationImageExtractor {
    /// Construct a new [`ArtstationImageExtractor`].
    pub fn new(url: &str, filename_format: &String) -> Result<ArtstationImageExtractor, Error> {
        let url_regex_pattern = Regex::new(
            r"(?:https?://)?(?:(?:\w+\.)?artstation\.com/(?:artwork|projects|search)|artstn\.co/p)/(\w+)"
        ).expect("Could not compile regex");

        let captures: Captures = url_regex_pattern
            .captures(url)?
            .ok_or(Error::SubExtractorNotFound)?;

        let project_id: String = captures
            .get(1)
            .expect("Could not get project_id from URL.")
            .as_str()
            .to_owned();

        Ok(ArtstationImageExtractor {
            common: Common::new(filename_format.to_string()),
            project_id,
        })
    }
}

#[async_trait]
impl Extractor for ArtstationImageExtractor {
    fn items(&self, metadata: Vec<Value>) -> Vec<(Message, Vec<Value>)> {
        self.common.items(metadata)
    }

    fn get_file_format(&self) -> &String {
        &self.common.filename_format
    }

    fn get_category(&self) -> ExtractorKind {
        ExtractorKind::Artstation
    }

    fn get_subextractor(&self) -> SubExtractorKind {
        SubExtractorKind::ArtstationImage
    }

    async fn get_metadata(&self, client: Client) -> Vec<Value> {
        let project_metadata_url =
            format!("{}/projects/{}.json", self.common.root, self.project_id);
        // TODO: Add proper error handling for status code 404.
        let response: Response = Common::get_request(&client, project_metadata_url.as_str()).await;
        let json: Value = response
            .json()
            .await
            .expect("Failed deserializing project assets");

        debug!("Deserialized {}.json", self.project_id);
        let metadata: Vec<Value> = vec![json];
        metadata
    }
}
