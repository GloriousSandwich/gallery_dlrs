/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Module responsible for extracting and deserializing Artstation metadata.
pub mod album;
pub mod artwork;
pub mod challenge;
pub mod common;
pub mod following;
pub mod image;
pub mod likes;
pub mod search;
pub mod user;
