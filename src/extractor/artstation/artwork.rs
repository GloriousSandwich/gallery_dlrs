/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Extractor implementation for Artstation's artwork page.

use std::sync::Arc;

use async_trait::async_trait;
use fancy_regex::Regex;
use futures::future;
use reqwest::{Client, Response};
use serde_json::Value;
use tokio::sync::Mutex;

use crate::extractor::artstation::common::Common;
use crate::extractor::message::Message;
use crate::extractor::{Error, Extractor, ExtractorKind, SubExtractorKind};

/// Extractor for projects on Artstation's artwork page
pub struct ArtstationArtworkExtractor {
    /// Shared artstation extractor types and methods.
    common: Common,
    /// Type of URL query.
    ///
    /// # Example
    ///```ignore
    ///     https://www.artstation.com/artwork?sorting=latest
    ///                                       <-----query---->
    ///     query == ["sorting", "latest"]
    /// ```
    query: Vec<String>,
    /// Number of pages to extract projects from Artstation.
    depth: i64,
}

impl ArtstationArtworkExtractor {
    /// Construct a new [`ArtstationArtworkExtractor`].
    pub fn new(
        url: &str,
        depth: i64,
        filename_format: &String,
    ) -> Result<ArtstationArtworkExtractor, Error> {
        let url_regex_pattern =
            Regex::new(r"(?:https?://)?(?:\w+\.)?artstation\.com/artwork/?\?([^#]+)")
                .expect("Could not compile regex");

        let query: String;

        if let Some(captures) = url_regex_pattern.captures(url)? {
            query = captures
                .get(1)
                .expect("Could not get project_id from URL.")
                .as_str()
                .into();
        } else {
            return Err(Error::SubExtractorNotFound);
        }

        let query: Vec<String> = query.split('=').map(|x| x.to_owned()).collect();
        Ok(ArtstationArtworkExtractor {
            common: Common::new(filename_format.to_string()),
            query,
            depth,
        })
    }
}

#[async_trait]
impl Extractor for ArtstationArtworkExtractor {
    fn items(&self, metadata: Vec<Value>) -> Vec<(Message, Vec<Value>)> {
        self.common.items(metadata)
    }

    fn get_file_format(&self) -> &String {
        &self.common.filename_format
    }

    fn get_category(&self) -> ExtractorKind {
        ExtractorKind::Artstation
    }

    fn get_subextractor(&self) -> SubExtractorKind {
        SubExtractorKind::ArtstationArtwork
    }

    async fn get_metadata(&self, client: Client) -> Vec<Value> {
        let metadata: Arc<Mutex<Vec<Value>>> = Arc::new(Mutex::new(Vec::new()));
        let mut futures: Vec<_> = Vec::new();

        for page_number in 1..self.depth {
            let artwork_metadata_url = format!(
                "{}/projects.json?{}={}&page={}",
                self.common.root, self.query[0], self.query[1], page_number
            );

            let response: Response =
                Common::get_request(&client, artwork_metadata_url.as_str()).await;

            let artwork_json: Value = response
                .json()
                .await
                .expect("Failed deserializing proxy json");

            let data: Vec<Value> = match artwork_json["data"].as_array() {
                Some(data) => data.clone(),
                None => break,
            };

            for project in data.into_iter() {
                let client_cloned: Client = client.clone();
                let root: String = self.common.root.clone();
                let query: String = self.query[1].clone();
                let metadata_cloned: Arc<Mutex<Vec<Value>>> = Arc::clone(&metadata);

                futures.push(tokio::spawn(async move {
                    let hash_id: &str = project["hash_id"]
                        .as_str()
                        .expect("Could not get project hash ID");

                    let project_metadata_url = format!("{}/projects/{}.json", root, hash_id);

                    let response: Response =
                        Common::get_request(&client_cloned, project_metadata_url.as_str()).await;

                    let mut json: Value = response
                        .json()
                        .await
                        .expect("Failed deserializing project assets");

                    json["query"] = Value::String(query);
                    metadata_cloned.lock().await.push(json);
                }));
            }
        }
        future::join_all(futures).await;

        #[allow(clippy::unwrap_used)]
        let metadata: Vec<Value> = Arc::try_unwrap(metadata).unwrap().into_inner();
        metadata
    }
}
