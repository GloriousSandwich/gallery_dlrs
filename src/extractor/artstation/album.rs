/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Extractor implementation for Artstation albums.

use std::process::exit;
use std::sync::Arc;

use async_trait::async_trait;
use fancy_regex::{Captures, Regex};
use futures::future;
use log::{error, info};
use reqwest::{Client, Response};
use serde_json::Value;
use tokio::sync::Mutex;

use crate::extractor::artstation::common::Common;
use crate::extractor::message::Message;
use crate::extractor::{Error, Extractor, ExtractorKind, SubExtractorKind};

/// Extractor for all projects in an artstation album
pub struct ArtstationAlbumExtractor {
    /// Shared artstation extractor types and methods.
    common: Common,
    /// Artist username.
    username: String,
    /// ID for the provided album.
    album_id: String,
}

impl ArtstationAlbumExtractor {
    /// Construct a new [`ArtstationAlbumExtractor`].
    pub fn new(url: &str, filename_format: &String) -> Result<ArtstationAlbumExtractor, Error> {
        let url_regex_pattern = Regex::new(
            r"(?:https?://)?(?:(?:www\.)?artstation\.com/(?!artwork|projects|search)([^/?#]+)|((?!www)\w+)\.artstation\.com)/albums/(\d+)",
        ).expect("Could not compile regex");

        let captures: Captures = url_regex_pattern
            .captures(url)?
            .ok_or(Error::SubExtractorNotFound)?;

        let username = captures
            .get(1)
            .or_else(|| captures.get(2))
            .expect("Could not get artist's username from URL")
            .as_str()
            .into();

        let album_id = captures
            .get(3)
            .expect("Could not get album id from URL.")
            .as_str()
            .into();

        Ok(ArtstationAlbumExtractor {
            common: Common::new(filename_format.to_string()),
            username,
            album_id,
        })
    }
}

#[async_trait]
impl Extractor for ArtstationAlbumExtractor {
    fn items(&self, metadata: Vec<Value>) -> Vec<(Message, Vec<Value>)> {
        self.common.items(metadata)
    }

    fn get_file_format(&self) -> &String {
        &self.common.filename_format
    }

    fn get_category(&self) -> ExtractorKind {
        ExtractorKind::Artstation
    }

    fn get_subextractor(&self) -> SubExtractorKind {
        SubExtractorKind::ArtstationAlbum
    }

    async fn get_metadata(&self, client: Client) -> Vec<Value> {
        let metadata: Arc<Mutex<Vec<Value>>> = Arc::new(Mutex::new(Vec::new()));
        let mut futures: Vec<_> = Vec::new();

        // 1. Fetches the user metadata and looks for the album id.
        let user_metadata_url: String =
            format!("{}/users/{}/quick.json", self.common.root, self.username);

        let response: Response = Common::get_request(&client, user_metadata_url.as_str()).await;

        let user_metadata: Value = response.json().await.unwrap_or_else(|error| {
            panic!(
                "Failed deserializing url \"{}\": {}",
                user_metadata_url.as_str(),
                error
            );
        });

        let albums: Vec<Value> = match user_metadata["albums_with_community_projects"].as_array() {
            Some(albums) => albums.clone(),
            None => {
                error!("User has no albums");
                exit(1);
            }
        };

        let album: Value = albums
            .into_iter()
            .find(|album| {
                album["id"]
                    == self
                        .album_id
                        .parse::<i64>()
                        .expect("Could not convert album_id to i64")
            })
            .expect("Could not match album ID from user metadata");

        let album: Arc<Value> = Arc::new(album);

        // 2. Album id found, now deserialize projects with the album id.
        let album_metadata_url: String = format!(
            "{}/users/{}/projects.json?album_id={}",
            self.common.root.clone(),
            self.username.clone(),
            album["id"]
        );

        let response: Response = Common::get_request(&client, album_metadata_url.as_str()).await;

        let album_metadata: Value = response
            .json()
            .await
            .expect("Failed deserializing album metadata");

        let data: Vec<Value> = match album_metadata["data"].as_array() {
            Some(data) => data.clone(),
            None => {
                error!("Could not fetch album project assets");
                exit(1);
            }
        };

        // 3. Deserialize artwork metadata in the album.
        for project in data.into_iter() {
            let root: String = self.common.root.clone();
            let metadata_cloned: Arc<Mutex<Vec<Value>>> = Arc::clone(&metadata);
            let client_cloned: Client = client.clone();
            let album_cloned: Arc<Value> = album.clone();

            let album_title: String = match album_cloned["title"].as_str() {
                Some(album_title) => album_title.to_owned(),
                None => {
                    error!("Album does not have a title");
                    exit(1);
                }
            };

            futures.push(tokio::spawn(async move {
                let hash_id: &str = project["hash_id"]
                    .as_str()
                    .expect("Could not get project hash ID");

                let project_metadata_url = format!("{}/projects/{}.json", root, hash_id);
                let response: Response =
                    Common::get_request(&client_cloned, project_metadata_url.as_str()).await;

                let mut json: Value = response
                    .json()
                    .await
                    .expect("Failed deserializing album project assets");

                json["album_title"] = Value::String(album_title);
                json["album_id"] = album_cloned["id"].clone();

                metadata_cloned.lock().await.push(json);
            }));
        }
        future::join_all(futures).await;
        info!("Deserialized Album");

        #[allow(clippy::unwrap_used)]
        let metadata: Vec<Value> = Arc::try_unwrap(metadata).unwrap().into_inner();
        metadata
    }
}
