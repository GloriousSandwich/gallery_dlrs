/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Responsible for artstation search results.

use std::sync::Arc;

use async_trait::async_trait;
use fancy_regex::{Captures, Regex};
use futures::future;
use log::info;
use reqwest::{Client, Response};
use serde_json::Value;
use tokio::sync::Mutex;

use crate::extractor::artstation::common::Common;
use crate::extractor::message::Message;
use crate::extractor::{Error, Extractor, ExtractorKind, SubExtractorKind};
use crate::text;

/// Extractor for artstation search results.
pub struct ArtstationSearchExtractor {
    /// Shared artstation extractor types and methods.
    common: Common,
    /// Search URL query
    ///
    /// # Example
    ///```ignore
    ///     https://www.artstation.com/search?query=ancient&sort_by=rank
    ///                                       <---query--->
    ///     query == "ancient"
    /// ```
    query: String,
    /// A query for sorting artstation search results.
    ///
    /// # Example
    ///```ignore
    ///     https://www.artstation.com/search?query=ancient&sort_by=rank
    ///                                                    <--sorting-->
    /// sorting == "rank"
    /// ```
    sorting: String,
    /// Number of pages to extract projects from Artstation.
    depth: i64,
}

impl ArtstationSearchExtractor {
    /// Construct a new [`ArtstationSearchExtractor`].
    pub fn new(
        url: &str,
        depth: i64,
        filename_format: &String,
    ) -> Result<ArtstationSearchExtractor, Error> {
        let url_regex_pattern: Regex =
            Regex::new(r"(?:https?://)?(?:\w+\.)?artstation\.com/search/?\?([^#]+)")
                .expect("Could not compile regex");

        // Check if the URL is valid.
        let _captures: Captures = url_regex_pattern
            .captures(url)?
            .ok_or(Error::SubExtractorNotFound)?;

        let queries: Vec<String> = text::parse_url_query_to_vec(url);

        let query_index = match queries.iter().position(|query| query.contains("query")) {
            Some(query) => query + 1,
            None => return Err(Error::SubExtractorNotFound),
        };
        let query: String = queries[query_index].clone();

        let sorting_index = match queries.iter().position(|query| query.contains("sort_by")) {
            Some(query) => query + 1,
            None => return Err(Error::SubExtractorNotFound),
        };
        let sorting: String = queries[sorting_index].clone();

        Ok(ArtstationSearchExtractor {
            common: Common::new(filename_format.to_string()),
            query,
            sorting,
            depth,
        })
    }
}

#[async_trait]
impl Extractor for ArtstationSearchExtractor {
    fn items(&self, metadata: Vec<Value>) -> Vec<(Message, Vec<Value>)> {
        self.common.items(metadata)
    }

    fn get_file_format(&self) -> &String {
        &self.common.filename_format
    }

    fn get_category(&self) -> ExtractorKind {
        ExtractorKind::Artstation
    }

    fn get_subextractor(&self) -> SubExtractorKind {
        SubExtractorKind::ArtstationSearch
    }

    async fn get_metadata(&self, client: Client) -> Vec<Value> {
        let metadata: Arc<Mutex<Vec<Value>>> = Arc::new(Mutex::new(Vec::new()));
        let mut futures: Vec<_> = Vec::new();

        for page_number in 1..self.depth {
            let search_metadata_url: String = format!(
                "{}/api/v2/search/projects.json?page={}&per_page=50&pro_first=1&query={}&\
                 sorting={}",
                self.common.root, page_number, self.query, self.sorting
            );

            let response: Response =
                Common::get_request(&client, search_metadata_url.as_str()).await;

            let search_metadata: Value = response
                .json()
                .await
                .expect("Failed deserializing search metadata");

            let data: Vec<Value> = match search_metadata["data"].as_array() {
                Some(data) => data.clone(),
                None => break,
            };

            for project in data.into_iter() {
                let client_cloned: Client = client.clone();
                let root: String = self.common.root.clone();
                let metadata_cloned: Arc<Mutex<Vec<Value>>> = Arc::clone(&metadata);
                let query: String = self.query.clone();

                futures.push(tokio::spawn(async move {
                    let hash_id: &str = project["hash_id"]
                        .as_str()
                        .expect("Could not get project hash ID");

                    let project_metadata_url = format!("{}/projects/{}.json", root, hash_id);

                    let response: Response =
                        Common::get_request(&client_cloned, project_metadata_url.as_str()).await;

                    let mut json: Value = response
                        .json()
                        .await
                        .expect("Failed deserializing project assets");

                    json["query"] = Value::String(query);
                    metadata_cloned.lock().await.push(json);
                }));
            }
        }
        future::join_all(futures).await;
        info!("Deserialized search query {}", self.query);

        #[allow(clippy::unwrap_used)]
        let metadata: Vec<Value> = Arc::try_unwrap(metadata).unwrap().into_inner();
        metadata
    }
}
