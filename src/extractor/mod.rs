/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Traits, types, and functions use for extractors.

use std::fmt::{Display, Formatter};
use std::sync::Arc;

use async_trait::async_trait;
use fancy_regex::Regex;
use log::info;
use reqwest::Client;
use serde_json::Value;

use artstation::album::ArtstationAlbumExtractor;
use artstation::artwork::ArtstationArtworkExtractor;
use artstation::challenge::ArtstationChallengeExtractor;
use artstation::following::ArtstationFollowingExtractor;
use artstation::image::ArtstationImageExtractor;
use artstation::likes::ArtstationLikesExtractor;
use artstation::search::ArtstationSearchExtractor;
use artstation::user::ArtstationUserExtractor;
use message::Message;
use reddit::image::RedditImageExtractor;
use reddit::submission::RedditSubmissionExtractor;
use reddit::subreddit::RedditSubredditExtractor;
use reddit::user::RedditUserExtractor;
use webtoons::comic::WebtoonsComicExtractor;
use webtoons::episode::WebtoonsEpisodeExtractor;

use crate::cache::Cache;
use crate::config::extractor::{ArtstationExtractors, Imgur, Reddit, Webtoons};
use crate::config::ui_config::UIConfig;
use crate::extractor::imgur::image::ImgurImageExtractor;
use crate::extractor::reddit::home::RedditHomeExtractor;

pub mod artstation;
pub mod imgur;
pub mod message;
pub mod reddit;
pub mod webtoons;

/// Types of extractors.
#[derive(PartialEq, Eq, Debug, Clone)]
#[non_exhaustive]
pub enum ExtractorKind {
    /// Artstation Extractor.
    Artstation,
    /// Imgur Extractor.
    Imgur,

    /// Reddit Extractor.
    Reddit,

    /// Webtoons Extractor.
    Webtoons,
}

/// Types of sub-extractors.
#[derive(PartialEq, Eq, Debug, Clone)]
#[allow(clippy::enum_variant_names)]
#[non_exhaustive]
pub enum SubExtractorKind {
    /// [`ArtstationImageExtractor`].
    ArtstationImage,

    /// [`ArtstationUserExtractor`].
    ArtstationUser,

    /// [`ArtstationLikesExtractor`].
    ArtstationLikes,

    /// [`ArtstationAlbumExtractor`].
    ArtstationAlbum,

    /// [`ArtstationArtworkExtractor`].
    ArtstationArtwork,

    /// [`ArtstationFollowingExtractor`].
    ArtstationFollowing,

    /// [`ArtstationChallengeExtractor`].
    ArtstationChallenge,

    /// [`ArtstationSearchExtractor`].
    ArtstationSearch,

    /// [`ImgurImageExtractor`].
    ImgurImage,

    /// [`RedditHomeExtractor`].
    RedditHome,

    /// [`RedditImageExtractor`].
    RedditImage,

    /// [`RedditSubmissionExtractor`]
    RedditSubmission,

    /// [`RedditSubredditExtractor`].
    RedditSubreddit,

    /// [`RedditUserExtractor`]
    RedditUser,

    /// [`WebtoonsEpisodeExtractor`].
    WebtoonsEpisode,

    /// [`WebtoonsComicExtractor`].
    WebtoonsComic,
}

/// Errors that can occur when finding an extractor.
#[derive(Debug)]
#[non_exhaustive]
pub enum Error {
    /// Could not detect website from URL.
    WebsiteNotFound,

    /// Could not find a suitable sub-extractor for URL.
    SubExtractorNotFound,
}

/// An Extractor trait for each extractor to implement.
#[async_trait]
pub trait Extractor: Sync + Send {
    /// Extract the image URL and the metadata associated with it.
    fn items(&self, metadata: Vec<Value>) -> Vec<(Message, Vec<Value>)>;

    /// Returns the filename format.
    fn get_file_format(&self) -> &String;

    /// Returns the type of extractor.
    fn get_category(&self) -> ExtractorKind;

    /// Returns the type of the sub-extractor.
    fn get_subextractor(&self) -> SubExtractorKind;

    /// Returns metadata for the given URL.
    async fn get_metadata(&self, client: Client) -> Vec<Value>;
}

impl Display for ExtractorKind {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            ExtractorKind::Artstation => write!(f, "Artstation"),
            ExtractorKind::Imgur => write!(f, "Imgur"),
            ExtractorKind::Reddit => write!(f, "Reddit"),
            ExtractorKind::Webtoons => write!(f, "Webtoons"),
        }
    }
}

impl Display for SubExtractorKind {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            SubExtractorKind::ArtstationImage => write!(f, "ArtstationImageExtractor"),
            SubExtractorKind::ArtstationUser => write!(f, "ArtstationUserExtractor"),
            SubExtractorKind::ArtstationLikes => write!(f, "ArtstationLikesExtractor"),
            SubExtractorKind::ArtstationAlbum => write!(f, "ArtstationAlbumExtractor"),
            SubExtractorKind::ArtstationArtwork => write!(f, "ArtstationArtworkExtractor"),
            SubExtractorKind::ArtstationFollowing => write!(f, "ArtstationFollowingExtractor"),
            SubExtractorKind::ArtstationChallenge => write!(f, "ArtstationChallengeExtractor"),
            SubExtractorKind::ArtstationSearch => write!(f, "ArtstationSearchExtractor"),
            SubExtractorKind::ImgurImage => write!(f, "ImgurImageExtractor"),
            SubExtractorKind::RedditHome => write!(f, "RedditHomeExtractor"),
            SubExtractorKind::RedditImage => write!(f, "RedditImageExtractor"),
            SubExtractorKind::RedditSubmission => write!(f, "RedditSubmissionExtractor"),
            SubExtractorKind::RedditSubreddit => write!(f, "RedditSubredditExtractor"),
            SubExtractorKind::RedditUser => write!(f, "RedditUserExtractor"),
            SubExtractorKind::WebtoonsEpisode => write!(f, "WebtoonsEpisodeExtractor"),
            SubExtractorKind::WebtoonsComic => write!(f, "WebtoonsComicExtractor"),
        }
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::WebsiteNotFound => write!(f, "Unable to detect website from the given URL"),
            Error::SubExtractorNotFound => {
                write!(f, "No suitable sub-extractor found from the given URL")
            }
        }
    }
}

impl From<fancy_regex::Error> for Error {
    fn from(_: fancy_regex::Error) -> Self {
        Self::SubExtractorNotFound
    }
}

/// Find a suitable extractor for a given url
pub fn match_extractor(config: &UIConfig, url: &str) -> Result<Arc<dyn Extractor>, Error> {
    let extractor: ExtractorKind = get_extractor(url)?;

    info!("Extractor Found: {}", extractor);

    let sub_extractor: Arc<dyn Extractor> = get_sub_extractor(config, &extractor, url)?;

    info!("Sub-Extractor found: {}", sub_extractor.get_subextractor());

    Ok(sub_extractor)
}

/// Detects and returns the type of extractor for the given URL.
///
/// # Examples
/// ```
///  # use gallery_dlrs::extractor::get_extractor;
///  # use gallery_dlrs::extractor::ExtractorKind;
///
/// assert_eq!(
///     get_extractor("https://www.artstation.com/artwork/LQVJr").unwrap(),
///     ExtractorKind::Artstation
/// );
/// assert!(get_extractor("https://69420.com").is_err());
/// ```
pub fn get_extractor(url: &str) -> Result<ExtractorKind, Error> {
    let error_message_compile: &str = "Could not compile regex.";
    let error_message_match: &str = "Failed to match url with regex";

    // Artstation website.
    let re: Regex = Regex::new(r"(?:https?://)?(?:\w+\.)?(artstation\.com)|(artstn\.co/p)/(\w+)")
        .expect(error_message_compile);
    if re.is_match(url).expect(error_message_match) {
        return Ok::<ExtractorKind, Error>(ExtractorKind::Artstation);
    }

    // Reddit website.
    let re: Regex =
        Regex::new(r#"(?:https?://)?(?:i\.|www\.)?redd(?:\.it|ituploads\.com|it\.com)"#)
            .expect(error_message_compile);
    if re.is_match(url).expect(error_message_match) {
        return Ok(ExtractorKind::Reddit);
    }

    // Imgur website
    let re: Regex =
        Regex::new(r"(?:https?://)?(?:www\.|[im]\.)?imgur\.com").expect(error_message_compile);
    if re.is_match(url).expect(error_message_match) {
        return Ok(ExtractorKind::Imgur);
    }

    // Webtoons website.
    let re: Regex = Regex::new(r"(?:https?://)?(?:www\.)?webtoons\.com/([^/?#]+)")
        .expect(error_message_compile);
    if re.is_match(url).expect(error_message_match) {
        return Ok::<ExtractorKind, Error>(ExtractorKind::Webtoons);
    }

    // TODO: Add more websites to match against.
    Err(Error::WebsiteNotFound)
}

/// Prints each extractor, sub-extractor, and an example.
pub fn list_extractors() {
    let extractor_info: Vec<(&str, &str, &str, &str)> = vec![
        (
            "Artstation",
            "Extractor for images from a single artstation project",
            "Image",
            "https://www.artstation.com/artwork/LQVJr",
        ),
        (
            "Artstation",
            "Extractor for all projects of an artstation user",
            "User",
            "https://www.artstation.com/wlop",
        ),
        (
            "Artstation",
            "Extractor for liked projects of an artstation user",
            "Likes",
            "https://www.artstation.com/wlop/likes",
        ),
        (
            "Artstation",
            "Extractor for all projects in an artstation album",
            "Album",
            "https://www.artstation.com/huimeiye/albums/770899",
        ),
        (
            "Artstation",
            "Extractor for projects on artstation's artwork page",
            "Artwork",
            "https://www.artstation.com/artwork?sorting=latest",
        ),
        (
            "Artstation",
            "Extractor for a user's followed users",
            "Following",
            "https://www.artstation.com/wlop/following",
        ),
        (
            "Artstation",
            "Extractor for submissions of artstation challenges",
            "Challenge",
            "https://www.artstation.com/contests/thu-2017/challenges/20",
        ),
        (
            "Artstation",
            "Extractor for artstation search results",
            "Search",
            "https://www.artstation.com/search?sort_by=relevance&query=arcane",
        ),
        (
            "Imgur",
            "Extractor for individual images on imgur.com",
            "Image",
            "https://imgur.com/21yMxCS",
        ),
        (
            "Reddit",
            "Extractor for reddit-hosted images",
            "Image",
            "https://i.redd.it/m23bwh4n0x151.png"
        ),
        (
            "Reddit",
            "Extractor for URLs in a reddit submission",
            "Submission",
            "https://www.reddit.com/r/wallpaper/comments/gtfkar/2560x1440_beautiful_sunset_with_a_rocket/"
        ),
        (
            "Reddit",
            "Extractor for URls from a reddit subreddit",
            "Subreddit",
            "https://www.reddit.com/r/dankmemes",
        ),
        (
            "Reddit",
            "Extractor for URLs from posts by a reddit user",
            "User",
            "https://www.reddit.com/u/username/",
        ),
        (
            "Webtoons",
            "Extractor for an entire comic on webtoons.com",
            "Comic",
            "https://www.webtoons.com/en/comedy/live-with-yourself/list?title_no=919",
        ),
        (
            "Webtoons",
            "Extractor for an episode on webtoons.com",
            "Episode",
            "https://www.webtoons.com/en/comedy/safely-endangered/ep-572-earth/viewer?title_no=352&episode_no=572"
        ),
    ];

    for (extractor, summary, sub_extractor, example) in extractor_info {
        println!("Category: {} - Subcategory: {}", extractor, sub_extractor);
        println!("{}", summary);
        println!("Example: {}\n", example);
    }
}

/// Prints each extractor module.
pub fn list_modules() {
    let modules: Vec<&str> = vec!["artstation", "imgur", "reddit", "webtoons"];
    for module in modules {
        println!("{}", module);
    }
}

/// Returns a suitable sub-extractor for the given Url.
fn get_sub_extractor(
    config: &UIConfig,
    extractor: &ExtractorKind,
    url: &str,
) -> Result<Arc<dyn Extractor>, Error> {
    match extractor {
        ExtractorKind::Artstation => {
            // + 1 is added since the page number starts at 1 instead of 0.
            let depth: i64 = config.extractor.artstation.depth + 1;

            let config: &ArtstationExtractors = &config.extractor.artstation.sub_extractors;

            if let Ok(sub_extractor) =
                ArtstationUserExtractor::new(url, depth, &config.user.filename_format)
            {
                Ok(Arc::new(sub_extractor))
            } else if let Ok(sub_extractor) =
                ArtstationImageExtractor::new(url, &config.image.filename_format)
            {
                Ok(Arc::new(sub_extractor))
            } else if let Ok(sub_extractor) =
                ArtstationLikesExtractor::new(url, depth, &config.likes.filename_format)
            {
                Ok(Arc::new(sub_extractor))
            } else if let Ok(sub_extractor) =
                ArtstationAlbumExtractor::new(url, &config.album.filename_format)
            {
                Ok(Arc::new(sub_extractor))
            } else if let Ok(sub_extractor) =
                ArtstationArtworkExtractor::new(url, depth, &config.artwork.filename_format)
            {
                Ok(Arc::new(sub_extractor))
            } else if let Ok(sub_extractor) =
                ArtstationChallengeExtractor::new(url, depth, &config.challenge.filename_format)
            {
                Ok(Arc::new(sub_extractor))
            } else if let Ok(sub_extractor) =
                ArtstationFollowingExtractor::new(url, depth, &config.following.filename_format)
            {
                Ok(Arc::new(sub_extractor))
            } else if let Ok(sub_extractor) =
                ArtstationSearchExtractor::new(url, depth, &config.search.filename_format)
            {
                Ok(Arc::new(sub_extractor))
            } else {
                Err(Error::SubExtractorNotFound)
            }
        }
        ExtractorKind::Imgur => {
            let config: &Imgur = &config.extractor.imgur;

            if let Ok(sub_extractor) =
                ImgurImageExtractor::new(url, config, &config.sub_extractors.image.filename_format)
            {
                Ok(Arc::new(sub_extractor))
            } else {
                Err(Error::SubExtractorNotFound)
            }
        }
        ExtractorKind::Reddit => {
            let cache: Cache = Cache::new(config.cache.directory.clone().into_inner());
            let config: &Reddit = &config.extractor.reddit;

            if let Ok(sub_extractor) =
                RedditImageExtractor::new(url, &config.sub_extractors.image.filename_format)
            {
                Ok(Arc::new(sub_extractor))
            } else if let Ok(sub_extractor) = RedditSubmissionExtractor::new(
                url,
                config,
                &cache,
                &config.sub_extractors.submission.filename_format,
            ) {
                Ok(Arc::new(sub_extractor))
            } else if let Ok(sub_extractor) = RedditSubredditExtractor::new(
                url,
                config,
                &cache,
                &config.sub_extractors.subreddit.filename_format,
            ) {
                Ok(Arc::new(sub_extractor))
            } else if let Ok(sub_extractor) = RedditHomeExtractor::new(
                url,
                config,
                &cache,
                &config.sub_extractors.home.filename_format,
            ) {
                Ok(Arc::new(sub_extractor))
            } else if let Ok(sub_extractor) = RedditUserExtractor::new(
                url,
                config,
                &cache,
                &config.sub_extractors.user.filename_format,
            ) {
                Ok(Arc::new(sub_extractor))
            } else {
                Err(Error::SubExtractorNotFound)
            }
        }
        ExtractorKind::Webtoons => {
            let config: &Webtoons = &config.extractor.webtoons;

            // + 1 is added since the page number starts at 1 instead of 0.
            let depth: i64 = config.depth + 1;

            if let Ok(sub_extractor) =
                WebtoonsEpisodeExtractor::new(url, &config.sub_extractors.episode.filename_format)
            {
                Ok(Arc::new(sub_extractor))
            } else if let Ok(sub_extractor) = WebtoonsComicExtractor::new(
                url,
                depth,
                &config.sub_extractors.comic.filename_format,
            ) {
                Ok(Arc::new(sub_extractor))
            } else {
                Err(Error::SubExtractorNotFound)
            }
        }
    }
}
