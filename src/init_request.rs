/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Initialize `reqwest::Client`.

use std::str::FromStr;
use std::sync::Arc;
use std::time::Duration;

use log::{trace, warn};
use num::ToPrimitive;
use reqwest::cookie::Jar;
use reqwest::header::HeaderMap;
use reqwest::{Client, ClientBuilder, Proxy};
use url::Url;

use crate::config::downloader::Header;
use crate::config::extractor::Cookie;
use crate::config::ui_config::UIConfig;
use crate::extractor::ExtractorKind;

/// Initialize [`reqwest::Client`] for extractors.
pub fn initialize_reqwest_extractor(
    config: &UIConfig,
    url: &str,
    extractor_kind: &ExtractorKind,
) -> Client {
    trace!("Creating extractor reqwest client");

    let mut client: ClientBuilder = Client::builder();

    client = set_timeout(config.extractor.timeout, client);

    let mut jar: Jar = Jar::default();

    if extractor_kind == &ExtractorKind::Webtoons {
        // Http2 with webtoons is unreliable (lots of connection timeouts).
        client = client.http1_only();

        jar = build_cookies_webtoons(url);
    }

    if let Some(proxy) = &config.extractor.proxy {
        let proxy: Proxy = proxy.clone().into_inner();
        client = client.proxy(proxy);
    }

    if let Some(cookies) = &config.extractor.cookies {
        for cookie in cookies.iter() {
            jar = set_cookie(cookie, url, jar);
        }
    }

    let user_agent: String = config.extractor.user_agent.clone();
    let headers: HeaderMap = build_http_headers(user_agent.as_str(), None);

    let client: Client = client
        .default_headers(headers)
        .cookie_store(true)
        .cookie_provider(Arc::new(jar))
        .use_rustls_tls()
        .build()
        .expect("Failed to build reqwest client");

    client
}

/// Initialize [`reqwest::Client`] for downloaders.
#[allow(clippy::unwrap_used)]
pub fn initialize_reqwest_downloader(
    config: &UIConfig,
    url: &str,
    extractor_kind: &ExtractorKind,
) -> Client {
    trace!("Creating downloader reqwest client");

    let mut client: ClientBuilder = Client::builder();

    client = set_timeout(config.downloader.timeout, client);

    let user_agent: String = config.extractor.user_agent.clone();
    let headers: HeaderMap = build_http_headers(
        user_agent.as_str(),
        Some(config.downloader.http.headers.clone()),
    );

    let mut jar: Jar = Jar::default();

    if extractor_kind == &ExtractorKind::Webtoons {
        // Http2 with webtoons is unreliable (lots of connection timeouts).
        client = client.http1_only();

        jar = build_cookies_webtoons(url);
    }

    let client: Client = client
        .default_headers(headers)
        .cookie_store(true)
        .cookie_provider(Arc::new(jar))
        .use_rustls_tls()
        .build()
        .expect("Failed to build reqwest client");

    client
}

/// Build custom cookies for the website [Webtoons](https://webtoons.com).
fn build_cookies_webtoons(url: &str) -> Jar {
    trace!("Creating webtoons cookies");

    let jar: Jar = Jar::default();
    #[allow(clippy::unwrap_used)]
    let url: Url = url.parse::<Url>().unwrap();

    // Bypass age gate.
    jar.add_cookie_str("atGDRP=AD_CONSENT", &url);
    jar.add_cookie_str("needCCPA=false", &url);
    jar.add_cookie_str("needCOPPA=false", &url);
    jar.add_cookie_str("needGDPR=false", &url);
    jar.add_cookie_str("pagGDPR=true", &url);
    jar.add_cookie_str("ageGatePass=true", &url);

    jar
}

/// Create http headers.
#[allow(clippy::unwrap_used)]
fn build_http_headers(user_agent: &str, user_headers: Option<Vec<Header>>) -> HeaderMap {
    trace!("Building HTTP headers");

    let headers: HeaderMap = {
        let mut h: HeaderMap = HeaderMap::new();

        h.insert(reqwest::header::USER_AGENT, user_agent.parse().unwrap());

        h.insert(
            reqwest::header::ACCEPT,
            "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8"
                .parse()
                .unwrap(),
        );

        h.insert(
            reqwest::header::ACCEPT_LANGUAGE,
            "en-US,en;q=0.5".parse().unwrap(),
        );

        // Custom user defined headers.
        if let Some(user_headers) = user_headers {
            for header in user_headers.into_iter() {
                if let Some((name, value)) = (header.name()).zip(header.value()) {
                    h.insert(name.clone(), value.clone());
                }
            }
        }

        h
    };

    headers
}

/// Add a cookie to `jar`.
fn set_cookie(cookie: &Cookie, url: &str, jar: Jar) -> Jar {
    let cookie: String = format!("{}={}", cookie.name, cookie.value);
    let url: Url = match Url::from_str(url) {
        Ok(url) => url,
        Err(error) => {
            warn!("Cannot use cookie \"{}\": {}", cookie, error);
            return jar;
        }
    };

    jar.add_cookie_str(cookie.as_str(), &url);

    trace!("Cookie set: {}", cookie);

    jar
}

/// Set timeout from `config`.
fn set_timeout(timeout: f64, client: ClientBuilder) -> ClientBuilder {
    // Convert to milliseconds for `std::time::Duration`.
    let milliseconds: u64 = match (timeout * 1000.0).to_u64() {
        Some(milliseconds) => milliseconds,
        None => {
            warn!("Could not set timeout {}.", timeout);
            return client;
        }
    };

    let client: ClientBuilder = client.timeout(Duration::from_millis(milliseconds));

    client
}
