/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Execute Python code from Rust.

use pyo3::prelude::*;

/// Prints the available keywords for a URL with python.
pub fn handle_keyword_job(py: Python, metadata: PyObject) -> PyResult<()> {
    let py_job_keyword: &str = include_str!(concat!(
        env!("CARGO_MANIFEST_DIR"),
        "/src/python/job_keyword.py"
    ));

    let keyword_job: &PyModule =
        PyModule::from_code(py, py_job_keyword, "job_keyword", "job_keyword")?;

    keyword_job.getattr("print_kwdict")?.call1((metadata,))?;

    Ok(())
}

/// TODO: Add documentation.
pub fn create_formatter(format: &str) -> PyResult<PyObject> {
    let py_formatter = include_str!(concat!(
        env!("CARGO_MANIFEST_DIR"),
        "/src/python/formatter.py"
    ));
    Python::with_gil(|py| {
        let formatter: &PyModule = PyModule::from_code(py, py_formatter, "formatter", "formatter")?;
        let formatter: PyObject = formatter.getattr("parse")?.call1((format,))?.extract()?;
        Ok(formatter)
    })
}

/// TODO: Add documentation.
pub fn parse_metadata_with_formatter(
    py: Python,
    filename_formatter: &PyObject,
    metadata: PyObject,
) -> PyResult<String> {
    let py_formatter = include_str!(concat!(
        env!("CARGO_MANIFEST_DIR"),
        "/src/python/formatter.py"
    ));
    PyModule::from_code(py, py_formatter, "formatter", "formatter")?;
    let segment: String = filename_formatter
        .call_method1(py, "format_map", (metadata,))?
        .extract(py)?;
    Ok(segment)
}
