"""Print available keywords"""
import sys


def print_kwdict(kwdict, prefix="", markers=None):
    """Print key-value pairs in 'kwdict' with formatting"""
    write = sys.stdout.write
    suffix = "]" if prefix else ""

    markerid = id(kwdict)
    if markers is None:
        markers = {markerid}
    elif markerid in markers:
        write("{}\n  <circular reference>\n".format(prefix[:-1]))
        return  # ignore circular reference
    else:
        markers.add(markerid)

    for key, value in sorted(kwdict.items()):
        if key[0] == "_":
            continue
        key = prefix + key + suffix

        if isinstance(value, dict):
            print_kwdict(value, key + "[", markers)

        elif isinstance(value, list):
            if value and isinstance(value[0], dict):
                print_kwdict(value[0], key + "[][", markers)
            else:
                write(key + "[]\n")
                for val in value:
                    write("  - " + str(val) + "\n")

        else:
            # string or number
            write("{}\n  {}\n".format(key, value))
