/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Traits and methods responsible for downloading images/videos.

use std::fs::File;
use std::io;

use async_trait::async_trait;
use reqwest::{Client, Response};

use crate::config::ui_config::UIConfig;
use crate::downloader::http::HttpDownloader;
use crate::path::PathFormatter;

pub mod http;
pub mod ytdl;

/// A downloader trait for each downloader to implement.
#[async_trait]
pub trait Downloader: Sync + Send {
    /// Write data from `url` into the file specified by [`PathFormatter`].
    async fn download(
        &self,
        url: &str,
        file_path: String,
        path_formatter: &mut PathFormatter,
    ) -> DownloadStatus;

    /// Returns which downloader being used.
    fn kind(&self) -> DownloaderKind;
}

#[derive(PartialEq)]
/// States the status of downloading the URL.
pub enum DownloadStatus {
    /// URL downloaded successfully.
    Success,

    /// URL failed to download.
    Failed,

    /// Downloading the URL timed out.
    TimedOut,

    /// Response status code is `403`.
    Forbidden,

    /// File being saved to disk already exists.
    FileAlreadyExists,
}

#[derive(Copy, Clone, Eq, PartialEq)]
pub enum DownloaderKind {
    /// HTTP downloader.
    Http,

    /// Youtube downloader.
    Ytdp,
}
