//! Downloader for URLs requiring youtube-dl support.

use std::io;
use std::io::Write;
use std::process::{Command, Output};

use async_trait::async_trait;
use log::info;

use crate::downloader::{DownloadStatus, Downloader, DownloaderKind};
use crate::path::PathFormatter;

pub struct YoutubeDL {}

impl YoutubeDL {
    /// Creates a new [`YoutubeDl`]
    #[allow(clippy::new_ret_no_self)]
    pub fn new() -> Box<dyn Downloader> {
        Box::new(Self {})
    }
}

#[async_trait]
impl Downloader for YoutubeDL {
    async fn download(
        &self,
        url: &str,
        _file_path: String,
        path_formatter: &mut PathFormatter,
    ) -> DownloadStatus {
        let ytdl: Output = Command::new("yt-dlp")
            .args([
                "-o",
                &path_formatter.filename,
                "-P",
                &path_formatter.directory_path,
                url,
            ])
            .output()
            .expect("Failed to launch yt-dlp");

        io::stdout()
            .write_all(&ytdl.stdout)
            .expect("Cannot write ytdl output");

        info!("ytdl: status: {}", ytdl.status);

        if ytdl.status.success() {
            DownloadStatus::Success
        } else {
            DownloadStatus::Failed
        }
    }

    fn kind(&self) -> DownloaderKind {
        DownloaderKind::Ytdp
    }
}
