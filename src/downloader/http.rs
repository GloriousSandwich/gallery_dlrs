/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Downloader for `http://` and `https://` URLs.

use std::collections::HashMap;
use std::fs::{File, Metadata, OpenOptions};
use std::io::{BufWriter, Write};
use std::os::linux::fs::MetadataExt;
use std::path::Path;
use std::{fs, io, thread, time};

use async_trait::async_trait;
use colored::*;
use log::{debug, error, info, warn};
use reqwest::header::{HeaderMap, HeaderValue, ACCEPT, RANGE};
use reqwest::{Client, Response};

use crate::config::de_wrappers::FilesizeWrapper;
use crate::config::ui_config::UIConfig;
use crate::downloader::{DownloadStatus, Downloader, DownloaderKind};
use crate::path::PathFormatter;

/// Downloader for `http://` and `https://` URLs.
#[derive(Builder)]
#[builder(name = "HttpBuilder")]
pub struct HttpDownloader {
    /// A client to make requests with.
    client: Client,
    /// Maximum number of retries during file downloads, or `-1` for
    /// for infinite retries.
    retries: i64,
    // Minimum allowed file size in bytes.
    ///
    /// Any file smaller than this limit will not be downloaded.
    /// Possible values are valid integer or floating-pint numbers
    /// optionally followed by one of [`k`, `m`, `g`, `t`, `p`].
    /// These suffixes are case insensitive.
    ///
    /// # Example
    /// ```ignore
    ///     "32000", "500k", "2.5M"
    /// ```
    #[builder(default, setter(strip_option))]
    filesize_min: i64,
    /// Maximum allowed file size in bytes.
    ///
    /// Any file larger than this limit will not be downloaded.
    /// Possible values are valid integer or floating-pint numbers
    /// optionally followed by one of [`k`, `m`, `g`, `t`, `p`].
    /// These suffixes are case insensitive.
    ///
    /// # Example
    /// ```ignore
    ///     "32000", "500k", "2.5M"
    /// ```
    #[builder(default, setter(strip_option))]
    filesize_max: Option<FilesizeWrapper>,
    /// Controls the use of `.part` files during file downloads.
    ///
    /// * `true` will write downloaded data into `.part` files and
    /// rename them upon download completion. This mode additionally
    /// supports resuming incomplete downloads.
    /// * `false` will not use `.part` files and write data directly
    /// into the actual output files.
    pub part: bool,
    /// Number of seconds to sleep before each download.
    pub sleep: i64,
}

impl HttpDownloader {
    /// Creates a new [`HttpDownloader`]
    #[allow(clippy::new_ret_no_self)]
    pub(crate) fn new(config: &UIConfig, client: Client) -> Box<dyn Downloader> {
        Box::new(Self {
            part: config.downloader.part,
            client,
            retries: config.downloader.retries,
            filesize_min: config.downloader.filesize_min.clone().into_inner(),
            filesize_max: config.downloader.filesize_max.clone(),
            sleep: config.downloader.sleep,
        })
    }

    /// Fetches the file extension from MIME type.
    fn find_extension(mime_type: &str) -> String {
        let mime_types: HashMap<&str, &str> = HashMap::from([
            ("image/bmp", "bmp"),
            ("image/x-bmp", "bmp"),
            ("image/x-ms-bmp", "bmp"),
            ("image/gif", "jif"),
            ("image/jpeg", "jpg"),
            ("image/jpg", "jpg"),
            ("image/ico", "ico"),
            ("image/icon", "ico"),
            ("image/vnd.microsoft.icon", "ico"),
            ("image/x-icon", "ico"),
            ("image/png", "png"),
            ("application/x-photoshop", "psd"),
            ("image/vnd.adobe.photoshop", "psd"),
            ("image/x-photoshop", "psd"),
            ("image/svg+xml", "svg"),
            ("image/webp", "webp"),
        ]);

        let mime_type: &str = mime_type.split(';').collect::<Vec<&str>>()[0];
        let mut mime_type: String = mime_type.to_owned();

        if !mime_type.contains('/') {
            mime_type = format!("image/{}", mime_type);
        }

        return match mime_types.get(mime_type.as_str()) {
            Some(mime_type) => mime_type.to_string(),
            None => {
                warn!("Unknown MIME type {}", mime_type);
                "bin".to_owned()
            }
        };
    }

    async fn save_file_to_disk(mut response: Response, file: File) -> io::Result<()> {
        let mut buffer: BufWriter<File> = BufWriter::new(file);
        while let Some(chunk) = match response.chunk().await {
            Ok(b) => b,
            Err(e) => {
                error!("Could not convert response to bytes.");
                return Err(io::Error::new(io::ErrorKind::Other, e));
            }
        } {
            buffer.write_all(&chunk)?;
            buffer.flush()?;
        }
        Ok(())
    }
}

#[async_trait]
impl Downloader for HttpDownloader {
    async fn download(
        &self,
        url: &str,
        file_path: String,
        path_formatter: &mut PathFormatter,
    ) -> DownloadStatus {
        let mut tries: i64 = 0;
        let message = "";
        let mut download_status: DownloadStatus = DownloadStatus::Failed;

        loop {
            if tries > 0 {
                warn!("{} ({}/{})", message, tries, self.retries);
            }
            if tries > self.retries {
                error!("Maximum retries reached, skipping url...");
                return download_status;
            }
            tries += 1;
            thread::sleep(time::Duration::from_secs(self.sleep as u64));

            let mut headers: HeaderMap = HeaderMap::new();
            #[allow(clippy::unwrap_used)]
            headers.insert(ACCEPT, "*/*".parse().unwrap());
            if self.part {
                let part_file: String = format!("{}.part", &file_path);
                let part_file: &Path = Path::new(part_file.as_str());

                if part_file.exists() {
                    let metadata: Metadata = match fs::metadata(part_file) {
                        Ok(metadata) => metadata,
                        Err(error) => {
                            warn!(
                                "Could not get metadata of part file {}, (reason: {})",
                                part_file.display(),
                                error
                            );
                            warn!("Deleting part file...");
                            fs::remove_file(part_file).expect("Could not remove part file");
                            continue;
                        }
                    };

                    let file_size: u64 = metadata.st_size();
                    #[allow(clippy::unwrap_used)]
                    headers.insert(RANGE, format!("bytes={}-", file_size + 1).parse().unwrap());
                }
            }

            let response = self.client.get(url).headers(headers).send().await;
            let response = match response {
                Ok(r) => r,
                Err(e) => {
                    warn!(
                        "Could not connect to url `{}`, retrying... (reason: {})",
                        url, e
                    );
                    download_status = DownloadStatus::TimedOut;
                    continue;
                }
            };

            // Check response.
            if response.status().is_server_error() {
                warn!("Status code is Server error, skipping url...");
                download_status = DownloadStatus::Failed;
                return download_status;
            } else if response.status().as_u16() == 403 {
                download_status = DownloadStatus::Forbidden;
                return download_status;
            } else if response.status().is_client_error() {
                warn!("Status code is Client error, retrying...");
                continue;
            }

            if !path_formatter.has_extension {
                let mime_type: &HeaderValue = match response.headers().get("Content-Type") {
                    Some(h) => h,
                    None => {
                        error!("Could not fetch header `Content-Type`, retrying...");
                        continue;
                    }
                };

                let mime_type: String = match mime_type.to_str() {
                    Ok(mime_type) => mime_type.to_owned(),
                    Err(error) => {
                        warn!(
                            "Could not convert MIME Type to string, skipping... (reason: {})",
                            error
                        );
                        download_status = DownloadStatus::Failed;
                        return download_status;
                    }
                };

                let extension: String = HttpDownloader::find_extension(&mime_type);
                path_formatter.extension = extension;
            }

            let response_size = match response.headers().get("Content-Length") {
                Some(h) => h,
                None => {
                    error!("Could not fetch header `Content-Length`, retrying...");
                    continue;
                }
            };

            // Checks file size
            if !response_size.is_empty() {
                #[allow(clippy::unwrap_used)]
                let response_size: i64 = response_size.to_str().unwrap().parse::<i64>().unwrap();

                if response_size < self.filesize_min {
                    error!(
                        "File size smaller than allowed minimum ({} bytes < {} bytes)",
                        response_size, self.filesize_min
                    );

                    download_status = DownloadStatus::Failed;

                    return download_status;
                }

                if let Some(filesize_max) = &self.filesize_max {
                    #[allow(clippy::unwrap_used)]
                    let filesize_max: i64 = filesize_max.clone().into_inner();

                    if response_size > filesize_max {
                        error!(
                            "File size bigger than allowed maximum ({} bytes > {} bytes)",
                            response_size, filesize_max
                        );

                        download_status = DownloadStatus::Failed;

                        return download_status;
                    }
                }
            }

            let file: File = if response.status().as_u16() == 206 {
                match OpenOptions::new().append(true).open(&file_path) {
                    Ok(file) => {
                        debug!("Resuming partial download");
                        file
                    }
                    Err(error) => {
                        warn!(
                            "Could not resume partial download for file {} (reason: {})",
                            file_path, error
                        );
                        download_status = DownloadStatus::Failed;
                        return download_status;
                    }
                }
            } else {
                match OpenOptions::new().write(true).create(true).open(&file_path) {
                    Ok(file) => file,
                    Err(error) => {
                        warn!("Could not open file {} (reason: {})", file_path, error);
                        download_status = DownloadStatus::Failed;
                        return download_status;
                    }
                }
            };

            match HttpDownloader::save_file_to_disk(response, file).await {
                Ok(_) => {
                    info!("Successfully downloaded file `{}`", file_path.green());
                    download_status = DownloadStatus::Success;
                    return download_status;
                }
                Err(e) => {
                    error!("Could not save file to disk, retrying... (reason: {})", e);
                    continue;
                }
            };
        }
    }

    fn kind(&self) -> DownloaderKind {
        DownloaderKind::Http
    }
}
