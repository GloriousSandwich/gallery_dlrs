/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Types and functions to handle various command-line tasks.

use std::borrow::Cow;

use clap::{CommandFactory, Parser};
use log::LevelFilter;
use serde_yaml::Value;

use crate::config::de_wrappers::PathbufWrapper;
use crate::config::serde_utils;
use crate::config::ui_config::UIConfig;

/// Responsible for parsing command-line arguments.
//
// Note that as the project grows more arguments will
// be added to the struct.
#[derive(Parser, Clone, Debug)]
#[clap(name = "Gallery_dlrs")]
#[clap(author = "GloriousSandwich <gitlab434123@protonmail.com>")]
#[clap(version = env ! ("CARGO_PKG_VERSION"))]
#[clap(
    about = "A command-line program that downloads images, galleries, and collection from several \
             hosting sites."
)]
#[clap(
group(
clap::ArgGroup::new("URL CONFLICTS")
.required(true)
.args(& ["url", "list-extractors", "list-modules"]),
))]
#[non_exhaustive]
pub struct CliArgs {
    /// Website URL.
    pub url: Option<String>,
    // General Options ----------------------------------------------
    /// Target location for file downloads.
    #[clap(short, long, value_name = "PATH")]
    pub destination: Option<String>,

    /// Do not skip downloads; overwrite existing files.
    #[clap(long)]
    pub no_skip: bool,

    /// Number of worker threads used for extracting images/videos.
    #[clap(long, value_name = "NTHREADS")]
    pub threads: Option<i64>,

    /// User authentication via oauth.
    #[clap(long, value_name = "WEBSITE_NAME")]
    pub oauth: Option<String>,

    // Output Options -----------------------------------------------
    /// Reduce the level of verbosity (the min level is -qq).
    #[clap(short, long, conflicts_with("verbose"), parse(from_occurrences))]
    pub quiet: u8,

    ///Print various debugging information (the max level is -vvv)
    #[clap(short, long, conflicts_with("quiet"), parse(from_occurrences))]
    pub verbose: u8,

    /// Print URLs instead of downloading.
    #[clap(short, long)]
    pub get_urls: bool,

    /// Prints a list of available extractor modules.
    #[clap(long)]
    pub list_modules: bool,

    /// Prints a list of available extractor classes with description,
    /// subcategory and example URL.
    #[clap(long)]
    pub list_extractors: bool,

    /// Prints a list of available keywords and example values for the given
    /// URL.
    #[clap(long)]
    pub list_keywords: bool,

    /// Write logging output to FILE.
    #[clap(long, value_name = "FILE")]
    pub write_log: Option<String>,

    // Configuration Options ----------------------------------------
    /// Load the default configuration file.
    #[clap(long)]
    pub ignore_config: bool,

    /// Override configuration file options [example:
    /// extractor.base_directory=$HOME/Pictures]
    #[clap(short = 'o', long, multiple_values = true)]
    pub option: Vec<String>,

    /// CLI options for config overrides.
    #[clap(skip)]
    pub config_options: Value,
}

impl CliArgs {
    /// Creates a new [`CliArgs`]
    pub fn new() -> Self {
        let mut options: CliArgs = CliArgs::parse();

        // Convert `--option` flags info serde `Value`.
        for option in options.option.iter() {
            match option_as_value(option) {
                Ok(value) => {
                    options.config_options = serde_utils::merge(options.config_options, value);
                }
                Err(_) => eprintln!("Invalid CLI config option: {:?}", option),
            }
        }

        options
    }

    /// Override configuration file with options from CLI.
    pub fn override_config(&self, config: &mut UIConfig) {
        if let Some(base_directory) = self.destination() {
            config.extractor.base_directory = base_directory;
        }

        if let Some(threads) = self.threads {
            config.extractor.threads = threads;
        }

        if let Some(log_path) = self.write_log.clone() {
            config.logging.log_file.path = PathbufWrapper::from(log_path);
        }

        if self.no_skip {
            config.extractor.skip = false;
        }

        let log_level: LevelFilter = self.log_level();
        config.logging.log_level = std::cmp::max(config.logging.log_level, log_level);
    }

    /// Returns logging level filter.
    pub fn log_level(&self) -> LevelFilter {
        match (self.quiet, self.verbose) {
            // Default.
            (0, 0) => LevelFilter::Info,

            // Verbose.
            (_, 1) => LevelFilter::Info,
            (_, 2) => LevelFilter::Debug,
            (0, _) => LevelFilter::Trace,

            // Quiet.
            (1, _) => LevelFilter::Error,
            (..) => LevelFilter::Off,
        }
    }

    /// Returns the destination.
    pub fn destination(&self) -> Option<PathbufWrapper> {
        match self.destination.as_ref() {
            Some(destination) => shellexpand::full(destination).ok().map(|mut destination| {
                if !destination.ends_with('/') {
                    destination = Cow::from(format!("{}/", destination));
                }

                PathbufWrapper::from(destination.to_string())
            }),
            None => None,
        }
    }
}

impl Default for CliArgs {
    fn default() -> Self {
        Self::new()
    }
}

/// Format an option in the format of `parent.field=value` to a serde Value.
fn option_as_value(option: &str) -> Result<Value, serde_yaml::Error> {
    let mut yaml_text = String::with_capacity(option.len());
    let mut closing_brackets = String::new();

    for (i, c) in option.chars().enumerate() {
        match c {
            '=' => {
                yaml_text.push_str(": ");
                yaml_text.push_str(&option[i + 1..]);
                break;
            }
            '.' => {
                yaml_text.push_str(": {");
                closing_brackets.push('}');
            }
            _ => yaml_text.push(c),
        }
    }

    yaml_text += &closing_brackets;

    serde_yaml::from_str(&yaml_text)
}

/// Generates CLI app help section
pub fn build_parser() -> CliArgs {
    CliArgs::parse()
}

/// Generates a manpage
pub fn generate_manpage() -> std::io::Result<()> {
    let cmd = CliArgs::command();
    let home_env = std::env::var("HOME").expect("$HOME enviornment variable could not be found.");
    let manpage_dir = home_env + "/.local/share/man/man1";
    let manpage = clap_mangen::Man::new(cmd);
    let mut buffer: Vec<u8> = Default::default();
    manpage.render(&mut buffer)?;
    let manpage_dir = std::path::PathBuf::from(manpage_dir);
    std::fs::write(manpage_dir.join("gallery_dlrs.1"), buffer)?;
    Ok(())
}

// /// Apply args to config.
// pub async fn apply_args_to_config(args: &CliArgs, config: &Arc<Mutex<Cfg>>) {
//     let mut config: MutexGuard<Cfg> = config.lock().await;
//     if let Some(destination) = &args.destination {
//         let mut destination = shellexpand::full(destination)
//             .expect("Could not shellexpand --destination")
//             .as_ref()
//             .to_owned();
//
//         if !destination.ends_with('/') {
//             destination = format!("{}/", destination);
//         }
//
//         config.extractor.base_directory = destination;
//
//         debug!(
//             "Args: Base directory set: {}",
//             config.extractor.base_directory
//         );
//     }
//
//     if args.no_skip {
//         config.extractor.skip = false;
//         debug!("Args: Skip set: {}", config.extractor.skip);
//     }
//
//     if let Some(threads) = args.threads {
//         config.extractor.threads = threads;
//         debug!("Args: Threads set: {}", config.extractor.threads);
//     }
// }
