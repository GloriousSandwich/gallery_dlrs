/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Collection of functions that handle strings/text

use std::collections::HashMap;

use url::Url;
use urlparse::unquote;

///Extract the filename and extension from the end of a URL.
///
/// # Examples
/// ```
/// # use crate::gallery_dlrs::text::extract_name_from_url;
/// let (filename, extension) = extract_name_from_url("https://cdna.artstation.com/p/assets/images/images/048/734/418/large/wlop-cover-se.jpg?1650810731");
/// assert_eq!("wlop-cover-se", filename);
/// assert_eq!("jpg", extension);
/// ```
pub fn extract_name_from_url(url: &str) -> (String, String) {
    let url_parts: (&str, &str) = get_filename_from_url(url);
    let filename: &str = url_parts.1;
    let filename: String = unquote(filename)
        .expect("Could not replace %xx escapes with their single-character equivalent.");
    let filename_parts: Vec<&str> = filename.split('.').collect();

    let name: &str = filename_parts[0];

    // A URL is not guaranteed to have a file extension.
    let extension: String = match filename_parts.get(1) {
        Some(ext) => ext.to_string(),
        None => String::new(),
    };

    if name.is_ascii() && extension.len() <= 16 {
        (name.to_owned(), extension.to_ascii_lowercase())
    } else {
        (filename, extension)
    }
}

/// Extract the part of a URL to use as a filename.
fn get_filename_from_url(url: &str) -> (&str, &str) {
    url.split('?').collect::<Vec<&str>>()[0]
        .rsplit_once('/')
        .expect("Could not rsplit URL.")
}

/// Convert a bytes-amount ("500k", "2.5M", ...) to bytes.
///
/// # Examples
/// ```
/// # use crate::gallery_dlrs::text::parse_bytes;
///
/// let bytes = parse_bytes("2.5M").unwrap();
/// assert_eq!(2621440, bytes);
///
/// let bytes = parse_bytes("500k").unwrap();
/// assert_eq!(512000, bytes);
/// ```
///
/// # Panics
///
/// This function panics if `value` cannot be converted into an `f64`.
pub fn parse_bytes(value: &str) -> Option<i64> {
    let byte_type: char = match value.chars().nth_back(0) {
        Some(c) => c.to_ascii_lowercase(),
        None => {
            return None;
        }
    };
    let byte_types = ['b', 'k', 'm', 'g', 't', 'p'];
    let mut mul: f64 = 1.0;
    if byte_types.contains(&byte_type) {
        let index: u32 = match byte_types.iter().position(|c| c == &byte_type) {
            Some(x) => x as u32,
            None => {
                return None;
            }
        };
        //mul = f64::from(1024_i64.pow(index as u32));
        mul = (1024_i64.checked_pow(index).unwrap_or(1)) as f64;
    }
    // Remove the byte type, leaving only the number.
    let value: String = value.to_ascii_lowercase().replace(byte_types, "");
    #[allow(clippy::unwrap_used)]
    Some((value.parse::<f64>().unwrap() * mul).round() as i64)
}

/// Extract the text between `begin` and `end` from `text`.
///
/// # Examples
/// ```
/// # use crate::gallery_dlrs::text::extract;
///
/// assert_eq!(extract("abcde", "b", "d"), Some("c".to_owned()));
/// assert_eq!(extract("abcde", "e", "g"), None);
pub fn extract(text: &str, begin: &str, end: &str) -> Option<String> {
    let first: usize = text.find(begin).map(|s| s + begin.len())?;
    let text: String = text[first..].to_string();
    let end: usize = text.find(end)?;
    let text: String = text[0..end].to_string();
    Some(text)
}

/// Extract all text that starts with `begin`.
pub fn extract_all(text: &str, begin: &str, end: &str) -> Option<Vec<String>> {
    let mut texts: Vec<String> = Vec::new();
    let mut first: usize = text.find(begin).map(|s| s + begin.len())?;
    let mut text_first: String = text[first..].to_string();
    let mut last: usize = text_first.find(end)?;
    let text: String = text_first[0..last].to_string();
    texts.push(text);
    loop {
        let body: String = text_first[last..].to_string();
        first = match body.find(begin).map(|s| s + begin.len()) {
            Some(u) => u,
            None => break,
        };
        text_first = body[first..].to_string();
        last = match text_first.find(end) {
            Some(u) => u,
            None => break,
        };
        let text: String = text_first[0..last].to_string();
        texts.push(text.clone());
    }
    Some(texts)
}

/// Parse a url query string to key-value pairs into a hashmap.
pub fn parse_url_query_to_map(url: &str) -> HashMap<String, String> {
    let url: Url = Url::parse(url).expect("Cannot parse URL");
    let mut queries: HashMap<String, String> = HashMap::with_capacity(url.query_pairs().count());

    for (key, value) in url.query_pairs() {
        queries.insert(key.to_string(), value.to_string());
    }

    queries
}

/// Parse a url query string into key-value pairs.
///
/// # Example:
/// ```
/// # use crate::gallery_dlrs::text::parse_url_query_to_vec;
///
/// let url: &str = "https://www.artstation.com/search?q=ancient&sort_by=rank";
/// let queries: Vec<String> = parse_url_query_to_vec(url);
///
/// assert_eq!(queries[0], "q");
/// assert_eq!(queries[1], "ancient");
/// assert_eq!(queries[2], "sort_by");
/// assert_eq!(queries[3], "rank");
/// ```
pub fn parse_url_query_to_vec(url: &str) -> Vec<String> {
    let url = Url::parse(url).expect("Could not parse URL");
    let mut queries: Vec<String> = Vec::with_capacity(url.query_pairs().count() * 2);
    for query in url.query_pairs() {
        queries.push(query.0.to_string());
        queries.push(query.1.to_string());
    }
    queries
}
