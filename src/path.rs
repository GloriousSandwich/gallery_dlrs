/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Filesystem path handling.
use std::path::Path;
use std::process::exit;
use std::sync::Arc;
use std::{fs, io};

use log::{debug, error, trace};
use pyo3::{PyObject, PyResult, Python};
use serde_json::Value;

use crate::config::extractor::{
    ArtstationExtractors, ExtractorConfig, ImgurExtractors, RedditExtractors, WebtoonsExtractors,
};
use crate::config::ui_config::UIConfig;
use crate::extractor::{Extractor, ExtractorKind, SubExtractorKind};
use crate::python;

/// Responsible for creating and formatting files.
#[derive(Builder, Clone, Debug)]
pub struct PathFormatter {
    /// TODO: Add documentation.
    filename_formatter: PyObject,

    /// TODO: Add documentation.
    directory_formatter: PyObject,

    /// File path for the file being stored.
    pub file_path: String,

    /// File extension.
    pub extension: String,

    /// Does the file being downloaded have an extension?
    pub has_extension: bool,

    /// Directory for storing images for the given URL.
    #[builder(default)]
    pub directory_path: String,

    /// Is the directory ready to be created.
    #[builder(default)]
    pub create_directory: bool,
    /// Controls the use of `.part` files during file downloads.
    ///
    /// * `true` will write downloaded data into `.part` files and
    /// rename them upon download completion. This mode additionally
    /// supports resuming incomplete downloads.
    /// * `false` will not use `.part` files and write data directly
    /// into the actual output files.
    pub is_part_enable: bool,

    /// Is the file being saved a `.part` file?
    pub is_part_file: bool,

    /// Path for the `.part` file.
    pub part_file_path: String,

    /// Name of the file with no extension.
    pub filename: String,
}

/// Metadata to PyObject error message.
const METADATA_TO_PYOBJECT_ERROR: &str = "Cannot convert metadata to PyObject";

/// Error message when building the directory with the directory formatter.
const BUILD_DIRECTORY_ERROR: &str = "Cannot build directory with directory formatter";

impl PathFormatter {
    /// Initialize [`path::PathFormatter`],
    pub fn init(config: &UIConfig, extractor: &Arc<dyn Extractor>) -> PathFormatter {
        let mut builder = PathFormatterBuilder::default();
        let filename_formatter: PyObject = python::create_formatter(extractor.get_file_format())
            .unwrap_or_else(|error| {
                error!("Failed building file formatter: {:?}", error);
                exit(1);
            });

        let directory_format: String = Self::get_directory_format(
            &extractor.get_category(),
            &extractor.get_subextractor(),
            &config.extractor,
        );

        let directory_formatter: PyObject = python::create_formatter(directory_format.as_str())
            .unwrap_or_else(|error| {
                error!("Failed building directory formatter: {:?}", error);
                exit(1);
            });

        builder.filename_formatter(filename_formatter);
        builder.directory_formatter(directory_formatter);
        builder.file_path("".to_owned());
        builder.extension("".to_owned());
        builder.has_extension(false);
        builder.is_part_enable(config.downloader.part);
        builder.is_part_file(false);
        builder.part_file_path("".to_owned());
        builder.filename("".to_owned());

        let path_formatter: PathFormatter =
            builder.build().expect("Failed to build PathFormatter.");

        trace!("PathFormatter initialization complete");

        path_formatter
    }
    /// Build directory path.
    pub fn build_directory(&mut self, metadata: &Value, mut base_directory: String) {
        trace!("Building directory");

        Python::with_gil(|py| {
            let metadata: PyObject =
                pythonize::pythonize(py, &metadata).expect(METADATA_TO_PYOBJECT_ERROR);

            let directory: String =
                python::parse_metadata_with_formatter(py, &self.directory_formatter, metadata)
                    .expect(BUILD_DIRECTORY_ERROR);

            if !base_directory.ends_with('/') {
                base_directory.push('/');
            }

            // Build and return the directory path.
            if !directory.is_empty() {
                self.directory_path = format!("{}{}", base_directory, directory);
            } else {
                self.directory_path = base_directory.clone();
            }
        });

        self.create_directory = true;

        debug!("Built directory structure: {}", self.directory_path);
    }

    /// Creates the directory to store files.
    pub fn create_directory(&mut self) {
        trace!("Creating directory");

        if self.create_directory && !Path::new(&self.directory_path).is_dir() {
            fs::create_dir_all(&self.directory_path).unwrap_or_else(|error| {
                error!("Could not create directory. (reason {})", error);
                exit(1);
            });

            debug!("Directory {} created", self.directory_path);

            self.create_directory = false;
        }
    }

    /// Removes the `.part` extension.
    pub fn convert_part_file(&mut self, part_file_path: &str) -> io::Result<()> {
        if !self.has_extension {
            self.file_path = format!("{}{}", &self.filename, &self.extension);
        }

        fs::rename(part_file_path, &self.file_path)?;

        Ok(())
    }

    /// Prepare [`PathFormatter`] for downloads.
    pub fn prepare_for_downloads(&mut self, metadata: &Value) {
        debug!("Preparing PathFormatter for downloads");

        // Handles the edge case if the filename contains the extension.
        let extensions: [&str; 8] = ["bmp", "jif", "jpg", "ico", "png", "psd", "svg", "webp"];
        let mut has_extension: bool = false;
        for extension in extensions {
            if let Some(filename) = metadata["filename"].as_str() {
                if filename.ends_with(extension) {
                    self.extension = extension.to_owned();
                    has_extension = true;
                    break;
                }
            }
        }

        if !has_extension {
            self.extension = match metadata["extension"].as_str() {
                Some(extension) => extension.to_owned(),
                None => String::new(),
            };
        }

        self.has_extension = !self.extension.is_empty();

        // Build the filename.
        let temp: PyResult<String> = self.build_filename(metadata);
        let mut filename: String = temp.unwrap_or_else(|error| {
            error!("{:?}", error);
            exit(1);
        });
        filename = filename.replace('/', ".");

        if filename.ends_with('.') {
            filename.remove(filename.len() - 1);
        }

        self.filename = filename.clone();

        let mut full_path: String = if self.directory_path.ends_with('/') {
            format!("{}{}", self.directory_path, filename)
        } else {
            format!("{}/{}", self.directory_path, filename)
        };

        self.file_path = full_path.clone();

        if self.is_part_enable {
            full_path.push_str(".part");
            self.is_part_file = true;
            self.part_file_path = full_path;
        }

        debug!("Full file path set: {}", self.file_path);
    }

    /// Builds a filename for artstation URLs.
    fn build_filename(&self, metadata: &Value) -> PyResult<String> {
        trace!("Building filename");

        Python::with_gil(|py| {
            let metadata: PyObject =
                pythonize::pythonize(py, metadata).expect("Could not convert metadata to PyObject");

            let filename: PyResult<String> =
                python::parse_metadata_with_formatter(py, &self.filename_formatter, metadata);

            filename
        })
    }

    /// Returns the directory format used by the sub-extractor.
    fn get_directory_format(
        extractor: &ExtractorKind,
        sub_extractor: &SubExtractorKind,
        config: &ExtractorConfig,
    ) -> String {
        match extractor {
            ExtractorKind::Artstation => {
                let config: &ArtstationExtractors = &config.artstation.sub_extractors;

                match sub_extractor {
                    SubExtractorKind::ArtstationAlbum => config.album.directory_format.clone(),
                    SubExtractorKind::ArtstationArtwork => config.artwork.directory_format.clone(),
                    SubExtractorKind::ArtstationChallenge => {
                        config.challenge.directory_format.clone()
                    }
                    SubExtractorKind::ArtstationFollowing => {
                        config.following.directory_format.clone()
                    }
                    SubExtractorKind::ArtstationImage => config.image.directory_format.clone(),
                    SubExtractorKind::ArtstationLikes => config.likes.directory_format.clone(),
                    SubExtractorKind::ArtstationSearch => config.search.directory_format.clone(),
                    SubExtractorKind::ArtstationUser => config.user.directory_format.clone(),
                    _ => unreachable!(),
                }
            }
            ExtractorKind::Imgur => {
                let config: &ImgurExtractors = &config.imgur.sub_extractors;

                match sub_extractor {
                    SubExtractorKind::ImgurImage => config.image.directory_format.clone(),
                    _ => unreachable!(),
                }
            }
            ExtractorKind::Reddit => {
                let config: &RedditExtractors = &config.reddit.sub_extractors;

                match sub_extractor {
                    SubExtractorKind::RedditHome => config.home.directory_format.clone(),
                    SubExtractorKind::RedditImage => config.image.directory_format.clone(),
                    SubExtractorKind::RedditSubmission => {
                        config.submission.directory_format.clone()
                    }
                    SubExtractorKind::RedditSubreddit => config.subreddit.directory_format.clone(),
                    SubExtractorKind::RedditUser => config.user.directory_format.clone(),
                    _ => unreachable!(),
                }
            }
            ExtractorKind::Webtoons => {
                let config: &WebtoonsExtractors = &config.webtoons.sub_extractors;

                match sub_extractor {
                    SubExtractorKind::WebtoonsComic => config.comic.directory_format.clone(),
                    SubExtractorKind::WebtoonsEpisode => config.episode.directory_format.clone(),
                    _ => unreachable!(),
                }
            }
        }
    }
}
