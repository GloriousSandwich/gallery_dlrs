//! Manage local key and content address caches.

use std::path::PathBuf;

use ssri::Integrity;

/// Handles creating and reading cache files.
#[derive(Clone, Debug)]
pub struct Cache {
    /// Cache directory.
    directory: PathBuf,
}

impl Cache {
    /// Creates a new [`Cache`].
    pub fn new(directory: PathBuf) -> Self {
        Self { directory }
    }

    /// Write `data` to the cache, indexing it under key.
    pub fn write<K: AsRef<str>, D: AsRef<[u8]>>(
        &self,
        key: K,
        data: D,
    ) -> cacache::Result<Integrity> {
        cacache::write_sync(&self.directory, key, data)
    }

    /// Reads the entire contents of a cache file into a bytes vector, looking
    /// the data up by key.
    pub fn read<K: AsRef<str>>(&self, key: K) -> cacache::Result<Vec<u8>> {
        cacache::read_sync(&self.directory, key)
    }
}
