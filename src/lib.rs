/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Command-line program to download images and collections from several
//! image hosting sites.
//!
//! # Design Notes
//!
//! This crate's API is structured to closely resemble the architecture
//! of [`gallery-dl`](https://github.com/mikf/gallery-dl), a popular python
//! project.
//!
//! # Limitations
//!
//! The python project `gallery-dl` realize heavily on inheritance which
//! is a pain in the ass to transpile into rust.
//!
//! This is all a work in progress, and will need severe refactoring
//! before it is done.
//!
//! This crate is my first attempt in rewriting a big python codebase
//! in rust, and it's probably pretty ugly.

// Makes clippy annoying
#![warn(clippy::all)]
#![warn(clippy::cognitive_complexity)]
#![warn(clippy::manual_ok_or)]
#![warn(clippy::needless_borrow)]
#![warn(clippy::needless_pass_by_value)]
#![warn(clippy::rc_buffer)]
#![warn(clippy::semicolon_if_nothing_returned)]
#![warn(clippy::trait_duplication_in_bounds)]
#![warn(clippy::unseparated_literal_suffix)]
#![warn(noop_method_call)]
#![deny(clippy::await_holding_lock)]
#![deny(clippy::cargo_common_metadata)]
#![deny(clippy::cast_lossless)]
#![deny(clippy::checked_conversions)]
#![deny(clippy::debug_assert_with_mut_call)]
#![deny(clippy::exhaustive_enums)]
#![deny(clippy::exhaustive_structs)]
#![deny(clippy::expl_impl_clone_on_copy)]
#![deny(clippy::fallible_impl_from)]
#![deny(clippy::implicit_clone)]
#![deny(clippy::large_stack_arrays)]
#![deny(clippy::missing_panics_doc)]
#![warn(clippy::option_option)]
#![deny(clippy::ref_option_ref)]
#![deny(clippy::unnecessary_wraps)]
#![deny(clippy::unwrap_used)]

extern crate colored;
extern crate core;
#[macro_use]
extern crate derive_builder;

pub mod app;
pub mod cache;
pub mod cli;
pub mod config;
mod downloader;
pub mod extractor;
pub mod init_request;
pub mod logging;
pub mod oauth;
pub mod path;
mod python;
pub mod text;
