/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

//! Logger implementation.

use std::io;
use std::io::{LineWriter, Write};
use std::path::PathBuf;
use std::sync::Mutex;

use chrono::{DateTime, Local};
use colored::{Color, Colorize};
use file_rotate::compression::Compression;
use file_rotate::suffix::AppendCount;
use file_rotate::{ContentLimit, FileRotate};
use log::{Level, LevelFilter, Metadata, Record};

use crate::config::colors::LoggingColors;
use crate::config::logging::OutputStream;
use crate::config::ui_config::UIConfig;

/// Initializes logger to be used crate wide.
pub fn initialize(config: &UIConfig) -> Result<(), log::SetLoggerError> {
    log::set_max_level(config.logging.log_level);

    let logger: Logger = Logger::new(config);
    let _path: PathBuf = logger.file_path().expect("");
    log::set_boxed_logger(Box::new(logger))?;

    Ok(())
}

/// Logger for gallery_dlrs.
pub struct Logger {
    /// Logfile configuration.
    logfile: Mutex<LogFile>,

    /// The output stream to log to.
    stream: Mutex<LineWriter<OutputStream>>,
}

/// Logfile configuration.
pub struct LogFile {
    /// The logfile itself.
    file: Option<FileRotate<AppendCount>>,

    /// Has the logfile already been created?
    created: bool,

    /// Logfile path.
    path: PathBuf,

    /// The max amount of logfiles that can be rotated before being deleted.
    max_files: usize,

    /// When to rotate the logfile.
    content_limit: ContentLimit,

    /// When to compress the logfile.
    compression: Compression,

    /// Logging colors.
    colors: LoggingColors,
}

/// List of targets which will be logged by gallery_dlrs.
const ALLOWED_TARGETS: [&str; 2] = ["gallery_dlrs", "reqwest"];

impl Logger {
    /// Creates a new [`Logger`].
    fn new(config: &UIConfig) -> Self {
        Self {
            logfile: Mutex::new(LogFile::new(config)),
            stream: Mutex::new(LineWriter::new(config.logging.console.stream.clone())),
        }
    }

    /// Returns the logfile's path.
    fn file_path(&self) -> Option<PathBuf> {
        if let Ok(logfile) = self.logfile.lock() {
            Some(logfile.path().clone())
        } else {
            None
        }
    }
}

impl LogFile {
    /// Creates a new [`Logfile`]
    fn new(config: &UIConfig) -> Self {
        Self {
            file: None,
            created: false,
            path: config.logging.log_file.path.clone().into_inner(),
            max_files: config.logging.log_file.max_files,
            content_limit: config.logging.log_file.content_limit.clone().into_inner(),
            compression: config.logging.log_file.compression.clone().into_inner(),
            colors: config.colors.logging.clone(),
        }
    }

    /// Returns the logfile.
    fn file(&mut self) -> &mut FileRotate<AppendCount> {
        // Create the file if it doesn't exist yet.
        if self.file.is_none() {
            self.file = Some(FileRotate::new(
                &self.path,
                AppendCount::new(self.max_files),
                self.content_limit.clone(),
                self.compression.clone(),
            ));

            self.created = true;

            let _ = writeln!(
                io::stdout(),
                "Created log file at \"{}\"",
                self.path.display()
            );
        }

        #[allow(clippy::unwrap_used)]
        self.file.as_mut().unwrap()
    }

    /// Returns the logfile's path.
    fn path(&self) -> &PathBuf {
        &self.path
    }
}

impl log::Log for Logger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= log::max_level()
    }

    fn log(&self, record: &Record) {
        // Get target crate.
        let index: usize = record
            .target()
            .find(':')
            .unwrap_or_else(|| record.target().len());
        let target: &str = &record.target()[..index];

        // Only log our own crates, except when logging at Level::Trace.
        if !self.enabled(record.metadata()) || !is_allowed_target(record.level(), target) {
            return;
        }

        // Create log message for the given `record` and `target`.
        #[allow(unused_mut, unused_assignments)]
        let mut message: String = String::new();
        match self.logfile.lock() {
            Ok(logfile) => message = create_log_message(&logfile.colors, record, target),
            Err(error) => {
                let _ = writeln!(
                    io::stderr(),
                    "Error: Using default logging colors: {}",
                    error
                );
                message = create_log_message(&LoggingColors::default(), record, target);
            }
        }

        // Write to logfile.
        if let Ok(mut logfile) = self.logfile.lock() {
            let _ = logfile.write(message.as_ref());
        }

        // Write to the chosen output stream.
        if let Ok(mut stream) = self.stream.lock() {
            let _ = stream.write_all(message.as_ref());
        }
    }

    fn flush(&self) {
        unimplemented!()
    }
}

impl Write for LogFile {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        self.file().write(buf)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.file().flush()
    }
}

/// Adds color to `message`.
fn colored_message(colors: &LoggingColors, level: Level, message: &str) -> String {
    let color: Color = match level {
        Level::Trace => colors.trace,
        Level::Info => colors.info,
        Level::Debug => colors.debug,
        Level::Warn => colors.warn,
        Level::Error => colors.error,
    };

    message.color(color).to_string()
}

/// Create a log message.
fn create_log_message(colors: &LoggingColors, record: &Record<'_>, target: &str) -> String {
    let local: DateTime<Local> = Local::now();

    let mut message: String = format!(
        "[{}] [{}] [{}] ",
        local.format("%Y-%m-%d %H:%M:%S"),
        record.level(),
        target
    );

    // Alignment for the lines after the first new line character in the payload. We
    // don't deal with full-width/unicode chars here, so just `message.len()` is
    // sufficient.
    let alignment: usize = message.len();

    // Push lines with added extra padding on the next line, which is trimmed later.
    let lines: String = record.args().to_string();
    for line in lines.split('\n') {
        let line: String = format!("{}\n{:width$}", line, "", width = alignment);
        message.push_str(&line);
    }

    // Drop extra trailing alignment.
    message.truncate(message.len() - alignment);

    let message: String = colored_message(colors, record.level(), message.as_str());

    message
}

/// Check if log messages from a crate should be logged.
fn is_allowed_target(level: Level, target: &str) -> bool {
    match (level, log::max_level()) {
        (Level::Error, LevelFilter::Trace) | (Level::Warn, LevelFilter::Trace) => true,
        _ => ALLOWED_TARGETS.contains(&target),
    }
}
