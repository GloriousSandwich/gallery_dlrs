# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.5.0] 2022-10-26

---

### Added

- Support for queuing URLs. 35dac112
- Support for caching oauth tokens. bdb5bdf4
- Display keywords for URLs. 7cb08c95
- Support for formatting filenames and directories with keywords. 76b16136
- Support for downloading videos with yt-dlp. c4bb8f8e
- [imgur] Support for extracting imgur hosted images. 92e2df16
- [reddit] Support for extracting reddit hosted images. 4b65abc5
- [reddit] Support for authentication via oauth. ea0f830b
- [reddit] Support for extracting reddit submissions. caac5a70
- [reddit] Extract galleries from reddit submisisons. d2e339c9
- [reddit] Support for extracting reddit user profiles. 022fd91f
- [reddit] Support for extracting reddit subreddits. 88534025
- [reddit] Support for extracting reddit home feeds. e1a8c4d4

### Fixed

- Manpage did not update. 8f35bced
- [imgur] Deleting the cache makes the program panics. 43cb45c4
- [reddit] Incorrect gallery image number. 12a659cb
- [reddit] Single images did not have a file number. 11a5fe5d

## [1.4.0] 2022-07-31

___

### Added

- Add colors to logging. b83bf5c0
- Support for custom `HTTP` cookies. b83bf5c0
- Support for rotating logfiles. b83bf5c0
- User can choose between logging to stdout and stderr. b83bf5c0
- Override config options from the CLI with `-o`. b83bf5c0

### Fixed

- [webtoons] Extractors did not bypass the age-gate. 43149d2c
- Crash if the filename in the download url contains the extension. 216f8eec

### Removed

- Support for a custom logging format. b83bf5c0
- Input files which has been replaced with the CLI arg `-o.` b83bf5c0

### Changed

Switched the config file type from `JSON` to `YAML` for a better user experience.
Extensive documentation has been added for each field. Run `gallery_dlrs` once to
create the new config file. Those familiar with alacritty's config file should no
problems navigating gallery_dlrs' config file. ff89bc4f

**Users will need to migrate their settings to `gallery_dlrs.yaml` from 1.4.0 onwards**.

## [1.3.0] 2022-07-03

___ 

### Added

- Support for resuming interuptted downloads. 8f57201a
- Support for creating config file at `$XDG_CONFIG_HOME.` 727f898f
- [artstation] Download 4k images by default. 62cd97ee
- [webtoons] Support for `--get-urls`. d9530fa6

### Fixed

- [artstation] `ParseIntError` when downloading challenges. 86f9e8a9
- [artstation] Program skips downloading artstation urls. b084e0ab
- [webtoons] Crash when making http2 requests. 7ea39847
- [webtoons] Crash downloading a comic from a different page. e1a70eb8
-

## [1.2.0] 2022-06-11

___

### Added

- Support for downloading an episode on
  webtoons.com. [#403aec06](https://gitlab.com/GloriousSandwich/gallery_dlrs/-/commit/403aec060fb354ac04336164832fde3cf1c3d6af)
- Support for downloading an entire comic on
  webtoons.com. [#eb08dac0](https://gitlab.com/GloriousSandwich/gallery_dlrs/-/commit/eb08dac0f8fec24896c7e24bca309790f7342622)
- Support for more values for config setting `extractor.timeout`:
  - **Range values**. Example: "20-30" chooses a random value between 20 and 30 for the timeout.
  - **Strings**. Example: "20" converts the string to a
    float. [#3c7dbeb9](https://gitlab.com/GloriousSandwich/gallery_dlrs/-/commit/3c7dbeb9093ee466e7319585cca4b4747a0d2e45)

### Fixed

- `--threads` did not changed the amount of worker threads.

## [1.1.0] 2022-06-01

___ 

### Changed

- Switch application from single threaded to
  multithreading. [#f48b713d](https://gitlab.com/GloriousSandwich/gallery_dlrs/-/commit/f48b713df12a5ac4d3818054efd027a990b82e15)

## [1.0.0] 2022-05-28

___

### Added

- Support for downloading a user's following page.
- Support for downloading Artstation search results.
- Support for the following configuration settings:
  - `extractor.skip`
  - `extractor.sleep`
  - `extractor.sleep-extractor`
  - `extractor.timeout`
- Support for adding multiple HTTP headers for config setting `downloader.http.headers`.
- Support for shell-expanding config setting `extractor.base-directory` and `output.logfile`.

### Fixed

- Could not detect query when downloading from Artstation's artwork page.
- Crash when using the CLI arg `--get-urls` with an Artstation challenge URL.

## [0.7.0] 2022-05-13

___

### Added

- Support for downloading artworks from Artstation's home page
- Support for downloading Albums from Artstation
- Support for specifing the number of pages to extract projects from Artstation
- Support for adding multiple proxies (only one can be used at a time)

### Fix

- Crash when adding multiple cookies

## [0.6.0] - 2022-05-03

___

### Added

- Support for the following Command line arguments (
  See [commit](https://gitlab.com/GloriousSandwich/gallery_dlrs/-/merge_requests/3)):
  - --quiet
  - --list-extractors
  - --list-modules
  - --write-log
  - --ignore-config
- Support for setting the following config options in an input file:
  - base-directory
  - timeout
  - filesize-min
  - filesize-max
  - proxy

## [0.5.0] - 2022-05-01

___

### Added

- A minimal API.
- [Artstation] Added support for downloading a user's profile.
- [Artstation] Added support for downloading all projects a user liked.
- Support for input files. (
  See [commit](https://gitlab.com/GloriousSandwich/gallery_dlrs/-/commit/33ef832fadafb2aa64b3bc93bc54a1066b58f76e))

### Fixed

- [Artstation] Every project on a user's profile did not get downloaded.
- Fixed crash when a file has '/' in its name.

## [0.4.0] - 2022-03-28

___

### Added

- A config file at `$HOME/.config/gallery_dlrs/`

### Changed

- Switch to clap's Derive API.

## [0.3.0] - 2022-03-28

___

## Added

- A manpage.
- A CLI help screen using the `clap` crate
- Accept URL as a CLI argument.
- Support setting a base directory for file downloads (
  See [commit](https://gitlab.com/GloriousSandwich/gallery_dlrs/-/commit/0d5fe6e22ec1469beb02c10f2207aaa375cd58b3))

## [0.2.0] - 2022-03-27

___

## Added

- Logging support.