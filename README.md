# Gallery_dlrs User Handbook

This document covers everything you ever wanted to know about using gallery_dlrs.

Gallery_dlrs is a command-line program that downloads images, galleries, and collection from several hosting sites.
(See [supported sites](docs/supportedsites.md))

The end goal of this project is to produce a production-quality implementation of the Python project
[Gallery-dl](https://github.com/mikf/gallery-dl) in the [Rust](https://www.rust-lang.org/) programming language.

## Trying it out today

Gallery-dlrs does not support the Windows operating system.

To try it out, run the program as follows. It will download a user's profile on the website `Artstation`.

```bash
cargo run --release -- -v "https://www.artstation.com/aenamiart"
```

A folder with the artworks will be created in the current working directory.

The command below downloads a reddit subreddit.

```bash
cargo run --release -- -v "https://reddit.com/r/dankmemes"
```

## Configuration

A config file is created at $HOME/.config/gallery_dlrs/ when running gallery_dlrs for the first time.

See the [config file](config/default.yaml) for extensive documentation.

## Roadmap

Gallery_dlrs 1.0.0: Minimal Working Client

- [X] Read settings from config
- [X] Basic logger
- [X] Arguments support
- [X] Minimal API
- [X] Full Artstation support
- [X] Support more configurations
- [X] Code cleanups
- [X] Good test coverage

Gallery_dlrs 2.0.0

- [X] Multithreading support
- [X] Full Reddit support
- [ ] Full Deviantart support
- [ ] Full Imgur support
- [X] Full Webtoons support
- [ ] Full Wallhaven support
- [ ] Full Pinterest support

## FAQ

### URLs with special characters

URLs containing special characters such as `&` need to be wrapped in `""`.

```console
gallery_dlrs "https://www.artstation.com/search?sort_by=relevance&query=art"
```

### Using the argument `-o`

To add the url after `-o` use `--`.

```bash
gallery_dlrs -o extractor.base_directory=/tmp -- https://artstation.com/wlop
```

Config types that can be changed:

```yaml
extractor:
  base_directory: "/tmp"  # gallery_dlrs -o extractor.base_directory=/tmp

logging:
  log_file:
    content_limit: { lines: 1000 } # gallery_dlrs -o logging.log_file.content_limit.lines=1000
```

Config types that cannot be changed:

```yaml
extractor:
  cookies:
    - { name: example, value: 123 } # List of dictionaries not supported
```

## Oauth

gallery_dlrs supports user authentication via OAuth for reddit only. This is mostly optional, but grants gallery_dlrs
the ability to issue requests on your account's behalf and enables it to access resources which would otherwise be
unavailable to a public user.

To link your account to gallery_dlrs, start by invoking it with ```--oauth <sitename>``` as an argument. For example:

```bash
gallery_dlrs --oauth reddit
```

You will be sent to the site's authorization page and asked to grant read access to gallery_dlrs. Authorize it, and you
will be shown one or more "tokens", which will be added to the cache for reuse. If you oauth with a different account
and delete the cache, will need to invoke ```--oauth <sitename>``` to use the same account again.

## How can I help out?

See [CONTRIBUTING](https://gitlab.com/GloriousSandwich/gallery_dlrs/-/blob/master/CONTRIBUTING.org) for a few ideas on
how to get started.

## License

This code is licensed under [GNU General Public Licence](https://www.gnu.org/licenses/gpl-3.0.en.html).
