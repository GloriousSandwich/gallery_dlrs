# Supported Sites

<table>
<thead valign="bottom">
<tr>
    <th>Site</th>
    <th>URL</th>
    <th>Capabilities</th>
    <th>Authentication</th>
</tr>
</thead>

<tbody valign="top">
<tr>
    <td>ArtStation</td>
    <td>https://www.artstation.com/</td>
    <td>Albums, Artwork Listings, Challenges, individual Images, Likes, Search Results, User Profiles</td>
    <td></td>
</tr>

<tr>
    <td>imgur</td>
    <td>https://imgur.com/</td>
    <td>Individual Images</td>
    <td></td>
</tr>

<tr>
    <td>Reddit</td>
    <td>https://www.reddit.com/</td>
    <td>Home Feed, individual Images, Submissions, Subreddits, User Profiles</td>
    <td><a href="../README.md#oauth">OAuth</a></td>
</tr>

</tbody>
</table>
