#!/usr/bin/env bash
#
# Copyright (c) 2022, Ronald Caesar, All rights reserved.
#

#
# Starts cargo-watch.
#
# cargo-watch is a rust crate that watches a project directory for
# any changes that has been made, then it will run the command
# `cargo build`.

# shellcheck disable=SC2103
# shellcheck disable=SC2164

# Fetches project root folder regardless of where this script is being executed.
PROJECT_ROOT_FOLDER="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; cd - )"

CARGO_WATCH_CRATE="$(cargo install --list |grep cargo-watch | cut -d " " -f 1)"

if [ "$CARGO_WATCH_CRATE" != "cargo-watch" ]; then
  echo "Installing cargo-watch"
  cargo install cargo-watch
fi

cd "$PROJECT_ROOT_FOLDER"
cargo watch -c -w src -x clippy