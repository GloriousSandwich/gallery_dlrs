use colored::Color::Red;
use reqwest::Client;
use serde_json::Value;

use gallery_dlrs::cache::Cache;
use gallery_dlrs::config::de_wrappers::{ClientIdWrapper, ClientSecretWrapper};
use gallery_dlrs::config::ui_config::UIConfig;
use gallery_dlrs::extractor::reddit::home::RedditHomeExtractor;
use gallery_dlrs::extractor::reddit::image::RedditImageExtractor;
use gallery_dlrs::extractor::reddit::submission::RedditSubmissionExtractor;
use gallery_dlrs::extractor::reddit::subreddit::RedditSubredditExtractor;
use gallery_dlrs::extractor::reddit::user::RedditUserExtractor;
use gallery_dlrs::extractor::{Extractor, ExtractorKind};
use gallery_dlrs::init_request::initialize_reqwest_extractor;

#[tokio::test]
async fn get_metadata_home_success() {
    let mut config: UIConfig = UIConfig::default();
    let cache: Cache = Cache::new(config.cache.directory.clone().into_inner());
    config.extractor.reddit.oauth2.client_secret =
        Some(ClientSecretWrapper::from("ffK7-rPpK07C-UZmwHQUsUQvPnk7UA"));
    config.extractor.reddit.oauth2.client_id = ClientIdWrapper::from("o7FSXFa45Gh1k9NUhpYFnQ");
    let client: Client = initialize_reqwest_extractor(
        &config,
        "https://old.reddit.com/top",
        &ExtractorKind::Reddit,
    );
    let extractor: RedditHomeExtractor = RedditHomeExtractor::new(
        "https://old.reddit.com/top",
        &config.extractor.reddit,
        &cache,
        "",
    )
    .unwrap();

    let metadata: Vec<Value> = extractor.get_metadata(client).await;

    assert!(!metadata.is_empty());
}

#[tokio::test]
async fn get_metadata_image_success() {
    let extractor: RedditImageExtractor =
        RedditImageExtractor::new("https://i.redd.it/m23bwh4n0x151.png", "").unwrap();
    let client: Client = initialize_reqwest_extractor(
        &UIConfig::default(),
        "https://i.redd.it/m23bwh4n0x151.png",
        &ExtractorKind::Reddit,
    );

    let metadata: Vec<Value> = extractor.get_metadata(client).await;

    assert!(!metadata.is_empty());
}

#[tokio::test]
async fn get_metadata_submission_success() {
    let mut config: UIConfig = UIConfig::default();
    config.extractor.reddit.oauth2.client_secret =
        Some(ClientSecretWrapper::from("ffK7-rPpK07C-UZmwHQUsUQvPnk7UA"));
    config.extractor.reddit.oauth2.client_id = ClientIdWrapper::from("o7FSXFa45Gh1k9NUhpYFnQ");
    let cache: Cache = Cache::new(config.cache.directory.clone().into_inner());
    let extractor: RedditSubmissionExtractor = RedditSubmissionExtractor::new("https://www.reddit.com/r/wallpaper/comments/gtfkar/2560x1440_beautiful_sunset_with_a_rocket/", &config.extractor.reddit, &cache, "").unwrap();
    let client: Client = initialize_reqwest_extractor(&config, "https://www.reddit.com/r/wallpaper/comments/gtfkar/2560x1440_beautiful_sunset_with_a_rocket/", &ExtractorKind::Reddit);

    let metadata: Vec<Value> = extractor.get_metadata(client).await;

    assert!(!metadata.is_empty());
}

#[tokio::test]
async fn get_metadata_subreddit_success() {
    let mut config: UIConfig = UIConfig::default();
    config.extractor.reddit.oauth2.client_secret =
        Some(ClientSecretWrapper::from("ffK7-rPpK07C-UZmwHQUsUQvPnk7UA"));
    config.extractor.reddit.oauth2.client_id = ClientIdWrapper::from("o7FSXFa45Gh1k9NUhpYFnQ");
    let cache: Cache = Cache::new(config.cache.directory.clone().into_inner());
    let extractor: RedditSubredditExtractor = RedditSubredditExtractor::new(
        "https://www.reddit.com/r/dankmemes",
        &config.extractor.reddit,
        &cache,
        "",
    )
    .unwrap();
    let client: Client = initialize_reqwest_extractor(
        &config,
        "https://www.reddit.com/r/dankmemes",
        &ExtractorKind::Reddit,
    );

    let metadata: Vec<Value> = extractor.get_metadata(client).await;

    assert!(!metadata.is_empty())
}

#[tokio::test]
async fn get_metadata_user_success() {
    let mut config: UIConfig = UIConfig::default();
    config.extractor.reddit.oauth2.client_secret =
        Some(ClientSecretWrapper::from("ffK7-rPpK07C-UZmwHQUsUQvPnk7UA"));
    config.extractor.reddit.oauth2.client_id = ClientIdWrapper::from("o7FSXFa45Gh1k9NUhpYFnQ");
    let cache: Cache = Cache::new(config.cache.directory.clone().into_inner());
    let extractor: RedditUserExtractor = RedditUserExtractor::new(
        "https://www.reddit.com/u/username/",
        &config.extractor.reddit,
        &cache,
        "",
    )
    .unwrap();
    let client: Client = initialize_reqwest_extractor(
        &config,
        "https://www.reddit.com/u/username/",
        &ExtractorKind::Reddit,
    );

    let metadata: Vec<Value> = extractor.get_metadata(client).await;

    assert!(!metadata.is_empty());
}
