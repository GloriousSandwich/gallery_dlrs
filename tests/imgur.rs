use reqwest::Client;
use serde_json::Value;

use gallery_dlrs::config::extractor::Imgur;
use gallery_dlrs::config::ui_config::UIConfig;
use gallery_dlrs::extractor::imgur::image::ImgurImageExtractor;
use gallery_dlrs::extractor::{Extractor, ExtractorKind};
use gallery_dlrs::init_request::initialize_reqwest_extractor;

#[tokio::test]
async fn get_metadata_image_success() {
    let config: UIConfig = UIConfig::default();
    let extractor: ImgurImageExtractor =
        ImgurImageExtractor::new("https://imgur.com/21yMxCS", &config.extractor.imgur, "").unwrap();
    let client: Client =
        initialize_reqwest_extractor(&config, "https://imgur.com/21yMxCS", &ExtractorKind::Imgur);

    let metadata: Vec<Value> = extractor.get_metadata(client).await;

    assert!(!metadata.is_empty());
}
