/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

use reqwest::Client;
use serde_json::Value;

use gallery_dlrs::config::ui_config::UIConfig;
use gallery_dlrs::extractor::webtoons::comic::WebtoonsComicExtractor;
use gallery_dlrs::extractor::webtoons::episode::WebtoonsEpisodeExtractor;
use gallery_dlrs::extractor::{Extractor, ExtractorKind};
use gallery_dlrs::init_request::initialize_reqwest_extractor;

#[tokio::test]
async fn get_metadata_comic_success() {
    let extractor: WebtoonsComicExtractor = WebtoonsComicExtractor::new(
        "https://www.webtoons.com/en/comedy/live-with-yourself/list?title_no=919",
        2,
        "",
    )
    .unwrap();
    let client: Client = initialize_reqwest_extractor(
        &UIConfig::default(),
        "https://www.webtoons.com/en/comedy/live-with-yourself/list?title_no=919",
        &ExtractorKind::Webtoons,
    );

    let metadata: Vec<Value> = extractor.get_metadata(client).await;

    assert!(!metadata.is_empty());
}

#[tokio::test]
async fn get_metadata_episode_success() {
    let extractor: WebtoonsEpisodeExtractor = WebtoonsEpisodeExtractor::new("https://www.webtoons.com/en/comedy/safely-endangered/ep-572-earth/viewer?title_no=352&episode_no=572", "").unwrap();
    let client: Client = Client::default();

    let metadata: Vec<Value> = extractor.get_metadata(client).await;

    assert!(!metadata.is_empty());
}
