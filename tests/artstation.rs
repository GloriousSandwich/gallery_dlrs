/*
 * Copyright (c) 2022, Ronald Caesar, All rights reserved.
 */

use reqwest::Client;
use serde_json::Value;

use gallery_dlrs::config::ui_config::UIConfig;
use gallery_dlrs::extractor::artstation::album::ArtstationAlbumExtractor;
use gallery_dlrs::extractor::artstation::artwork::ArtstationArtworkExtractor;
use gallery_dlrs::extractor::artstation::challenge::ArtstationChallengeExtractor;
use gallery_dlrs::extractor::artstation::following::ArtstationFollowingExtractor;
use gallery_dlrs::extractor::artstation::image::ArtstationImageExtractor;
use gallery_dlrs::extractor::artstation::likes::ArtstationLikesExtractor;
use gallery_dlrs::extractor::artstation::search::ArtstationSearchExtractor;
use gallery_dlrs::extractor::artstation::user::ArtstationUserExtractor;
use gallery_dlrs::extractor::{Extractor, ExtractorKind};
use gallery_dlrs::init_request::initialize_reqwest_extractor;

#[tokio::test]
async fn get_metadata_album_success() {
    let extractor: ArtstationAlbumExtractor = ArtstationAlbumExtractor::new(
        "https://www.artstation.com/solarblack/albums/6532880",
        &String::new(),
    )
    .unwrap();
    let client: Client = initialize_reqwest_extractor(
        &UIConfig::default(),
        "https://www.artstation.com/solarblack/albums/6532880",
        &ExtractorKind::Artstation,
    );

    let metadata: Vec<Value> = extractor.get_metadata(client).await;

    assert!(!metadata.is_empty());
}

#[tokio::test]
async fn get_metadata_artwork_success() {
    let extractor: ArtstationArtworkExtractor = ArtstationArtworkExtractor::new(
        "https://www.artstation.com/artwork?sorting=latest",
        2,
        &String::new(),
    )
    .unwrap();
    let client: Client = Client::default();

    let metadata: Vec<Value> = extractor.get_metadata(client).await;

    assert!(!metadata.is_empty());
}

#[tokio::test]
async fn get_metadata_challenge_success() {
    let extractor: ArtstationChallengeExtractor = ArtstationChallengeExtractor::new(
        "https://www.artstation.com/contests/thu-2017/challenges/20",
        2,
        &String::new(),
    )
    .unwrap();
    let client: Client = initialize_reqwest_extractor(
        &UIConfig::default(),
        "https://www.artstation.com/contests/thu-2017/challenges/20",
        &ExtractorKind::Artstation,
    );

    let metadata: Vec<Value> = extractor.get_metadata(client).await;

    assert!(!metadata.is_empty());
}

#[tokio::test]
async fn get_metadata_following_success() {
    let extractor: ArtstationFollowingExtractor = ArtstationFollowingExtractor::new(
        "https://www.artstation.com/wlop/following",
        2,
        &String::new(),
    )
    .unwrap();
    let client: Client = initialize_reqwest_extractor(
        &UIConfig::default(),
        "https://www.artstation.com/gaerikim/following",
        &ExtractorKind::Artstation,
    );

    let metadata: Vec<Value> = extractor.get_metadata(client).await;

    assert!(!metadata.is_empty());
}

#[tokio::test]
async fn get_metadata_image_success() {
    let extractor: ArtstationImageExtractor =
        ArtstationImageExtractor::new("https://www.artstation.com/artwork/LQVJr", &String::new())
            .unwrap();
    let client: Client = initialize_reqwest_extractor(
        &UIConfig::default(),
        "https://www.artstation.com/artwork/LQVJr",
        &ExtractorKind::Artstation,
    );

    let metadata: Vec<Value> = extractor.get_metadata(client).await;

    assert!(!metadata.is_empty());
}

#[tokio::test]
async fn get_metadata_likes_success() {
    let extractor: ArtstationLikesExtractor =
        ArtstationLikesExtractor::new("https://www.artstation.com/wlop/likes", 2, &String::new())
            .unwrap();
    let client: Client = initialize_reqwest_extractor(
        &UIConfig::default(),
        "https://www.artstation.com/wlop/likes",
        &ExtractorKind::Artstation,
    );

    let metadata: Vec<Value> = extractor.get_metadata(client).await;

    assert!(!metadata.is_empty());
}

#[tokio::test]
async fn get_metadata_search_success() {
    let extractor: ArtstationSearchExtractor = ArtstationSearchExtractor::new(
        "https://www.artstation.com/search?sort_by=relevance&query=arcane",
        2,
        &String::new(),
    )
    .unwrap();
    let client: Client = initialize_reqwest_extractor(
        &UIConfig::default(),
        "https://www.artstation.com/search?sort_by=relevance&query=arcane",
        &ExtractorKind::Artstation,
    );

    let metadata: Vec<Value> = extractor.get_metadata(client).await;

    assert!(!metadata.is_empty());
}

#[tokio::test]
async fn get_metadata_user_success() {
    let extractor: ArtstationUserExtractor =
        ArtstationUserExtractor::new("https://www.artstation.com/wlop", 2, &String::new()).unwrap();
    let client: Client = initialize_reqwest_extractor(
        &UIConfig::default(),
        "https://www.artstation.com/wlop",
        &ExtractorKind::Artstation,
    );

    let metadata: Vec<Value> = extractor.get_metadata(client).await;

    assert!(!metadata.is_empty());
}
